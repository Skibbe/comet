# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 14:52:03 2019

@author: Skibbe.N
"""
from comet import pyhed as ph
import pygimli as pg


a = ph.loop.buildCircle(10, 5)
a.max_area_para = 1/32.
a.refinement_para = 0.5
a.createLoopMesh(savename='test_mesh')

if __name__ == '__main__':
    a.calculate(num_cpu=4)
    a.save('test_loop')

# The End
