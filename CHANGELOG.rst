Changelog
=========

:v 0.9.8:
- Working prototype.
:v 0.9.9:
- Last test for paper submission ready.
:v 0.9.10:
- Changes while paper in second revision.
- secondary field calculation now via .sh, fixes mpi environment bug.
:v 1.0.0:
- First release.
- Compatibility with custEM=0.99.08
:v 1.0.1:
- Compatibility with pyGIMLi versions <= 1.1
- Testing of Structural Coupled Cooperative Inversion (SCCI) finished, entering production mode.
