"""
Part of comet/snmr/modelling
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

class InputError(Exception):
    def __init__(self, file=None, msg=None):
        self.errormessage = 'An Input Error occurred.'
        if file is not None:
            if isinstance(file, str):
                self.errormessage += ' Datatype %r not understood, abort.' % \
                                     (file.split('.')[-1])
            else:
                self.errormessage += ' Datatype %r not understood, abort.' % \
                                     (type(file))

        if msg is not None:
            self.errormessage += ' ' + msg
        super(InputError, self).__init__(self.errormessage)


def DepricationWarning(msg=None):
    full_msg = 'Deprication warning. this feature will be removed \
in the future.'
    if msg is not None:
        full_msg += ' ' + msg
    print(full_msg)


class KernImportError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

# The End
