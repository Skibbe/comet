"""
Part of comet/snmr/modelling
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import sys

# simple submodule import
# direct import of things that are frequently in use
from . snmrModelling import (SNMRModelling, MRS1dBlockQTModelling,
                             SNMRJointModelling)
from . mrs import (MRS, showErrorBars, showWC, showT2)
from . nmr_base import SNMRBase, effectiveNoise, getPhiByGridSearch
from . errors import (InputError, DepricationWarning, KernImportError)
from . smooth_syn import modelVadose
from . mrs_survey import MRT
from . dipole import (handleInputMag, getBfromM, createIntegrationPoints,
                      explicit_curl, integrateEField, integrate)
# THE END
