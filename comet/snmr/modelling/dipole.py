"""
Part of comet/snmr/modelling

Magnetic resonance sounding module.
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import time
import scipy

import numpy as np
import pygimli as pg

from comet.pyhed import log
from comet.pyhed.misc import rotFromAtoB
from scipy.spatial.distance import pdist, squareform


def handleInputMag(magvec, magpos, rpos, m0=(0, 0, 0),
                   globally=False, mindistance=0.1, magsize=None):
    """

    Parameters
    ----------
    magvec: array_like, shape = (n, 3)
        Magnetization vector for n points or cells.

    magpos: pg.Mesh or array_like, shape = (n, 3)
        Position of magnetization vectors as n points or
        pygimli Mesh, assuming m cells, with vectors living
        at the cell centers, representing the entire cell and
        therefore volume of the mesh.

    rpos: optional, array_like, shape = (m, 3)
        Receiver points for which the magnetic field is
        integrated. Used for singularity removal if discretization
        is a pygimli mesh and a singularity removal is desired.
        For each of the m positions in rpos, the coresponding cell
        is searched and the magnetization vector in this cell is
        substracted of the values in all other cells and returned
        as second output value ref_mag to later calculate the background
        b-field analytically. If no cell is found, the background
        magnetization is set to b0 for stable boundary conditions.

    m0: array_like, shape (3,)
        Background magnetization (e.g. magnetization of earth magnetiv
        field) for singularity removal of rpos outside the magnetization
        mesh domain.

    globally : boolean, optional
        Experimental flag for a different kind of singularity removal.
        If True for every magnetic field vector the corresponding cell is
        found and the magnetic moment within this cell is removed from all
        cells to have a zero anomaly situation to avoid the effects of the
        singularities. After integration of the magneitc field a global
        analytic value based on the removed moment is added to the field value.
        The default is False. A warning is issued as long as the tests are
        not finished for this functionality.

    mindistance : float or None, optional
        Distance between magnetic moments and integration points of the
        magnetic fields at which the singularity removal is applied.
        For a value of None, the corresponding cell is searched for each
        point and used for the removal.
        The default is 0.1 [m].

    magsize : float or None, optional
        Vector with volume weightings of the input dipole moments. In case a
        pygimli mesh is given for magpos, this will be ignored and a warning
        is issued.
        The default is None.

    Returns
    -------
    mag_anom : TYPE
        DESCRIPTION.
    ref_b : TYPE
        DESCRIPTION.
    coincident : TYPE
        DESCRIPTION.

    """
    # In this case the magnetization lives on the cell centers of the mesh
    # and represents the entire cell. Therefore a weighting with the cell
    # sizes is needed. Also a singularity removal for each receiver point
    # is needed, because an accurate integration of the magnetic fields in
    # a cell is not easy.

    if isinstance(magpos, pg.Mesh):
        mcount = magpos.cellCount()
        dpos = magpos.cellCenters().array()

        if magsize is not None:
            log.warning('keyword argument magsize passed, but ignored due to'
                        ' magpos being a pygimli mesh.')
        magsize = np.array(magpos.cellSizes())

    else:
        shape = magpos.shape
        if shape[0] == 3 and shape[1] != 3:
            magpos = magpos.T
        mcount = len(magpos)
        dpos = np.asarray(magpos)
        shape = np.shape(dpos)
        if shape[0] == 3 and shape[1] != 3:
            dpos = dpos.T

    # search for dipole receiver in cells for singularity removal
    ref_mag = np.zeros((len(rpos), mcount, 3))
    coincident = np.zeros((len(rpos), mcount), dtype=bool)

    if mindistance is None:
        if not isinstance(magpos, pg.Mesh):
            raise Exception('**magpos** need to be a pyGIMLi mesh if '
                            '**mindistance** is set to None to activate the '
                            'cell search option.')
        # step 1: search corresponding cell
        for pi, pos in enumerate(rpos):
            cell = magpos.findCell(pos, extensive=False)
            if cell is not None:
                # step 1a: cell inside mesh: subtract m(cell)
                cid = cell.id()
                # note: use M value, not volume integrated m!
                ref_mag[pi, cid] = magvec[cid]
                coincident[pi, cid] = True
        # shape: (d, 3) -> (r, d, 3)
        # now singularity removal for each receiver independantly
        mag_anom = np.repeat(magvec[np.newaxis, :], ref_mag.shape[0],
                             axis=0)

    else:
        # calc position of each receiver relative to each
        # magnetization vector
        diff = rpos[:, np.newaxis, :] - dpos[np.newaxis, :, :]
        # diff shape: (r, d, 3)

        # distance: dipols - receivers
        if np.shape(dpos) == np.shape(rpos) and np.allclose(dpos, rpos):
            # shortcut if positions are identical
            distances = squareform(pdist(rpos))
        else:
            # dot product with itself is euclidian distance due to
            # shift of diff by dipole position earlier
            distances = np.sqrt(np.einsum('abc,abc->ab', diff, diff))
        # distances shape: (r, d)
        coincident[distances < mindistance] = True
        # shape: (d, 3) -> (r, d, 3)
        # now singularity removal for each receiver independantly
        mag_anom = np.repeat(magvec[np.newaxis, :], ref_mag.shape[0],
                             axis=0)
        print('magvec', magvec.shape)
        print('ref_mag', ref_mag.shape)
        print('min/max ref_mag', np.min(ref_mag), np.max(ref_mag))
        print('mag_anom', mag_anom.shape)
        print('distances', distances.shape)

        ref_mag[distances < mindistance] =\
            mag_anom[distances < mindistance]

    if globally:
        log.warning('Removing singularity globally, this is not yet tested'
                    ' properly!')
        mag_anom = mag_anom - ref_mag
    else:
        log.debug('singularity handled locally')

    if magsize is not None:
        # weighting for individual cells
        # not possible nor needed for defined magnetic moments at specific
        # positions, they should lready be weighted and not be specific values
        # (r, d, 3) * (d, 1) = (r, d, 3)
        mag_anom *= magsize[:, np.newaxis]
        ref_b = ref_mag * magsize[:, np.newaxis]

    log.debug('magnetic anomaly: {}'.format(mag_anom))
    log.info('coincident found: {}'.format(np.sum(coincident)))
    # already weighted by curie, weight with missing mu0
    # *= mu0
    ref_b *= 1e-7 * 4 * np.pi
    return mag_anom, ref_b, coincident


def getBfromM(magnetization, discretization,
              receiver, m0=(0, 0, 0), globally=False, mindistance=0.1,
              magsize=None):
    """
    Parameters:
    -----------

        magvec: array_like, shape = (n, 3)
            Magnetization vector for n points or cells.

        discretization: pg.Mesh or array_like, shape = (n, 3)
            Position of magnetization vectors as n points or
            pygimli Mesh, assuming m cells, with vectors living
            at the cell centers, representing the entire cell and
            therefore volume of the mesh.

        rpos: pg.Mesh or array_like, shape = (m, 3)
            Receiver points for which the magnetic field is
            integrated. Used for singularity removal if discretization
            of the magnetization is a pygimli mesh.
            For each of the m positions in rpos, the coresponding cell
            is searched and the magnetization vector in this cell is
            substracted of the values in all other cells and returned
            as second output value ref_mag to later calculate the background
            b-field analytically. If no cell is found, the background
            magnetization is set to b0 for stable boundary conditions.

        m0: array_like, shape (3,)
            Background magnetization (e.g. magnetization of earth magnetiv
            field) for singularity removal of rpos outside the magnetization
            mesh domain.

    """
    # handle magnetization discretization
    if isinstance(discretization, pg.Mesh):
        # case 1/2: magnetization vectors are living at cell centers
        # singularity removal, needed
        dpos = np.array(discretization.cellCenters())
    else:
        # case 2/2: magnetization moments are given directly
        dpos = np.atleast_2d(discretization)

    shape = dpos.shape
    if shape[0] == 3 and shape[1] != 3:
        dpos = dpos.T

    # handle magnetization discretization
    if isinstance(receiver, pg.Mesh):
        # case 1/2: receiver positions are cell centers
        rpos = np.array(receiver.cellCenters())

    else:
        # case 2/2: receiver positions are given as vector
        rpos = receiver

    tick0 = time.time()
    ret = handleInputMag(
        magnetization, discretization, rpos, m0=m0,
        globally=globally,
        mindistance=mindistance, magsize=magsize)
    tack0 = time.time()
    log.info('handle input mag: {:.3f} sec'.format(tack0 - tick0))
    mag_anom, ref_b, coincident = ret
    # mag_vec.shape: (d, 3)
    # ref_mag.shape: (r, 3)

    # calc position of each receiver realtive to each magnetization vector
    diff = rpos[:, np.newaxis, :] - dpos[np.newaxis, :, :]
    # diff shape: (r, d, 3)

    # distance: dipols - receivers
    if np.shape(dpos) == np.shape(rpos) and np.allclose(dpos, rpos):
        # shortcut if positions are identical
        distances = squareform(pdist(rpos))
    else:
        # dot product with itself is euclidian distance due to shift of diff by
        # dipole position earlier
        distances = np.sqrt(np.einsum('abc,abc->ab', diff, diff))
    # distances shape: (r, d)

    # remove 0 distances for coincident cells to avoid division by 0
    # those cases are handled by the singularity removal later
    # the values of 1e-32 are not important, we override them
    distances[coincident] = 1e-32
    # coincident shape: (r, d)

    # We now have:
    # The distances and positions of all receiver, magnetization dipoles
    # they are weighted if representing a cell
    # Furthermore all coincident cells where a receiver is living on a
    # dipole are identified to handle them later on.
    # Also the singularity removal for each receiver point is prepared

    # next step: using analytical formulation to integrate bfields for each
    # receiver, for all magnetization vectors

    r, d, _ = np.shape(mag_anom)

    log.info('tasks: {} x {} = {:,}'.format(r, d, r*d))
    tick3 = time.time()
    mdotr = np.einsum('abc,abc->ab', mag_anom, diff)

    b1 = np.array(rpos.shape, dtype=np.complex128)

    b1_all = 1e-7 * (3 * diff * mdotr[:, :, np.newaxis] /
                     (distances[:, :, np.newaxis]**5) -
                     mag_anom/(distances[:, :, np.newaxis]**3))
    # (r, d, 3) * (r, d, :) / (r, d, :) - (r, d, 3) / (r, d, :) = (r, d, 3)
    tack3 = time.time()
    log.info('magnetic: {:.3f} sec'.format(tack3 - tick3))
    log.info('tasks/sec: {:,.1f}'.format(r * d / (tack3 - tick3)))

    # remove b fields only generated by 1e-32 distance we added, those
    # values are meaningless and are catched by the singularity removal
    # print('remove hack')
    log.info('coincident: {}'.format(np.shape(np.where(coincident))))

    log.debug('shape b1_all: {}'.format(np.shape(b1_all)))
    log.debug('shape ref_b: {}'.format(np.shape(ref_b)))

    if globally:
        b1_all[coincident, :] = 0
        b1_all += ref_b

    else:
        b1_all[coincident, :] = ref_b[coincident, :]

    # sum all sub-fields for all magnetization vectors to get one field value
    # per receiver
    b1 = np.sum(b1_all, axis=1)
    # (r, d, 3) -> (r, 3)

    return b1


def createIntegrationPoints(dipoles, d=0.05):
    """

    Parameters
    ----------
    dipoles : array_like
        Input dipoles where the integration will be performed.
        Shape = (n, 3)
    d : float, optional
        Edgelength of the curl window in meter. The default is 0.05.

    Returns
    -------
    quadsh_rotated : array_like
        Position vector at which the magneitc field needs to be known for
        the efield integration. Returns four points per integration step.
        Shape = (n-1, 4, 3).
        ((n-1)*4, 3) with shift order (-x, -z, x, z) for normal y direction.

    quad_dir : array_like
        Direction vector of the magnetic field vectors to extract
        components (local y direction for x-z curl plane).
        Shape = ((n-1)*4, 3) with same order as quadsh_rotated.

    d_len : array_like
        Length of each dipole segment. Shape = (n-1,)

    """

    # Construct help dipoles for the evaluation of the electric fields.
    d_mid = (dipoles[:-1] + dipoles[1:]) / 2
    d_dir = dipoles[1:] - dipoles[:-1]
    d_len = np.sqrt(np.sum(np.diff(dipoles, axis=0)**2, axis=1))
    d_dir = d_dir / d_len[:, np.newaxis]
    # definition, sign of vector segments
    d_sign = np.sign(np.diff(np.sqrt(np.sum(
        (dipoles - dipoles[0])**2, axis=1))))

    # create helping ring for curl and shift to correct position.
    quad = np.array(((-d/2., 0, 0), (0, 0, -d/2.),
                     (d/2., 0, 0), (0, 0, d/2.)))
    # 4, 3
    start_dir = np.array((0, 1, 0))  # local y direction
    quadsh_rotated = np.zeros((len(d_mid), 4, 3))
    for idx in range(len(d_mid)):
        quadsh_rotated[idx] = rotFromAtoB(
            quad,
            start_dir,
            d_dir[idx]) + d_mid[idx]

    # constructing directions of the magneitc fields at the quadsh_rotated
    # positions
    hlp_quadsh = quadsh_rotated - d_mid[:, np.newaxis, :]
    quad_dir = np.cross(hlp_quadsh, d_dir[:, np.newaxis, :])
    quad_dir /= scipy.linalg.norm(quad_dir, axis=-1)[:, :, np.newaxis]

    # (n, 4, 3) -> (n*4, 3) with (-x, -z, x, z)
    quadsh_rotated = np.concatenate(
          [quadsh_rotated[:, 0], quadsh_rotated[:, 1],
           quadsh_rotated[:, 2], quadsh_rotated[:, 3]])

    # (n, 4, 3) -> (n*4, 3) with (-x, -z, x, z)
    quad_dir = np.concatenate(
          [quad_dir[:, 0], quad_dir[:, 1],
           quad_dir[:, 2], quad_dir[:, 3]])

    return quadsh_rotated, quad_dir, d_len


def integrate(field, direction, length):
    """
    Integrated a number of field values along a dipole discretization.

    Parameters
    ----------
    field : array_like or float
        Field vector, vector of magnitudes or constant magnitude.
        Can be broadcasted if constant.
    direction : array_like
        Vector of directions for vector magnitudes.
    length : array_like
        Representative lengths of the fields for weighting.

    Returns
    -------
    float
        magnitude of the integrated field.

    """

    # normalize direction
    direction =\
        direction / scipy.linalg.norm(direction, axis=-1)[:, np.newaxis]

    if direction is None and len(np.shape(field)) == 1:
        # field is already pointing towards te integration path
        intfield = field[:, np.newaxis]

    elif np.shape(field) == np.shape(direction):
        # case 1/3 vector in
        intfield =\
            np.einsum('ij,ij->i', field, direction)[:, np.newaxis]
        # print('*'*80)
        # print(field[:3])
        # print(direction[:3])
        # print(intfield[:3])
        # print('*'*80)

    elif len(np.shape(field)) == 1 and\
            np.shape(field)[0] == np.shape(direction)[0]:
        # field is pointing towards direction anyway:
        intfield = field[:, np.newaxis]

    else:
        raise Exception('Field {} and direction {} need to be the same '
                        'shape.'.format(np.shape(field), np.shape(direction)))
    #     # cases (2, 3)/3 magnitudes in
    #     field_comps = np.zeros((len(length), 3))
    #     field_comps[:, 0] = field

    #     intfield = rotFromAtoB(field_comps,
    #                             field_comps,
    #                             direction)

    integrated = np.sum(intfield * length[:, np.newaxis], axis=0)
    # return scipy.linalg.norm(integrated)
    return integrated


def explicit_curl(magnitudes, d):
    """
    Calculates the curl using four sample points and magnitudes in the
    direction of the edges.

    Parameters
    ----------
    magnitudes : array_like
        Array of magnitudes in shape (n, 4) for n curls.
    d : float
        Distance of opposing integration points. Is also used in
        **createIntegrationPoints**.

    Returns
    -------
    curl : array_like
        Array containing the curl component in normal direction to the plane
        where the integration points are lying. shape: (n,)

    """
    if len(np.shape(magnitudes)) == 1:
        magnitudes =\
            np.stack(np.array_split(magnitudes, 4, axis=0), axis=1)
        assert np.shape(magnitudes)[1] == 4
    dj = (magnitudes[:, 0] + magnitudes[:, 2]) / d
    # don't know about the minus...
    # something, something right-hand-rule and definition of curl
    # checked in a test
    di = -(magnitudes[:, 1] + magnitudes[:, 3]) / d
    curl_z = di - dj
    return curl_z


def integrateEField(kern, d=0.05, sig=0.001, static_magnetization=False):

    from comet.snmr.misc import constants
    # support points for curl and integration of bfields
    quadsh_rotated, quad_dir, d_len = createIntegrationPoints(
        kern.rx.pos, d=d)
    rpos = quadsh_rotated.reshape(-1, 3)

    # corner points of the discretized rx wire
    pos = kern.rx.pos

    # points where the efields will live
    # mid_pos = (pos[1:] + pos[:-1]) / 2
    mid_dir = pos[1:] - pos[:-1]
    mid_len = scipy.linalg.norm(mid_dir, axis=1)[:, np.newaxis]
    mid_dir = mid_dir / mid_len

    # step 1/4: input magnetization
    magpos = kern.kernelMeshCellCenter
    magsize = kern.kernelMeshCellVolume
    if static_magnetization:
        curie = constants.calcCurieFactor(281)
        mag = np.ones((kern.tx.loopmesh.cellCount(), 3))
        mag *= kern.survey.earth.field.T * kern.survey.earth.magnitude
        magc = mag * curie

    else:
        if kern.magnetization is None:
            kern.calcMagnetization()

        log.warning("get only response for first pulse moment")
        magc = kern.magnetization[0]

    # step 2/4: magnetic fields at support points for curl
    b1 = getBfromM(magc, magpos, rpos, m0=magc[0],
                   globally=False,
                   mindistance=0.1,
                   magsize=magsize)
    h1 = b1 / (4e-7 * np.pi)
    h1 = h1.reshape(-1, 4, 3)

    # step 3/4: explicit curls
    # get projection of H to help window edges
    h_comps = np.einsum('...k,...k->...', h1, quad_dir)

    # step 4/4: weight with sig and integrate
    # 4 values per window, in rotated coordinate system i ,j, k
    # edges of window are defined in i and j direction, with k being the
    # normal direction of the window and therefore the direction of the
    # resulting E field
    # E = (dH(j)) / di - (dH(i)) / dj * 1/sigma
    # dx = dy = d
    e_comps = explicit_curl(h_comps, d)
    e_field = e_comps[:, np.newaxis] * mid_dir / sig

    voltage = integrate(e_field, mid_dir, mid_len)
    print('Voltage (integrate): ', voltage)
    voltage2 = scipy.linalg.norm(np.sum(e_field * mid_len, axis=0))
    print('Voltage (sum): ', voltage2)
    return voltage

# The End
