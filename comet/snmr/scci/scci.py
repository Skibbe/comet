"""
SCCI: structural coupled cooperative inversion
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import pygimli as pg
from comet.pyhed import log


class SCCI(object):
    def __init__(self, managers=[]):
        """ Structural coupled cooperative inversion manager.
        """
        # scci parameters
        self.a = None
        self.b = None
        self.c = None
        self.cmin = None
        self.cmax = None
        self.setCouplingPara()

        # init managers list
        self.managers = managers

        # init constraint weights

    def setCouplingPara(self, a=0.1, b=0.1, c=1.0, cmin=0.2, cmax=1.0):
        """
        Set coupling parameter.
        """
        self.a = a
        self.b = b
        self.c = c
        self.cmin = cmin
        self.cmax = cmax

    @property
    def invs(self):
        return self._gather('inv', raise_error=True)

    @property
    def fops(self):
        return self._gather('fop', raise_error=True)

    @property
    def nparas(self):
        paras = self._gather('_numPara', default=1)
        return paras

    @property
    def npara(self):
        npara = np.sum(self.nparas)
        return npara

    @property
    def nmeth(self):
        nmeth = len(self.managers)
        return nmeth

    @property
    def roughnesses(self):
        output = []
        # do for all managers == inversion instances
        for im, man in enumerate(self.managers):
            try:  # pg 1.0
                model = man.inv.model()
            except BaseException:  # weird cpp boost error, hard to catch
                # pg 1.1
                model = man.inv.model

            if man.fop.constraints().cols() != model.size():
                man.fop.createConstraints()
            try:  # pg 1.0
                roughness = man.inv.pureRoughness(model)
            except AttributeError:  # pg 1.1
                roughness = man.inv.inv.pureRoughness(model)

            seg = int(len(roughness)/self.nparas[im])
            # do for all parameters
            for i in range(self.nparas[im]):
                output.append(np.array(roughness[i * seg:i * seg + seg]))
        return output

    def singleCWeights(self):
        cweights = []
        for rough in self.roughnesses:
            cweight = roughness2CWeight(
                rough, a=self.a, b=self.b, c=self.c, Min=0,  # self.cmin,
                Max=self.cmax)
            log.debug([np.min(cweight), np.max(cweight)])
            cweights.append(cweight)

        return cweights

    def updateConstraintWeights(self):
        """
        Set new constraint weights based on the actual model roughnesses.
        """
        log.debug('SCCI: updateConstraintWeights()')
        # write down single c weights
        single_weights = np.array(self.singleCWeights())
        log.debug('single c weights before updateConstraintWeights: {}'
                  .format(single_weights))

        new_cweight = np.ones_like(single_weights)

        # each new c weight consists of the combination of cweights of all
        # other parameters
        for ipar in range(self.npara):
            all_others = np.arange(self.npara) != ipar
            log.debug('SCCI: all others: {}'.format(all_others))
            new_cweight[all_others] *= single_weights[ipar]

        # cut extreme values according to cmin, cmax
        new_cweight = np.minimum(new_cweight, self.cmax)
        new_cweight = np.maximum(new_cweight, self.cmin)

        log.debug('min/max new c weight: {}/{}'
                  .format(np.min(new_cweight), np.max(new_cweight)))

        # set c weights
        total = 0
        matrices = []
        for iinv, inv in enumerate(self.invs):
            weight = []
            for j in range(self.nparas[iinv]):
                weight.extend(new_cweight[total])
                total += 1
            log.debug('SCCI: manager {}, weights {}'
                      .format(iinv, np.shape(weight)))
            try:
                inv.setCWeight(weight)  # pg 1.0
            except AttributeError:
                inv.inv.setCWeight(weight)  # pg 1.1

            try:
                matrices.append(inv.fop().constraints())
            except BaseException:
                matrices.append(inv.fop.constraints())

        return new_cweight, matrices

    def _gather(self, attribute, raise_error=False, default=None):
        """ Internal function.
        Gather variables from underlaying managers for convenience. """
        ret = []
        for mi, manager in enumerate(self.managers):
            if hasattr(manager, attribute):
                ret.append(getattr(manager, attribute))
            else:
                if raise_error:
                    raise AttributeError(
                        'Manager {} of type {} has no attribute "{}"'
                        .format(mi, type(manager), attribute))
                else:
                    ret.append(default)
        return ret

    def _gather_from_cpp(self, call, raise_error=False, default=None):
        """ Internal function.
        Gather variables from given callable from underlaying managers
        for convenience.
        """
        ret = []
        for mi, manager in enumerate(self.managers):
            if hasattr(manager, call):
                to_call = getattr(manager, call)
                if not callable(to_call):
                    if raise_error:
                        raise TypeError(
                           '{} object of manager {} of type {} is not'
                           ' callable'
                           .format(type(to_call), mi, type(manager)))
                    else:
                        ret.append(default)
                else:
                    ret.append(to_call())
            else:
                if raise_error:
                    raise AttributeError(
                        'Manager {} of type {} has no function "{}"'
                        .format(mi, type(manager), call))
                else:
                    ret.append(default)
        return ret


def roughness2CWeight(vec, a=0.1, b=0.1, c=1.0, Max=1.0, Min=None):
    """
    Structural constraint weight as function of roughness. See
    Günther et al. (2010, SAGEEP extended abstract) (case a>0) for details.
    """
    if Min is None:
        Min = b
    avec = np.absolute(vec)
    cfun = (a / (avec + a) + b)**c
    # confine between min and max
    cfun = np.minimum(cfun, Max)
    cfun = np.maximum(cfun, Min)
    return cfun


def getNormalizedRoughness(mesh, model, trans='log', tmin=None, tmax=None):
    """ Returns normalized roughness of the model with respect to the given
    mesh and transformation.

    The roughness is defined on each cell boundary with connection to another
    cell (so not for cells at the boundary of the mesh), so the number of
    values is not number of nodes nor number of cells or boundaries and
    strongly depends on the mesh.
    """
    # Step 1: init of mesh, fop and inv
    # we just need the methods of the inv and fop instances, so no input needed
    mesh.createNeighbourInfos()

    fop = pg.core.ModellingBase()
    fop.setMesh(mesh)

    # pseudo data, fop, verbose, debug
    inv = pg.core.Inversion([1, 2, 3], fop, True, False)

    # Step 2: take care of transformation
    if trans.lower() == 'log':
        if tmin is not None and tmax is not None:
            transmodel = pg.core.TransLogLU(tmin, tmax)
        else:
            transmodel = pg.core.TransLogLU()

    elif trans.lower() == 'cot':
        if tmin is None:
            tmin = 0

        if tmax is None:
            tmax = 0.7

        transmodel = pg.core.TransCotLU(tmin, tmax)

    else:
        raise Exception(
            'Transformation not implemented, expect "log" or "cot", not "{}".'
            .format(trans))

    # Step 3: region manager initialization (enables use of createConstraints)
    inv.setTransModel(transmodel)
    fop.regionManager()
    fop.createConstraints()

    # Step 4: with active constraints for the mesh, we simply get the roughness
    roughness = inv.pureRoughness(model)

    # return as numpy array
    return np.asarray(roughness)

# The End
