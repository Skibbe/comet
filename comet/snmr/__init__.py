"""
Module comet/snmr
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# from . autotest import *
# from . example import *
from . import misc
from . import kernel
from . kernel import Kernel
from . import modelling
from . import survey
from . survey import Survey, FID, Earth
from . import scci
import logging

log = logging.getLogger('comet')

# see all active logger
# comet.log.manager.loggerDict

# log.setLevel(logging.WARNING)
# CRITICAL  = 50, errors that may lead to data corruption and suchlike
# ERROR     = 40, things that go boom
# WARNING   = 30, things that may go boom later
# INFO      = 20, information of general interest
# PROGRESS  = 16, what's happening (broadly)
# TRACE     = 13, what's happening (in detail)
# DBG       = 10  sundry

# The End
