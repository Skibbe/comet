"""
Part of comet (and MRSMatlab... kinda)

Some utility funtions from mrsmatlab especially concerning discretization to
simplify the validation of kernel routines.

"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from comet.pyhed import log
import numpy as np


def sinh_sample(lmin, lmax, nlay):
    """ [SAMP,f2,A,B] = sinhSample(LMIN,LMAX,NLAY)

    Input:
    ------

        lmin:
            the first sample line

        lmax:
            last sample line

        nlay:
            number of sampling

    Output:
    -------

        samp:
            Sampling vector

        f2:
            Vector of ABcosh(Bi) values

        a, b:
            Coefficients
    """

    samp = None
    a = None
    b = None

    n1 = nlay - 1
    n2 = nlay - 2
    l0 = lmax / lmin

    fac = l0 ** (1/n2)  # initial asymptotic factor f
    f2 = 1. / (fac * fac)

    # compute asymptotic value f for detemination of A and B
    for i in range(10):
        # LL = L0*(1-F2)/(1-F2^N1);   %LL = y = f^(n-1)
        # FAC = LL^(1/N2);  %f
        # F2 = 1/(FAC*FAC);
        ll = l0 * (1 - f2) / (1 - (f2**n1))  # ll = y = f^(n-1)
        fac = ll ** (1 / n2)  # f
        f2 = 1. / (fac * fac)

    # define A and B
    samp = np.zeros(nlay)  # check shavad
    f2 = np.zeros(nlay)

    b = np.log(fac)
    a = lmin / np.sinh(b)

    for n in range(nlay):
        # SAMP(1,in) = A*sinh(B*(in-1));
        # f2(1,in) = A*B*cosh(B*(in-1));
        samp[n] = a * np.sinh(b * n)
        f2[n] = a * b * np.cosh(b * n)

    return (samp, f2, a, b)


def make_x_vec(a, z, dz, rmax):
    """
    % Discritization using sinh function
    % Input:
    %  a	       square loop side
    %  LMIN_IN     first sample close to wire
    %  NLAY_IN     number of samples insdie the loop (between wire and center)
    %  LMAX_OUT    maximum doscritization limit outside the loop
    %  Output:
    %  x_vec       vector of discrete points from loop center to outside limit.
    """
    x0 = None
    dx = None

    lmin_in = dz / 10
    lmax_out = max(a*3, rmax)

    if z > a:
        nlay_in = 11
    elif z > a/5:
        nlay_in = 21
    else:
        nlay_in = 31

    h = a/2
    lmax_in = h  # the last point inside the loop (at center)

    # sample vector inside and outside of the right loop side
    # [SAMP_IN,f2_IN,A,B] = sinhSample(LMIN_IN,LMAX_IN,NLAY_IN+1);
    samp_in, f2, a, b = sinh_sample(lmin_in, lmax_in, nlay_in + 1)
    nlay_out = nlay_in

    # add point close to loop center
    # SAMP_IN    = [SAMP_IN(1:end-1) SAMP_IN(end)-0.01 SAMP_IN(end)];
    # SAMP_IN_av = (SAMP_IN(2:end)+SAMP_IN(1:end-1))/2;
    # Dx1 = SAMP_IN(2:end)-SAMP_IN(1:end-1);
    samp_in = np.concatenate((samp_in[:-1], [samp_in[-1] - 0.01, samp_in[-1]]))
    # !!! check +- 0.01
    samp_in_av = (samp_in[1:] + samp_in[:-1]) / 2
    dx1 = samp_in[1:] - samp_in[:-1]

    samp_out = samp_in

    # extend outside discritization to the next(s) 10th.
    # Number of samples has to be K*10+1 for 11 point formula

    for i in range(5):
        if samp_out[len(samp_out) - 1] < lmax_out:
            nlay_out = nlay_out + 10
            samp_out = np.zeros(nlay_out + 1)

            for n in range(nlay_out + 1):
                samp_out[n] = a * np.sinh(b * n)

    # SAMP_OUT_av = (SAMP_OUT(2:end)+SAMP_OUT(1:end-1))/2;
    # Dx2 = SAMP_OUT(2:end)-SAMP_OUT(1:end-1);
    samp_out_av = (samp_out[1:] + samp_out[:-1]) / 2
    dx2 = samp_out[1:] - samp_out[:-1]

    sampin_r = h - samp_in_av
    sampout_r = h + samp_out_av

    # x0 = [fliplr(SAMPIN_R) SAMPOUT_R]';
    # Dx = [fliplr(Dx1) Dx2]';
    x0 = np.concatenate((sampin_r[::-1], sampout_r))
    dx = np.concatenate((dx1[::-1], dx2))
    return x0, dx


def make_z_vec(nz, zmax, sinh_zmin, case='sinh', dz=1):
    """


    Parameters
    ----------
    nz : integer
        Number of layers.
    zmax : float
        Maximum depth. Note the singchange compared to MRSMatlab, Comet
        defines z negative downwards. The return array is in Comet definition.
    sinh_zmin : float
        DESCRIPTION. MRSMAtlab default: data.loop.size/200
    case : string, optional
        Type of the discretization, options are "sinh" or "lin".
        The default is 'sinh'. MRSMatlab's loglin is currently not
        implemented.
    dz : float, optional
        Thickness of first layer for log/lin. The default is 1.

    Raises
    ------
    Exception
        DESCRIPTION.

    Returns
    -------
    z : TYPE
        DESCRIPTION.
    Dz : TYPE
        DESCRIPTION.

    """
    if case.lower() == 'sinh':
        # case 1 sinh discritization after original implementation
        # of Ahmad Behroozmand

        # nl_temp = model.nz+1;
        # N1 = nl_temp-1; %n=NLAY-1
        # N2 = nl_temp-2;
        # L0 = model.zmax/model.sinh_zmin;
        # FAC = L0^(1/N2);      % initial asymptotic factor f
        # F2 = 1/(FAC*FAC);     %f^-2
        nl_temp = nz + 1
        n1 = nl_temp - 1
        n2 = nl_temp - 2
        l0 = zmax / sinh_zmin
        fac = l0 ** (1/n2)
        f2 = 1. / (fac * fac)

        # % compute asymptotic value f for detemination of A and B
        # test = zeros(1,11);
        # test(1) = FAC;
        # for i = 1:10
        #     LL = L0*(1-F2)/(1-F2^N1);   %LL = y = f^(n-1)
        #     FAC = LL^(1/N2);  %f
        #     test(i+1) = FAC;
        #     F2 = 1/(FAC*FAC);
        # end
        test = np.zeros(11)
        test[0] = fac

        for i in range(10):
            ll = l0 * (1 - f2) / (1 - (f2 ** n1))  # ll = y = f^(n-1)
            fac = ll ** (1 / n2)  # f
            test[i + 1] = fac
            f2 = 1. / (fac * fac)

        # % define A and B
        # SAMP = zeros(1,nl_temp); %check shavad
        # f2 = zeros(1,nl_temp);
        samp = np.zeros(nl_temp)
        f2 = np.zeros(nl_temp)

        # B = log(FAC);
        # A = model.sinh_zmin/sinh(B);
        b = np.log(fac)
        a = sinh_zmin / np.sinh(b)

        # for in = 1:nl_temp
        #     SAMP(1,in) = A*sinh(B*(in-1));
        #     f2(1,in)   = A*B*cosh(B*(in-1));
        # end
        # z_vec = SAMP;
        for n in range(nl_temp):
            samp[n] = a * np.sinh(b * n)
            f2[n] = a * b * np.cosh(b * n)
        zvec = samp

        # model.z  = (z_vec(2:end)+z_vec(1:end-1))/2;
        # model.Dz = z_vec(2:end)-z_vec(1:end-1)
        z = (zvec[1:] + zvec[:-1]) / 2
        Dz = zvec[1:] - zvec[:-1]

    elif case.lower() == 'loglin':
        raise Exception('loglin not yet implemented. Please try "sinh" or'
                        ' "lin".')

    elif case.lower() == 'lin':
        # layer    = 0:model.dz:model.zmax;
        # model.z  = (layer(1:end-1)+layer(2:end))/2;
        # model.Dz = layer(2:end)-layer(1:end-1);
        zvec = np.arange(0, zmax + dz, dz)
        z = (zvec[1:] + zvec[:-1]) / 2
        Dz = zvec[1:] - zvec[:-1]

    else:
        raise Exception(
            'case "{}" not recognized. Please try "sinh" or "lin".'
            .format(case))

    return z, Dz


def getMRSMatlabDefaultDict(loop):
    """
    Parameters
    ----------
    loop : ph.loop.Loop
        Loop class instance, mainly used for area and shape of the loop.

    Returns
    -------
    default : dictionary
        Dictionary containing MRSMatlab default values based on given loop.

    Contains
    --------
    nz: integer
        Number of layers. Default is 72.
    dh: float
         Default is 1.

    """
    default = {'dh': 1.0,
               'nz': 72,  # number of layers
               }
    if loop.type in ['square', 'rectangle']:
        edgelen = max(np.max(loop.pos[:, 0] - np.min(loop.pos[:, 0])),
                      np.max(loop.pos[:, 1] - np.min(loop.pos[:, 1])))
        default['zmax'] = 1.5 * edgelen

    else:
        default['zmax'] = 1.5 * loop._maxdistance

    return default

# def createKernelMesh():
#     return

# The End
