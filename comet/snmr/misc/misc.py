"""
Part of comet/snmr/misc
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import numpy as np


def rms(data_obs, data_calc):
    """
    Calculates the root-mean-square of the two given vectors:

    .. math:: rms=\sqrt{1/N\sum{(d_{obs}-d_{calc})^2}}

    **data_obs** and **data_calc** are supposed to be real valued vectors the
    same shape.

    """
    rms = np.sqrt(np.mean(np.abs(data_obs - data_calc)**2))
    return rms


def meanAbs(data_obs, data_calc):
    """
    Calculates the mean of the absolute of the difference of the two given
    vectors.

    .. math:: rms=1/N\sum{|(d_{obs}-d_{calc}})|


    **data_obs** and **data_calc** are supposed to be real valued vectors the
    same shape.
    """
    rms = np.mean(np.abs(data_obs - data_calc))
    return rms


def rotData(data, phi):
    """
    Rotates the given complex-valued NMR data set by the given angle phi.
    """
    absolute = np.absolute(data)
    phase = np.angle(data)

    if np.shape(phi) == ():
        PHI = phi  # float
    elif np.shape(data) != np.shape(phi):
        PHI = phi[:, np.newaxis]  # one val per pulse moment 1d array
    else:
        PHI = phi  # one val per pulse moment in correct 2d array

    rotreal = absolute * np.cos(phase - PHI)  # - wdft)
    rotimag = absolute * np.sin(phase - PHI)  # - wdft)

    rot_data = rotreal + 1j * rotimag
    return rot_data


def getDataPhase(data_obs, data_calc, return_rms=False):
    """
    Searches for the constant phase between two qt-data sets and returns the
    phase. After definition first given dataset is expected to be the observed
    (field, to be fitted) dataset, which has to be rotated.
    """
    testphases = np.linspace(-np.pi, np.pi, 361)[:-1]
    rms_array = np.ones(360)
    arr2 = np.append(data_calc.flatten().real, data_calc.flatten().imag)

    for t, tp in enumerate(testphases):
        # mind the minus sign!
        # remember we search for the phase, not the angle we have to
        # apply in order to rotate it back.
        data_obs_rot = rotData(data_obs, tp)
        arr1 = np.append(data_obs_rot.real,
                         data_obs_rot.imag)  # np.append also flatten
        rms_array[t] = rms(arr1, arr2)

    phi = testphases[np.argmin(rms_array)]

    if return_rms:
        return phi, rms_array
    else:
        return phi

def modelKozenyGodefroy(porosity, t2, tau=1.5, surface_relaxivity=50e-6,
                        temperature=10):
    """
    Evaluate hydraulic conductivity from NMR parameters.
    See Dlugosch 2014 dissertation eq. 3.13 or Dlugosch et al., 2013.

    porosity: float
        Porosity of media.

    t2: float
        Relaxation time of media.

    tau float [1.5]:
        τ [m/m] tortuosity defined as arc-chord ratio

    surface_relaxivity float [ 50 ]:
        ρ [10e-6 m/s] surface relaxivity

    temperature [ 10 ] in °C:
        Used to evaluate:

            rho [kg/m³] density (McCutcheon et al., 1993)

            t_b [s] bulk relaxation time (Dlugosch et al., 2013)

            η [Pas] dynamic viscosity (0 < temp < 100) from general literature

            D [m²/s] self-diffusion constant (Dunn et al., 2002)


    Further assuming g earth gravitational acceleration: 9.81 m/s²

    """
    from comet.pyhed import log

    kelvin = temperature + 273.15
    # density of water at temperature (McCutcheon et al., 1993)
    rho = 1000 * (1 - ((temperature + 288.94) / (508929 * (temperature + 68.12))) * (temperature - 3.98)**2)

    # bulk relaxation time at temperature (Dlugosch et al., 2013)
    # for temperatures between 5°C and 35°C
    t_b = 3.3 + 0.044 * (temperature - 35)

    # dynamic viscosity at temperature (0 < temp < 100)
    visc = 1 / (0.1 * kelvin**2 - 34.335 * kelvin + 2472)

    # self-diffusion constant (Dunn et al., 2002)
    D = 1.0413 + 0.039828 * temperature + 0.00040318 * temperature ** 2

    log.debug('temperature: {:.1f} °C'.format(temperature))
    log.debug('density: {:.3e} kg/m³'.format(rho))
    log.debug('bulk relaxation time: {:.3f} s'.format(t_b))
    log.debug('dyn. viscosity: {:.3e} Pas'.format(visc))
    log.debug('self-diffusion constant: {:.3f} m²/s'.format(D))

    # earth gravitational acceleration
    g = 9.81

    hlp1 = D / surface_relaxivity

    k_f = (rho * g) / (8 * tau**2 * visc) * porosity *\
        (-hlp1 + np.sqrt(hlp1**2 + (4 * D * t_b * t2) / (t_b - t2)))**2

    log.debug('hydraulic conductivity: {:.3e} m/s'.format(k_f))

    return k_f


def etraSourceDiscretization(x_min, x_max, small, segments=100):
    from custEM.meshgen import meshgen_utils as mu

    assert int(x_max - x_min) % small == 0, 'No Etra fits in there.'
    if segments % 4 != 0:
        raise Exception(
            'Error in "etra_source": "segments" needs to divisible by 4')

    loop_tx = []
    segments_large = (int(x_max - x_min) // int(small) - 1) * segments

    # transmitter
    loop_tx.append(mu.loop_r([x_min + small/2, -small/2],
                             [x_max - small/2, small/2], nx=segments_large,
                             ny=segments))

    # receiver
    for x in np.arange(x_min, x_max - small/2, small/2):
        loop_tx.append(mu.loop_r([x, -small/2], [x + small, small/2],
                       nx=segments, ny=segments))

    return loop_tx

# if __name__ == '__main__':
#     from comet import pyhed as ph
#     ph.log.setLevel(10)
#     modelKozenyGodefroy(0.38, 0.7, temperature=22)

#     ph.log.setLevel(20)

#     k = []
#     for t2 in np.arange(0.1, 2, 0.1):
#         k.append(modelKozenyGodefroy(0.38, t2, temperature=22))

#     import matplotlib.pyplot as plt
#     plt.loglog(np.arange(0.1, 2, 0.1), k)
#     plt.grid()


# The End
