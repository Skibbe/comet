"""
Part of comet/snmr/misc
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import numpy as np


def setAxSize(ax, size):
    for item in ([ax.title, ax.xaxis.label,
                  ax.yaxis.label] + ax.get_xticklabels() +
                 ax.get_yticklabels()):
        item.set_fontsize(size)


def setCBarSize(cbar, size):
    setAxSize(cbar.ax, size)


def grayCBarPalette(steps, lims=[1.0, 0.5]):
    color_palette = []
    colors = np.linspace(lims[0], lims[1], steps)
    for i in colors:
        color_palette.append((i, i, i))
    return color_palette


if __name__ == '__main__':
    pass

# The End
