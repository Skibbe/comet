"""
Module comet/snmr/misc
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from . IO_pdf import (exportKernelPDF, exportColorBarPDF, robustPDFSave,
                      returnFigure, closeAxis)

from . plotting_tools import setAxSize, setCBarSize, grayCBarPalette
from . plot_routines import drawSoundingCurve

from . misc import (rms, rotData, getDataPhase, meanAbs, modelKozenyGodefroy,
                    etraSourceDiscretization)

from . fromMRSMatlab import (make_x_vec, make_z_vec)

class Constants():
    @property
    def gamma(self):
        return 0.267518e9  # gamma = 2pi * gymagnetic ratio of protons

    def calcCurieFactor(self, temperature):
        """

        """
        import numpy as np
        # append factor of Curie formula to the integral
        # calculate Curie formula M0
        mass_H = 1.00794  # [g/mol]
        mass_O = 15.9994  # [g/mol]
        # unflexible, not further altered
        # dichte_20Grad = 998.20  # [kg/m³]
        density_10degree = 999.70  # [kg/m³]  # HACK fixed for 10 °C
        Avogadro = 6.002140857e23  # [mol^-1]
        Boltzmann = 1.38064852e-23  # [J/K]
        Planck = 6.62606957e-34 / (2. * np.pi)  # [Js]
        # Curie formula
        # number of spins per unit volume [1/m³] (2 per hydrogen atom)[1]
        N = 2. * density_10degree * 1000. / (2. * mass_H + mass_O) * Avogadro
        # print(N)  # check 6.65e28
        curie = N * self.gamma**2 * Planck**2 / \
            (4. * Boltzmann * temperature)
        # factor check 3.27e-3
        # print('curie factor %0.2e' % (factor))
        return curie


constants = Constants()
# The End
