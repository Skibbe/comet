"""
Part of comet/snmr/misc
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import numpy as np
from comet.pyhed import log
from comet.pyhed.plot import setAxSize


def drawSoundingCurve(ax, kern_mat, pulses, size=12, color='r',
                      to_plot=['abs'], y_ticks_right=True, plot_abs=True,
                      title='volume-integrated kernel', marker_size=5,
                      **kwargs):
    """
    """
    if kern_mat.shape[0] == len(pulses) and len(kern_mat.shape) == 2:
        kern_mat2 = np.sum(kern_mat, 1)
    else:
        kern_mat2 = kern_mat

    item_plot = np.absolute(kern_mat2)
    ret = ()
    legend_handler = []
    legend_label = []

    if 'abs' in to_plot:
        Abs, = ax.plot(item_plot * 1e9, pulses, linestyle='', marker='o',
                       color=color, markersize=marker_size, **kwargs)
        legend_handler.append(Abs)
        legend_label.append('abs')
        ret += (Abs,)

    if 'real' in to_plot:
        Real, = ax.plot(np.real(kern_mat2) * 1e9, pulses, color=color,
                        linestyle='-', **kwargs)
        legend_handler.append(Real)
        legend_label.append('real')
        ret += (Real,)

    if 'imag' in to_plot:
        Imag, = ax.plot(np.imag(kern_mat2) * 1e9, pulses, color=color,
                        linestyle='--', **kwargs)
        legend_handler.append(Imag)
        legend_label.append('imag')
        ret += (Imag,)

    plt.legend(legend_handler, legend_label)

    ax.set_xlabel('Voltage (nV)')
    if not ax.yaxis_inverted():
        ax.invert_yaxis()
    ax.set_ylabel('Pulse moments (As)')
    if y_ticks_right:
        ax.yaxis.tick_right()
        ax.yaxis.set_label_position("right")
    ax.grid(True)
    if title is not None:
        ax.set_title(title)

    if 'real' in to_plot or 'imag' in to_plot:
        eval_minmax = np.column_stack([item_plot, np.real(kern_mat2),
                                       np.imag(kern_mat2)])
        start = np.min(eval_minmax * 1e9)
        end = np.max(eval_minmax * 1e9)
    else:
        start = np.min(item_plot * 1e9)
        end = np.max(item_plot * 1e9)

    # determine xticks in 0D plot and conf axis
    fixed = 500
    start_mult = start // fixed
    end_mult = end // fixed

    mirror = None
    if start_mult < 0:
        mirror = start_mult - 1
        start_mult = 0

    mult = end_mult - start_mult
#    mult = (end - start) // fixed  # tick rounded to 500
    div = 5  # 5 ticks per plot
    if end_mult > 19:
        div = 4  # if max > 10000, only 4 ticks per plot

    while mult == 0:  # end smaller than current fixed
        fixed /= 10
        start_mult = start // fixed
        end_mult = end // fixed
        mult = end_mult - start_mult

    # calc ticks value
    mult5 = max(mult // div, 1)

    # set ticks
    if not mirror:
        ax.set_xticks(
            np.arange(start_mult * fixed, end, mult5 * fixed))
    else:
        positive = np.arange(0, end, mult5 * fixed)
        negative = (np.arange(0, mirror * fixed, -mult5 * fixed))
        ax.set_xticks(np.unique(np.append(negative[::-1], positive)))

    setAxSize(ax, size)
    return ret


if __name__ == '__main__':
    pass

# The End
