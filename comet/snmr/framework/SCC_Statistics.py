"""
Part of comet/snmr/framework

Class for SNMR+ERT specific visualization of SCC Inversion results.
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# from comet.pyhed.IO import checkDirectory
from comet.snmr.misc import robustPDFSave, returnFigure, grayCBarPalette

try:  # pg.__version__ < 1.1
    from pygimli.mplviewer import drawModel1D
except ImportError:  # pg.__version__ >= 1.1
    from pygimli.viewer.mpl import drawModel1D


class SCCStatistics(object):
    def __init__(self, directory=None, **kwargs):
        self.setFontSize(kwargs.pop('fontsize', 20))
        self.directory = directory
        self.fig_size = [21, 7]
        self.xlim_water = [0.05, 0.5]
        self.xlim_res = [1, 2000]
        self.xlim_t2 = [0.01, 1]
        self.ylim = None

    def __str__(self):
        return self.__class__.__name__

    def __repr__(self):
        return self.__str__()

    def setFontSize(self, size):
        self.font_size = size
        matplotlib.rcParams.update({'font.size': self.font_size})

    def _loadOrSet(self, ndarray_or_string, encoding='ASCII'):
        if isinstance(ndarray_or_string, str):
            return np.load('{}/{}'.format(self.directory, ndarray_or_string),
                           encoding=encoding)
        else:
            return ndarray_or_string

    def _savepdf(self, fig, ax, name, half_ticker=False):
        if half_ticker:
            # import matplotlib.ticker as ticker
            for label in ax[2].get_xticklabels(which='both')[1::2]:
                label.set_visible(False)
            for label in ax[0].get_xticklabels(which='both')[0::2]:
                label.set_visible(False)
            # ax[0].xaxis.set_minor_formatter(ticker.FormatStrFormatter('%.0f'))
        self.savePDF(name, fig)
        return fig, ax

    def savePDF(self, name, fig_or_ax):
        robustPDFSave(fig_or_ax, name)

    def loadStatistics(self, basename, encoding='ASCII'):
        base = '{}/'.format(self.directory) + basename
        print('load: {}'.format(base))
        if os.path.exists(base + '.npy'):
            # old
            print('found: npy')
            raw = np.load(base + '.npy', encoding=encoding)
            return_ = raw[:, :4], raw[:, 4:]

        elif os.path.exists(base + '.npz'):
            # new
            raw = np.load(base + '.npz', encoding=encoding)
            print('found: npz')
            print(raw['basic'].shape)
            print(raw['results'].shape)

            return_ = raw['basic'], raw['results']
        else:
            raise Exception(
                'Did not found file: "{}", searched for npy and npz.'
                .format(base))
        print(return_)
        return return_

    def loadResults(self,
                    names=['tem', 'mrs'],
                    z_thk='z_thk.npy',
                    c_weight_history='c_weight_history.npy',
                    scc_parameters='scc_parameters.npy',
                    statistics='statistics_{}',
                    synthetic_or_field='synthetics_{}.npy',
                    synthetic_model='synthetic_model.npy'):
        if self.directory is None:
            return
        print('load from dir: {}'.format(self.directory))
        self.z_thk = self._loadOrSet(z_thk)
        self.c_weight_history = self._loadOrSet(c_weight_history)
        self.scc_parameters = self._loadOrSet(scc_parameters)

        self.stats_ves, self.results_ves = self.loadStatistics(
            statistics.format(names[0]), encoding='latin1')
        self.stats_mrs, self.results_mrs = self.loadStatistics(
            statistics.format(names[1]), encoding='latin1')
        self.synthetic_data_ves = self._loadOrSet(
                                          synthetic_or_field.format(names[0]),
                                          encoding='latin1')
        self.synthetic_data_mrs = self._loadOrSet(
                                          synthetic_or_field.format(names[1]),
                                          encoding='latin1')
        try:
            self.synthetic_model = self._loadOrSet(synthetic_model,
                                                   encoding='latin1')
            if isinstance(self.synthetic_model[0], list):
                self.synthetic_model[0].append(99)
                for i in range(1, len(self.synthetic_model)):
                    self.synthetic_model[i].append(self.synthetic_model[i][-1])
            else:
                self.synthetic_model[0] = np.append(self.synthetic_model[0],
                                                    [99])
                for i in range(1, len(self.synthetic_model)):
                    self.synthetic_model[i] = np.append(
                        self.synthetic_model[i], self.synthetic_model[i][-1])

        except FileNotFoundError:
            self.synthetic_model = None

        # self.printSCCParas()
        self.coupling_case = self.scc_parameters[1]
        self.max_index = len(self.stats_ves) - 1

    def printSCCParas(self):
        print('cWeight Parameter (a, b, c, Min, Max): {}'.format(
                self.scc_parameters[0]))
        print('coupling Case: {}'.format(self.scc_parameters[1]))
        print('abort pre coupling inversion after: {}'
              .format(self.scc_parameters[2]))
        print('prec max delta Phi {}'.format(self.scc_parameters[3]))
        if self.scc_parameters[4] is not None:
            print('lambda VES: {}'.format(self.scc_parameters[4][0]))
            print('lambda MRS: {}'.format(self.scc_parameters[4][1]))

    def showChiAndRMS(self, to_plot=['tem', 'mrs'], suptitle='default'):
        for method in to_plot:
            if method == 'mrs':
                stats = self.stats_mrs
                def_sup = 'MRS statistics'
            else:
                stats = self.stats_ves
                def_sup = 'VES statistics'

            if suptitle == 'default':
                used_suptitle = def_sup
            else:
                used_suptitle = suptitle

            fig, ax = plt.subplots(3, 1, sharex=True)
            # 0 chi², 1 relRMS, 2 absRMS, 3 iter
            ax[0].plot(stats[:, 3], stats[:, 0], '.', label='chi2')
            ax[1].plot(stats[:, 3], stats[:, 1], '.', label='relrms')
            ax[2].plot(stats[:, 3], stats[:, 2], '.', label='absrms')
            for j in range(3):
                ax[j].plot(stats[0, 3], stats[0, j], 'ro',
                           label='smooth result')
            for a in ax:
                a.legend(loc='best')
            ax[2].set_xlabel('number of iterations')
            fig.suptitle(used_suptitle)

    def show(self, model, nPara=1, ax=None,
             label='cWeight',
             xlabel='weighting factor (1)', log=False, **kwargs):

        no_legend = kwargs.pop('no_legend', False)
        ylim = kwargs.pop('ylim', [np.sum(self.z_thk), 0.])
        if ax is None:
            if self.coupling_case == 0:
                fig, ax = plt.subplots(1, 1, figsize=(8, 6))
            else:
                fig, ax = plt.subplots(1, nPara, figsize=(8, 6))

        if nPara == 1:
            if log is True:
                drawModel1D(ax, thickness=self.z_thk,
                            values=model[0], label=label,
                            plot='semilogx', **kwargs)
                niceLogTicks(ax, model)
            else:
                drawModel1D(ax, thickness=self.z_thk,
                            values=model[0], label=label,
                            **kwargs)
            ax.set_xlabel(xlabel)
            ax.grid(True, which='both')
            ax.legend(loc='best')
            ax.set_ylim(ylim)

        else:
            if log is False:
                log = [False] * nPara
            for i in range(nPara):
                if log[i] is True:
                    drawModel1D(ax[i],
                                thickness=self.z_thk, values=model[i],
                                label=label[i],
                                plot='semilogx',
                                **kwargs)
                    niceLogTicks(ax[i], model[i])

                else:
                    drawModel1D(ax[i],
                                thickness=self.z_thk, values=model[i],
                                label=label[i],
                                **kwargs)
                ax[i].set_xlabel(xlabel[i])
                ax[i].grid(True, which='both')
                if not no_legend and label[i] is not None:
                    ax[i].legend(loc='best')
                ax[i].set_ylim(ylim)

        return ax

    def showSynthetics(self, ax=None, syncolor=None, savename=None,
                       close_after=True, labels=['synthetic', None, None],
                       **kwargs):
        ylim = kwargs.pop('ylim', [np.sum(self.z_thk), 0.])
        if ax is None:
            fig, ax = plt.subplots(1, 3, sharey=True,
                                   figsize=kwargs.pop('fig_size',
                                                      self.fig_size))
        if self.synthetic_model is not None:
            syncolorkwarg = {}
            if syncolor is not None:
                syncolorkwarg['color'] = syncolor
            drawModel1D(ax[0], thickness=self.synthetic_model[0],
                        values=self.synthetic_model[1], label=labels[0],
                        plot='semilogx', **syncolorkwarg)
            niceLogTicks(ax[0], self.synthetic_model[1])
            drawModel1D(ax[1], thickness=self.synthetic_model[0],
                        values=self.synthetic_model[2], label=labels[1],
                        **syncolorkwarg)
            drawModel1D(ax[2], thickness=self.synthetic_model[0],
                        values=self.synthetic_model[3], label=labels[2],
                        plot='semilogx', **syncolorkwarg)
            niceLogTicks(ax[2], self.synthetic_model[3])
        else:
            print('No synthetic model found.')

        ax[1].set_ylabel('')
        ax[2].set_ylabel('')

        ax[0].set_xlim(self.xlim_res)
        ax[1].set_xlim(self.xlim_water)
        ax[2].set_xlim(self.xlim_t2)

        ax[0].set_xlabel('resistivity (Ohmm)')
        ax[1].set_xlabel('water content (1)')
        ax[2].set_xlabel('relaxation time (s)')
        ax[0].grid(linestyle='--')
        ax[0].grid(linestyle='--', which='minor')
        ax[1].grid(linestyle='--')
        ax[2].grid(linestyle='--')
        ax[2].grid(linestyle='--', which='minor')

        for axis in ax:
            axis.set_ylim(ylim)
            axis.legend()
        if savename is not None:
            self._savepdf(fig, ax, savename)
        if close_after is True:
            plt.close(fig)
        else:
            return ax

    def showModels(self, index, ax=None, syn=False, label=None,
                   savename=None, suptitle=None, syncolor=None, **kwargs):

        close_after = kwargs.pop('close_after', False)
        ylim = kwargs.pop('ylim', [np.sum(self.z_thk), 0.])
        if index < 0 or index > self.max_index:
            if index == -1:
                index = self.max_index
            else:
                print('Choose 0 <= index < {}'.format(self.max_index))
                return
        # xlim = kwargs.pop('xlim', None)
        if ax is None:
            fig, ax = plt.subplots(1, 3, sharey=True,
                                   figsize=kwargs.pop('fig_size',
                                                      self.fig_size))
        else:
            try:
                fig = ax.figure
            except AttributeError:
                fig = ax[0].figure

        if syn is True:
            if self.synthetic_model is not None:
                syncolorkwarg = {}
                if syncolor is not None:
                    syncolorkwarg['color'] = syncolor
                drawModel1D(ax[0], thickness=self.synthetic_model[0],
                            values=self.synthetic_model[1], label='synthetic',
                            plot='semilogx', **syncolorkwarg)
                drawModel1D(ax[1], thickness=self.synthetic_model[0],
                            values=self.synthetic_model[2], label='synthetic',
                            **syncolorkwarg)
                drawModel1D(ax[2], thickness=self.synthetic_model[0],
                            values=self.synthetic_model[3], label='synthetic',
                            plot='semilogx', **syncolorkwarg)
            else:
                print('No synthetic model found.')
        if label is None:
            label = 'iter: {} ({})'.format(
                    self.stats_ves[index][3],
                    self.stats_ves[index][3] - self.stats_ves[0][3])
            label = [label, label, label]

        self.show(self.results_ves[index][0], ax=ax[0],
                  label=label[0],
                  xlabel='resistivity (Ohmm)', log=True, **kwargs)

        self.show(self.results_mrs[index][0], ax=ax[1:],
                  label=[label[1], label[2]],
                  xlabel=['water content (1)',
                          'relaxation time (s)'],
                  nPara=2, log=[False, True], **kwargs)
        ax[0].set_ylabel('depth (m)')
        ax[1].set_ylabel('')
        ax[2].set_ylabel('')

        ax[0].set_xlim(self.xlim_res)
        ax[1].set_xlim(self.xlim_water)
        ax[2].set_xlim(self.xlim_t2)

        ax[0].set_xlabel('resistivity (Ohmm)')
        ax[1].set_xlabel('water content (1)')
        ax[2].set_xlabel('relaxation time (s)')
        ax[0].grid(linestyle='--')
        ax[0].grid(linestyle='--', which='minor')
        ax[1].grid(linestyle='--')
        ax[2].grid(linestyle='--')
        ax[2].grid(linestyle='--', which='minor')

        for axis in ax:
            axis.set_ylim(ylim)

        # ax[0].set_ylim(50, 0)
        if suptitle is not None:
            ax[0].figure.suptitle(suptitle, fontsize=36)
        if savename is not None:
            self._savepdf(fig, ax, savename)
        if close_after is True:
            plt.close(fig)
        else:
            return ax

    def showChi2(self, savename=None, close_after=False, one_plot=False):
        if one_plot is False:
            fig, ax = plt.subplots(1, 2)
            min1 = np.min(self.stats_ves[:, 3]) - 0.5
            min2 = np.min(self.stats_mrs[:, 3]) - 0.5
            max1 = np.max(self.stats_ves[:, 3]) + 0.5
            max2 = np.max(self.stats_mrs[:, 3]) + 0.5
            ax[0].plot(self.stats_ves[:, 3],
                       self.stats_ves[:, 0], '.', label='$\chi{^2}_{VES}$')
            ax[1].plot(self.stats_mrs[:, 3],
                       self.stats_mrs[:, 0], '.', label='$\chi{^2}_{MRS}$')

            ax[0].plot([min1, max1], [1, 1], 'r-')
            ax[1].plot([min2, max2], [1, 1], 'r-')
            ax[0].set_xlim(min1, max1)
            ax[1].set_xlim(min2, max2)
            ax[0].legend(loc='best')
            ax[1].legend(loc='best')
        else:
            fig, ax = plt.subplots(1, 1)
            min1 = np.min(self.stats_ves[:, 3]) - 0.5
            min2 = np.min(self.stats_mrs[:, 3]) - 0.5
            mina = np.min([min1, min2])
            max1 = np.max(self.stats_ves[:, 3]) + 0.5
            max2 = np.max(self.stats_mrs[:, 3]) + 0.5
            maxa = np.max([max1, max2])
            ax.plot(self.stats_ves[:, 3],
                    self.stats_ves[:, 0], '.', label='$\chi{^2}_{VES}$')
            ax.plot(self.stats_mrs[:, 3],
                    self.stats_mrs[:, 0], '.', label='$\chi{^2}_{MRS}$')

            ax.plot([mina, maxa], [1, 1], 'r-')
            ax.set_xlim(mina, maxa)
            ax.legend(loc='best')
        if savename is not None:
            robustPDFSave(fig, savename)
        if close_after:
            plt.close(fig)

    def returnFigureAndAx(self, *args, **kwargs):
        ax = kwargs.pop('ax', None)
        fig_size = kwargs.pop('figsize', self.fig_size)
        if ax is None:
            fig, ax = plt.subplots(*args, figsize=fig_size, **kwargs)
        else:
            fig = returnFigure(ax)
        return fig, ax

    def showCWeights(self, index, savename=None, ax=None,
                     suptitle=None, close_after=False):
        if index < 1 or index > self.max_index:
            if index == -1:
                index = self.max_index
            else:
                print('Choose 0 < index < {}'.format(self.max_index))
                return

        fig, ax = self.returnFigureAndAx(1, 3, sharey=True, ax=ax)

        to_ves = self.results_ves[index][2]
        to_mrs = self.results_mrs[index][2]

        self.show(to_ves, ax=ax[0], label=r'$C_\rho$ ({})'.format(index),
                  xlabel='constraint weight (1)')
        self.show(to_mrs, ax=ax[1:], nPara=2,
                  label=[r'$C_\theta$ ({})'.format(index),
                         r'$C_{T_2^*}$ (%d)' % (index)],
                  xlabel=['constraint weight (1)',
                          'constraint weight (1)'])
        wanted = self.c_weight_history[index-1]
        labels = [r'$C_\theta$ * $C_{T_2^*}$',
                  r'$C_\rho$ * $C_{T_2^*}$',
                  r'$C_\rho$ * $C_\theta$']
        for i, a in enumerate(ax):
            if isinstance(wanted[i], np.ndarray):
                toshow = wanted[i]
                label = labels[i]
            else:
                toshow = wanted
                label = '$C_{common}$'
            self.show(toshow, ax=a, label=label,
                      xlabel='constraint weight (1)')
        ax[0].set_ylabel('depth (m)')
        ax[1].set_ylabel('')
        ax[2].set_ylabel('')
        if suptitle is not None:
            fig.suptitle(suptitle, fontsize=36)
        if savename is not None:
            robustPDFSave(fig, savename)
        if close_after:
            plt.close(fig)
        else:
            return ax

    def showRoughness(self, index=1, ax=None, savename=None, suptitle=None,
                      **kwargs):
        if index < 1 or index > self.max_index:
            if index == -1:
                index = self.max_index
            else:
                print('Choose 0 < index < {}'.format(self.max_index))
                return
        if ax is None:
            fig, ax = plt.subplots(1, 3, sharey=True, figsize=self.fig_size)
        else:
            try:
                fig = ax.get_figure()
            except AttributeError:
                fig = ax[0].get_figure()

        self.show(self.results_ves[index][1], ax=ax[0],
                  label=r'$R_\rho$',
                  xlabel='model roughness (1)', **kwargs)
        self.show(self.results_mrs[index][1], ax=ax[1:],
                  nPara=2,
                  label=[r'$R_\theta$', '$R_{T_2^*}$'],
                  xlabel=['model roughness (1)',
                          'model roughness (1)'],
                  **kwargs)
        ax[0].set_ylabel('depth (m)')
        ax[1].set_ylabel('')
        ax[2].set_ylabel('')

        if suptitle is not None:
            fig.suptitle(suptitle, fontsize=36)
        if savename is not None:
            robustPDFSave(fig, savename)
        if kwargs.pop('close_after', False) is True:
            plt.close(fig)
        else:
            return ax

    def showStartMiddleEnd(self, save_directory='.', close_after=False):
        if save_directory is None:
            save_directory = self.directory
        self.showModels(0, syn=True,
                        label=['smooth inversion', 'smooth inversion',
                               'smooth inversion'],
                        savename=save_directory + '/syntheticModel',
                        suptitle='single inversion results',
                        syncolor='#1f77b4', color='#ff7f0e',
                        close_after=close_after)

        self.showModels(1, syn=True,
                        label=['first coupling', 'first coupling',
                               'first coupling'],
                        savename=save_directory + '/firstCouplingModel',
                        suptitle='inversion results after first coupled step',
                        syncolor='#1f77b4', color='#ff7f0e',
                        close_after=close_after)
        self.showModels(self.max_index, syn=True,
                        label=['final', 'final', 'final'],
                        savename=save_directory + '/finalCouplingModel',
                        suptitle='end results of the coupled inversion',
                        syncolor='#1f77b4', color='#ff7f0e',
                        close_after=close_after)

    def showAllModelSteps(self, suptitle=None, name_suffix='',
                          close_after=False,
                          smooth_label=['smooth inversion',
                                        'smooth inversion',
                                        'smooth inversion'],
                          label=['after coupled step {}',
                                 'after coupled step {}',
                                 'after coupled step {}']):
        for i in range(self.max_index):
            fig, ax = plt.subplots(1, 3, sharey=True, figsize=self.fig_size)
            self.showModels(0, syn=True, ax=ax,
                            label=smooth_label,
                            syncolor='#1f77b4', color='#ff7f0e')
            this_label = []
            for j in range(3):
                try:
                    this_label.append(label[j].format(i + 1))
                except AttributeError:
                    this_label.append(None)

            self.showModels(
                i + 1, ax=ax,
                label=this_label,
                savename=self.directory + '/{}Step{}'.format(
                        name_suffix, i + 1),
                color='#2ca02c',
                close_after=close_after)

    def showAllInOne(self, ax=None, suptitle=None, savename=None,
                     close_after=False, smooth_label=[
                         'smooth', None, None],
                     label=['SCCI', None, None], **kwargs):
        grays = grayCBarPalette(self.max_index, lims=[0.8, 0.4])
        if ax is None:
            fig, ax = plt.subplots(1, 3, sharey=True, figsize=self.fig_size)
        ax = self.showModels(0, syn=True, ax=ax,
                             label=smooth_label,
                             syncolor=kwargs.pop('syncolor', '#1f77b4'),
                             color=kwargs.pop('color', '#ff7f0e'), **kwargs)
        # '#ff7f0e' = orange
        # '#1f77b4' = blue
        # '#2ca02c' = green
        savename_ = None
        close_after_last = False
        alli = range(self.max_index)
        if kwargs.pop('onlyFinal', False):
            alli = [self.max_index]
        for i in alli:
            this_label = [None, None, None]
            if i == self.max_index - 1:
                close_after_last = close_after
                savename_ = savename
                this_label = label
            if i < self.max_index - 1:
                color = grays[i]
            else:
                color = '#000000'  # '#000000' = black
            ax = self.showModels(
                i + 1, ax=ax,
                label=this_label,
                savename=savename_,
                color=color,
                close_after=close_after_last,
                **kwargs)
        return ax

    def saveAll(self):
        plt.ioff()
        sub_dir = self.directory.split('/')[-1]
        self.showChi2(savename='{}/chi2_{}'
                      .format(self.directory, sub_dir),
                      close_after=True)
        # stats_scc.showStartMiddleEnd(save_directory='new_dir')

        self.showModels(index=0, syn=True,
                        savename='{}/smooth_result_{}'
                                 .format(self.directory, sub_dir),
                        close_after=True)
        self.showAllInOne(savename='{}/all_in_one_{}'
                                   .format(self.directory, sub_dir),
                          close_after=True)
        self.showAllInOne(savename='{}/smooth-final_{}.pdf'
                                   .format(self.directory, sub_dir),
                          onlyFinal=True, close_after=True)

        for i in [1, 4, -1]:
            if i == -1:
                i = self.max_index
            self.showModels(index=i, syn=True,
                            savename='{}/model_step_{}_{}'
                                     .format(self.directory, i, sub_dir),
                            close_after=True)

            self.showRoughness(index=i, savename='{}/roughness_step_{}_{}'
                               .format(self.directory,
                                       i,
                                       sub_dir),
                               close_after=True)
            self.showCWeights(index=i, savename='{}/c_weights_step_{}_{}'
                                                .format(self.directory,
                                                        i,
                                                        sub_dir),
                              close_after=True)
        plt.ion()


def niceLogTicks(ax, toplot):
    minlog = np.round(np.log10(np.min(toplot)) - 0.5)
    maxlog = np.round(np.log10(np.max(toplot)) + 0.5)
    tickrange = np.arange(minlog, maxlog, 1)
    ax.set_xticks(10**tickrange)
    ax.set_xticklabels(['${10^{%d}}$' % (exp) for exp in tickrange])
# The End
