"""
Part of comet/snmr/framework
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# import os
import numpy as np
import pygimli as pg

try:  # pg.__version__ < 1.1
    from pygimli.mplviewer import drawModel1D
except ImportError:  # pg.__version__ >= 1.1
    from pygimli.viewer.mpl import drawModel1D

import matplotlib.pyplot as plt

from comet.pyhed.IO import checkDirectory
from comet.pyhed.misc import Timer
from comet.snmr.misc import robustPDFSave, closeAxis
# =============================================================================
# Toolbox for handling the different managers classes as list_like object
# =============================================================================


class NamedList(list):
    """
    Basically a standard list. with a addional name for each item

    Additionally the setNames() method can be used to make the list callable.

    >>> mrs = managerinstance 1
    >>> ves = pg.physics.VESManager  # for example
    >>> Managers = ManagerList(mrs, ves)
    >>> Managers.setKeys('mrs', 'ves')
    >>> for m in Managers:
    >>>    print(m)
    mrs.__repr__
    ves.__repr__
    >>> Managers('ves')
    <class 'pygimli.physics.VESManager'>
    """
    def __init__(self, *args, **kwargs):
        for arg in args:
            if isinstance(arg, list):
                # case 1: arguments are already a list
                super(NamedList, self).__init__(*args)
            else:
                # case 2: arguments are given as tuple
                super(NamedList, self).__init__(args)
            break

        self.setNames(names=kwargs.pop('names', None))

    def setNames(self, names=None):  # sets specific names for managers
        self.dict = {}
        self.names = []
        for i in range(len(self)):
            if names is not None:
                self.names = names
                if self.names[i] not in self.dict:
                    self.dict[self.names[i]] = i
                else:
                    raise Exception('Abort. Please use unique names only.')
            else:
                self.names.append('item%d' % (i))
                self.dict[self.names[-1]] = i

    def __call__(self, cry):
        # make list callable by manager name or index
        if isinstance(cry, str):
            return self[self.dict[cry]]
        elif isinstance(cry, int):
            return self[cry]

#    def __getitem__(self, item):
#        # FIXME!: i create an infinity loop
#        print(type(item))
#        if isinstance(item, str):
#            return self[self.dict[item]] <- start infinity in this line
#        else:
#            return self[item] <- start infinity in this line
#        edit: only works if actual 'get' comes from another variable!
#        solution: give the list to another protected member like __list.
#        needs to override all

    def enumerateByName(self):
        # return [[name, self[self.dict[name]]] for name in self.names]
        return [[name, self[name]] for name in self.names]

    def assignByName(self, name, value):
        self[self.dict[name]] = value

    def append(self, item, name=None):
        self.insert(len(self), item)
        if name is None:
            name = 'item%d' % (len(self) - 1)
        self.names.append(name)
        self.dict[name] = len(self) - 1

    def extend(self, items, names=None):
        if names is not None:
            if len(names) == len(items):
                for i in range(len(items)):
                    self.append(items[i], names[i])
            else:
                raise Exception('len(names) != len(items), abort extend')
        else:
            for i in range(len(items)):
                self.append(items[i])

# =============================================================================
# SCC Framework starts here
# =============================================================================


class SCC_Framework(object):
    """ Structurally Coupled Cooperative inversion framework. """
    def __init__(self, *managers, **kwargs):
        # manager handling done by ManagerList class
        # contains all the fancy stuff
        self.managers = NamedList(*managers, names=kwargs.pop('names', None))
        self.nMan = len(self.managers)
        self.nPara = 0
        for m in self.managers:
            if hasattr(m, '_numPara'):
                self.nPara += m._numPara
            else:
                self.nPara += 1
                m._numPara = 1  # HACK
        self.startModels = []
        self.statistics = NamedList()
        for name in self.managers.names:
            self.statistics.append([], name)

        self.responses = [None] * len(self.managers)
        self.models = [None] * len(self.managers)
        self.lambdas = None

        self.verbose = kwargs.pop('verbose', False)
        self.debug = kwargs.pop('debug', False)
        self.maxCPUCount = kwargs.pop('maxCPUCount', 32)
        self.cWeight = None
        self.couplingPara = {}
        # kwargs.pop('couplingPara', [0.1, 0.1, 1.0])
        self.couplingPara.update({'a': kwargs.pop('a', 0.1)})
        self.couplingPara.update({'b': kwargs.pop('b', 0.1)})
        self.couplingPara.update({'c': kwargs.pop('c', 1.0)})
        self.couplingPara.update({'Min': kwargs.pop('Min', 0.05)})
        self.couplingPara.update({'Max': kwargs.pop('Max', 1.0)})
#        b=coupling_kwargs.pop('b', self.couplingPara[1]),
#        c=coupling_kwargs.pop('c', self.couplingPara[2]),
#        Min=coupling_kwargs.pop('Min', 0.2),
#        Max=coupling_kwargs.pop('Max', 1.0)

        self.couplingCase = 0
        self.singleCWeights = [None] * self.nPara
        self.roughnesses = [None] * self.nPara
        self.prec_maxIter = None
        self.prec_deltaPhi = None
        self.cWeightHistory = []
        self.newCWeight = None
        self.synthetics = NamedList([None] * self.nMan,
                                    names=kwargs.pop('names', None))

        self.timer = kwargs.pop('timer', Timer())
        self.timer.setVerbose(self.verbose)
        self.timer.tick('Initialized SCCI framework at time {1:2.2f}')

        self.smooth_inversion_ready = False
        self.setDiscretisation(kwargs.pop('discretisation', None))
        self.experimental_roughness = kwargs.pop('experimental_roughness',
                                                 False)

    def __str__(self):
        string = 'SCC_Framework: {} with {} managers'.format(
                self.__class__.__name__, self.nMan)
        for m in self.managers:
            string += '\n{}'.format(m)

        string += '\n####### SCC specifics #########'
        string += '\ncoupling Para: a, b, c: {}'.format(self.couplingPara)
        string += '\ncoupling Case: {}'.format(self.couplingCase)
        string += '\npre coupling max Iterations: {}'.format(self.prec_maxIter)
        string += '\npre coupling delta Phi: {}'.format(self.prec_deltaPhi)
        string += '\nlambdas: {}'.format(self.lambdas)
        return string

    def __repr__(self):
        return self.__str__()

    def setDiscretisation(self, discretisation):
        self.discretisation = discretisation

    def setNames(self, names):
        if len(names) != self.nMan:
            raise Exception('number of names are not the same as \
the number of managers')
        # set new names
        self.managers.setNames(names)
        self.statistics.setNames(names)
        self.synthetics.setNames(names)

    def setCouplingPara(self, **coupling_kwargs):
        self.couplingPara.update(**coupling_kwargs)

    def setSynthetics(self, *args):
        if isinstance(args, list):
            for i, syn in enumerate(args):
                self.synthetics[i] = syn
        else:
            for i, syn in enumerate(*args):
                self.synthetics[i] = syn

    def response(self, manager='all'):
        if manager == 'all':
            for i, m in enumerate(self.managers):
                self.responses[i] = m.response()
        else:
            self.responses[self.managers.dict[manager]] = \
                self.managers(manager).response()

    def createJacobian(self, manager='all'):
        if manager == 'all':
            for m in self.man:
                m.createJacobian()
        else:
            self.managers(manager).createJacobian()

    def getRoughness(self, manager):
        ''' Returns a list with roughnesses for each parameter '''
        if manager.INV is None:
            raise Exception('Manager has no inversion instance to calculate \
roughness from.')
        rough = manager.INV.roughness()  # C_0 * log(trans(m))
        returnValue = []
        i2 = int(len(rough)/manager._numPara)
        for i in range(manager._numPara):
            if self.experimental_roughness is True:
                thk_weights = (self.discretisation[1:] +
                               self.discretisation[:-1])/2.
                # handle infinite thickness of last layer
                thk_weights = np.append(thk_weights, self.discretisation[-1])
                print('Take real derivation instead of absolute parameter '
                      'difference.')
#                print(np.shape(rough[i*i2:i*i2 + i2]))
#                print(np.shape(thk_weights))
                returnValue.append(
                    np.array(rough[i*i2:i*i2 + i2]) / thk_weights)
            else:
                returnValue.append(np.array(rough[i*i2:i*i2 + i2]))
        return returnValue

    @staticmethod
    def _getCWeight(vec, a=0.1, b=0.1, c=1.0, Max=1.0, Min=0.2):
        """
        Structural constraint weight as function of roughness. See
        Günther & Rücker (2006, SAGEEP extended abstract) (case a=0) and
        Günther et al. (2010, SAGEEP extended abstract) (case a>0) for details.
        """
        avec = np.absolute(vec)
        if a == 0:  # old IRLS based style
            cfun = np.sum(avec**2) / np.sum(avec) / (avec + 1e-8*max(avec))
            if b < 1.0:  # use b instead of minimum (if >=1)
                b = 1.0
            cfun = np.minimum(cfun, b)
            if c > 0:
                cfun /= pg.mean(cfun)  # normalize to same sum
        else:
            cfun = (a / (avec + a) + b)**c
            cfun = np.minimum(cfun, Max)
            cfun = np.maximum(cfun, Min)
        # print(len(cfun), max(cfun), min(cfun))
        # print('NO ABSOLUTE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        return cfun  # *np.sign(vec)

    def _structuralCoupling(self, case=0, normalized=False, **coupling_kwargs):
        # step 1: get roughness from each manager
        # 1 per parameter
        self.roughnesses = []
        for i, m in enumerate(self.managers):
            # modelSize = int(m.FOP.mesh().cellCount/m._numPara)
            a = np.ones(m.FOP.mesh().cellCount())
            print(np.shape(a))
            # print(np.shape(np.ones(m.FOP.mesh().boundaryCount())))
            m.INV.setCWeight(a)
            self.roughnesses.extend(self.getRoughness(m))

            if normalized is True:
                print('WEIGHTED ROUGHNESS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                for r in self.roughnesses:
                    Max = max([-min(r), max(r)])
                    r /= Max

        # step 2: calculate c_weight factor for coupling
        common_c_weight = np.ones_like(self.roughnesses[0])
        self.singleCWeights = []
        for j, r in enumerate(self.roughnesses):
            if len(r) != len(common_c_weight):
                raise Exception('to do: len thing...')
            self.singleCWeights.append(
                self._getCWeight(r, **self.couplingPara))
            if self.debug:
                print('roughness {}'.format(j),
                      np.min(r),
                      np.max(r),
                      np.shape(r))

        if case == 0:  # all weights
            self.newCWeight = np.ones_like(self.singleCWeights[0])
            for weight in self.singleCWeights:
                self.newCWeight *= weight  # all weights
            self.newCWeight = np.minimum(self.newCWeight,
                                         self.couplingPara['Max'])
            self.newCWeight = np.maximum(self.newCWeight,
                                         self.couplingPara['Min'])
            if self.verbose:
                print('common_c_weight', np.min(self.newCWeight),
                      np.max(self.newCWeight), np.shape(self.newCWeight))

            # step 3.0: append c_weights to inversion instances
            for i, m in enumerate(self.managers):
                m.INV.setCWeight(np.tile(self.newCWeight, m._numPara))

        elif case == 1:  # all other weights
            self.newCWeight = np.ones_like(self.singleCWeights)
            for i in range(self.nPara):
                allOthers = [np.arange(self.nPara) != i]
                print(allOthers)
                self.newCWeight[allOthers] *= self.singleCWeights[i]
            self.newCWeight = np.minimum(self.newCWeight,
                                         self.couplingPara['Max'])
            self.newCWeight = np.maximum(self.newCWeight,
                                         self.couplingPara['Min'])

            # %%
            # produces overall smother models, nothing else
#            print('Lambda factor consideration')
#            print(np.shape(self.newCWeight))
#            print(np.sum(self.newCWeight, axis=1))
#            print(np.sum(self.newCWeight[0]),
#                  np.sum(self.newCWeight[1]),
#                  np.sum(self.newCWeight[2]))
#            nl = np.shape(self.newCWeight)[1]
#            lam_weights = nl / np.sum(self.newCWeight, axis=1)
#            print(lam_weights)
#            self.timer.silent('{}: lambda factors: {}'
#                              .format(self.counter, lam_weights))
#            # nico's lambda conservation law
#            self.timer.silent('corrected: {}'
#                              .format(np.sum(
#                                      self.newCWeight *
#                                      lam_weights[:, np.newaxis],
#                                      axis=1)))
#            self.newCWeight = self.newCWeight * lam_weights[:, np.newaxis]
            # %%

            # step 3.1: append c_weights to inversion instances
            n = 0
            for i, m in enumerate(self.managers):
                weight = []
                for j in range(m._numPara):
                    weight.extend(self.newCWeight[n])
                    n += 1
                print('manager {}, weights {}'.format(i, np.shape(weight)))
                m.INV.setCWeight(weight)

    def _getStatistics(self, manager=None):
        # seems ugly, but is convenient to use
        if manager is None:
            n = 0
            for i, m in enumerate(self.managers):
                Mrough = []
                Mcweight = []
                model = np.split(np.array(m.INV.model()), m._numPara)
                for j in range(m._numPara):
                    Mrough.append(self.roughnesses[n])
                    Mcweight.append(self.singleCWeights[n])
                    n += 1
                self.statistics[i].append([m.INV.getChi2(),
                                           m.INV.relrms(),
                                           m.INV.absrms(),
                                           m.INV.iter(),
                                           model,
                                           Mrough,
                                           Mcweight])
        else:
            Mrough = []
            Mcweight = []
            model = np.split(np.array(self.managers(manager).INV.model()),
                             self.managers(manager)._numPara)
            for j in range(self.managers(manager)._numPara):
                Mrough.append(self.roughnesses[j + manager])
                Mcweight.append(self.singleCWeights[j + manager])
            self.statistics(manager).append(
                    [self.managers(manager).INV.getChi2(),
                     self.managers(manager).INV.relrms(),
                     self.managers(manager).INV.absrms(),
                     self.managers(manager).INV.iter(),
                     model,
                     Mrough,
                     Mcweight])  # TODO: dictionary

    def custom_PreStep(self):
        """ User defined calculations before each coupled inversion step. """
        pass

    def custom_InterStep(self, after):
        """
        User defined calculations between the different inversions in
        each coupled inversion step.

        automatic argument
        ------------------

        after: str
            Name of the last manager that just finished it#s current inversion
            step. The custom_InterStep is called after each inversion step of
            each manager. To apply some code only after one specific time use
            the "if after == manager_name:" phrase.

        """
        pass

    def custom_PostStep(self):
        """ User defined calculations after each coupled inversion step. """
        pass

    def custom_AbortCriteria(self, manager_name):
        """
        User defined abort criteria. Returns True if an inversion has reached
        it's abort criteria and False if the inversion is not finished yet.

        example:
        --------

        >>> if manager_name == 'VES':
        >>>     check special VES abort criteria
        >>>
        >>> if manager_name == 'MRS':
        >>>     same same  MRS
        >>> ...

        """
        return False

    def _checkAbortCriteria(self, maxIter=10, chi2=0.9, custom=None,
                            force=False):
        """
        Standard abort criteria check.
        Returns True if an inversion has reached its abort criteria and False
        if the inversion is not finished yet.
        """
        if force is True:
            print('Ignoring abort criterias due to "force" keyword.')
            self.abort = [self.counter >= maxIter] * self.nMan
            return

        for i, m in enumerate(self.managers):
            if maxIter is not None:
                if self.counter >= maxIter:
                    print('The {} manager has reached the maximum iteration \
count of {} iterations'.format(self.managers.names[i], maxIter))
                    self.abort[i] = True

            if chi2 is not None:
                if self.statistics[i][-1][0] <= chi2:
                    print('The {} manager has reached chi2 criteria of chi2 <= \
{}'.format(self.managers.names[i], chi2))
                    self.abort[i] = True

            if custom is not None:
                if self.custom_AbortCriteria(self.managers.names[i]) is True:
                    print('The {} manager has reached the customized abort \
criteria'.format(self.managers.names[i]))
                    self.abort[i] = True

            if maxIter is None and chi2 is None and custom is None:
                print('Abort inversion, no abort criteria has been set.')
                self.abort = [True] * self.nMan

    def _oneStep(self, manager='all', force=False, coupled=False):
        if manager == 'all':
            for i, m in enumerate(self.managers):
                print('SCC: checking abort criteria of manager {}: '
                      .format(self.managers.names[i]))
                if self.abort[i] is False or force is True:
                    self.timer.tick(
                        'SCC: Manager {}, inversion step {}'
                        .format(self.managers.names[i], self.counter))
                    m.INV.oneStep()
                    if coupled is True:
                        self.custom_InterStep(self.managers.names[i])
                    self.timer.tick('{:2.2f} seconds')
        else:
            if self.abort[i] is False:
                self.timer.tick(
                    'SCC: Manager {}, inversion step {}'
                    .format(manager, self.counter))
                self.managers(manager).INV.oneStep()
                self.timer.tick('{:2.2f} seconds')

    def _runCoupled(self, force=False):
        self.custom_PreStep()
        self._structuralCoupling(self.couplingCase)
        self._oneStep(force=force, coupled=True)
        self._getStatistics()
        self.cWeightHistory.append(self.newCWeight)
        self.custom_PostStep()

    def _runSeperate(self, datas, lambdas, coupling_maxIter,
                     coupling_deltaPhi):
        for i, m in enumerate(self.managers):
            self.timer.tick('seperate inversion {}'.format(m))
            m.invert(datas[i],
                     lam=lambdas[i],
                     deltaPhiAbortPercent=coupling_deltaPhi,
                     maxIter=coupling_maxIter,
                     verbose=True)
            self.custom_InterStep(self.managers.names[i],
                                  seperate=True)
            self.timer.silent('finished in {:2.2f} seconds')
        self.stopSmoothInversion()

    def checkDatas(self, datas):
        if datas is not None:
            self.datas = datas
        for data in self.datas:
            if data is None:
                raise Exception('No data, no inversion!')

    def setLambdas(self, lambdas):
        for i, m in enumerate(self.managers):
            if m.INV is None:
                raise Exception('Manager "{}" has no inversion instance.'
                                .format(m))
            m.INV.setLambda(lambdas[i])
        self.lambdas = lambdas

    def setManager(self, name, manager):
        if name in self.managers.dict:
            self.managers.dict.update(name, manager)
        else:
            print('Did not found "{}" in original manger names. Abort.')
            return

    def stopSmoothInversion(self):
        for i, m in enumerate(self.managers):
            self._getStatistics(i)
        self.smooth_inversion_ready = True

    def invert(self, datas=None,
               prec_maxIter=10,
               postc_maxIter=10,
               prec_deltaPhi=20.0,
               couplingCase=0,
               force=False,
               save_directory='.',
               lambda_factor=1.0,
               start_from_smooth=False,
               synthetics=None,
               **kwargs):
        """
        run():
        initialize stuff
        repeat until abort criteria reached:
               oneStep()
               do some post process and store results
               recalc abort criteria

        parameters
        ----------

        datas: array_like
            List with data vectors to be inverted.

        prec_lambdas: array_like
            List with lambdas for the decoupled part of the inversion.
            It's suggested to give a bit higher lambdas for the pre coupling
            part, than for the coupled part of the inversion.

        postc_lambdas: array_like
            List with lambdas for the coupled part of the  for the inversion.

        prec_maxIter: int [3]
            Maximum Number of iteration, each inversion runs, before the first
            coupling will be applied. Gives the inversion time to find the
            right minimum before intervening.

        prec_deltaPhi: int [3]
            Special abort criteria for decoupled inversions. Usually set to a
            high value of about 20%. Gives the inversions the possiblility to
            find a certain minimum without optimizing the results to far
            (will be altered due to the coupling anyway). No further effect
            on the coupled part of the joint inversion.

        synthetics: list
            Optional. List will be used to call the setSynthetics() method.
        """
        self.counter = 0  # counter for coupled! inversion steps
        self.maxCounter = postc_maxIter
        self.abort = [False] * self.nMan

        self.prec_lambdas = kwargs.pop('prec_lambdas', [10000] * self.nMan)
        self.post_lambdas = kwargs.pop('post_lambdas', self.prec_lambdas)

        if start_from_smooth is True:
            if not self.smooth_inversion_ready:
                self.stopSmoothInversion()
            self.counter = 1
            self.setLambdas(self.post_lambdas)

        if synthetics is not None:
            self.setSynthetics(*synthetics)

        else:
            if datas is None:
                if self.synthetics[0] is not None:
                    datas = self.synthetics
                    print('SCC: Found synthetic data for inversion')
                    self.checkDatas(datas)
                elif start_from_smooth is True:
                    pass
#                    print('Get data from smooth inversion results.')
#                    for m in self.managers:
#                        datas.append(m.INV.model().array())
                else:
                    print('No data given.')
                    print('Did not found synthetic data.')
                    raise Exception('No data, no inversion!')

        self.couplingCase = couplingCase
        self.setCouplingPara(**kwargs)
        # self.couplingPara = kwargs.pop('couplingPara')

        print('SCC: start inversion')
        while not np.all(self.abort):

            if not self.smooth_inversion_ready:
                # uncoupled part
                self.prec_maxIter = prec_maxIter
                self.prec_deltaPhi = prec_deltaPhi
                self._runSeperate(datas,
                                  self.prec_lambdas,
                                  self.prec_maxIter,
                                  self.prec_deltaPhi)
                self.setLambdas(self.post_lambdas)
                self.counter += 1
            else:
                # coupled part
                print('SCC: run {} (coupled)'.format(self.counter))
                if not np.isclose(lambda_factor, 1.0):
                    new_lam = np.asanyarray(self.post_lambdas) *\
                              (lambda_factor**self.counter)
                    print('SCC: new lambdas {}'.format(new_lam))
                    self.setLambdas(new_lam)
                self._runCoupled(force=force)
                self.counter += 1
                self._checkAbortCriteria(maxIter=self.maxCounter,
                                         force=force)

        print('Abort criteria(s) has been reached. Inversion finished.')
        for i, m in enumerate(self.managers):
            self.models[i] = m.INV.model()

        self.saveStatistics(save_directory)
        plt.ioff()
        for j, m in enumerate(self.managers):
            try:
                ax = m.showResultAndFit()
            except AttributeError:
                ax = m.showResultsAndFit()
            robustPDFSave(ax, '{}/{}_ResultAndFit.pdf'
                          .format(save_directory, self.managers.names[j]))
            closeAxis(ax)
        ax = self.managers('mrs').showDataAndFit(
                savematrices=True,
                savename='{}/mrs_DataAndFit'.format(save_directory))
        np.save(save_directory + '/ves_dataToFit.npy',
                self.managers[0].dataToFit)
        np.save(save_directory + '/ves_INVresponse.npy',
                self.managers[0].INV.response())
#        np.save(save_directory + '/ves_ab2.npy',
#                self.managers[0].ab2)
        closeAxis(ax)
        plt.ion()
        return self.models

    def showCWeight(self, ax=None, thk=None, label='cWeight',
                    iteration=None):
        if ax is None:
            if self.couplingCase == 0:
                fig, ax = plt.subplots(1, 1, figsize=(8, 6))
            else:
                fig, ax = plt.subplots(1, self.nPara, figsize=(8, 6))

        if self.newCWeight is None and self.cWeightHistory is None:
            print('No cWeight calculated yet.')
            return

        if thk is None:
            thk = np.ones(int(self.managers[0].FOP.mesh.cellCount() /
                              self.managers[0]._numPara))
        if self.couplingCase == 0:
            if iteration is None:
                drawModel1D(ax, thickness=thk,
                            values=self.newCWeight, label=label)
            else:
                drawModel1D(ax, thickness=thk,
                            values=self.statistics[0][iteration][6][0],
                            label=label)
            ax.set_xlabel('weighting factor [1]')
            ax.grid(True, which='both')
            ax.legend(loc='best')
        else:
            if iteration is not None:
                weights = []
                for i in range(self.nMan):
                    for weight in self.statistics[i][iteration][6]:
                        weights.append(weight)
            for i in range(self.nPara):
                if iteration is None:
                    drawModel1D(ax[i], thickness=thk,
                                values=self.newCWeight[i],
                                label=label)
                else:
                    drawModel1D(ax[i], thickness=thk,
                                values=weights[i],
                                label=label)
                ax[i].set_xlabel('weighting factor [1]')
                ax[i].grid(True, which='both')
                ax[i].legend(loc='best')
        return ax

    def showRoughness(self, ax=None, thk=None, label='roughness',
                      iteration=None):
        if ax is None:
            fig, ax = plt.subplots(1, self.nPara, figsize=(8, 6))

        if self.roughnesses is None and self.r:
            print('No roughnesses calculated yet.')
            return

        if thk is None:
            thk = np.ones(int(self.managers[0].FOP.mesh.cellCount() /
                              self.managers[0]._numPara))
        n = 0
        for j, m in enumerate(self.managers):
            for i in range(m._numPara):
                if iteration is None:
                    drawModel1D(ax[n], thickness=thk,
                                values=self.roughnesses[n], label=label)
                else:
                    roughs = []
                    for rough in self.statistics[n][iteration][5]:
                        roughs.extend(rough)
                    drawModel1D(ax[n], thickness=thk,
                                values=roughs,
                                label=label)
                ax[n].set_xlabel('roughness ({} {})'
                                 .format(self.managers.names[j], i + 1))
                ax[n].grid(True, which='both')
                ax[n].legend(loc='best')
                n += 1
        return ax
    def saveStatistics_old(self, directory):
        checkDirectory(directory)
        np.save(directory + '/c_weight_history.npy', self.cWeightHistory)
        paras = [[self.couplingPara['a'],
                  self.couplingPara['b'],
                  self.couplingPara['c'],
                  self.couplingPara['Min'],
                  self.couplingPara['Max']],
                 self.couplingCase,
                 self.prec_maxIter,
                 self.prec_deltaPhi,
                 self.lambdas]
        np.save(directory + '/scc_parameters.npy', paras)
        for i, m in enumerate(self.managers):
            np.save(directory + '/statistics_{}.npy'
                    .format(self.managers.names[i]),
                    self.statistics[i])
            np.save(directory + '/synthetics_{}.npy'
                    .format(self.managers.names[i]),
                    self.synthetics[i])
        # TODO: HACK, hard coded 'ves' to be removed
        z_thk = self.managers('ves').Z
        np.save(directory + '/z_thk.npy', z_thk)

    def saveStatistics(self, directory):
        checkDirectory(directory)
        np.save(directory + '/c_weight_history.npy', self.cWeightHistory)
        paras = [[self.couplingPara['a'],
                  self.couplingPara['b'],
                  self.couplingPara['c'],
                  self.couplingPara['Min'],
                  self.couplingPara['Max']],
                 self.couplingCase,
                 self.prec_maxIter,
                 self.prec_deltaPhi,
                 self.lambdas]
        np.save(directory + '/scc_parameters.npy', paras)
        for i, m in enumerate(self.managers):
            basic = []
            results = []
            for j in range(len(self.statistics[i])):
                # loop over iterations

                # chi2, absrms, relrms, iter
                basic.append(self.statistics[i][j][:4])
                # model, roughness, cweight
                results.append(np.squeeze(np.array(self.statistics[i][j][4:])))

            np.savez(directory + '/statistics_{}.npz'
                     .format(self.managers.names[i]),
                     basic=basic, results=results)
            np.save(directory + '/synthetics_{}.npy'
                    .format(self.managers.names[i]), self.synthetics[i])

        z_thk = self.managers[0].Z
        np.save(directory + '/z_thk.npy', z_thk)

# =============================================================================
# Specialised SCC Manager start here
# =============================================================================


class SCC_MRS_VES(SCC_Framework):
    """ Approach for structurally coupled joint inversion of MRS and VES. """
    def __init__(self, VES_Manager, MRS_Manager, verbose=False, debug=False,
                 updateKernel=False, **kwargs):
        super(SCC_MRS_VES, self).__init__(VES_Manager, MRS_Manager,
                                          verbose=verbose, debug=debug,
                                          **kwargs)
        self.setNames(('ves', 'mrs'))
        self.updateKernel = updateKernel
        self.managers('mrs').FOP.updateKernel = self.updateKernel
        if self.managers('mrs').FOP is not None:
            self.managers('mrs').FOP.update_kernel = self.updateKernel

    def custom_InterStep(self, after, seperate=False):

        if seperate:
            if after == 'ves':  # after initial ves smooth inversion
                print('SEPARATED: resistivity update')
                self.managers('mrs').Kernel.tx.setModel(
                    self.managers('ves').INV.model().array(),
                    d=self.managers('ves').Z)

        else:
            if self.updateKernel and after == 'ves':
                print('CUSTOM: resistivity update')
                new_res = self.managers('ves').INV.model().array()
                new_thk = self.managers('ves').Z
                print(np.min(new_res), np.max(new_res), np.shape(new_res))
                self.managers('mrs').Kernel.tx.config.rho = new_res
                self.managers('mrs').Kernel.tx.config.d = new_thk

    def custom_AbortCriteria(self):
        return False

# The End
