"""
overall COMET init file, if you want to import comet as one module.
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from . import pyhed
from . import snmr
import logging

from pathlib import Path
basepath = Path(__file__).parent
# conda test try except to prevent NotADirectory Error
try:
    with open(basepath.joinpath('VERSION.rst').as_posix(), 'r') as versionrst:
        __version__ = versionrst.read()

except (NotADirectoryError, FileNotFoundError):
    __version__ = None

log = logging.getLogger('comet')

logging.addLevelName(16, 'PROGRESS')
logging.addLevelName(13, 'TRACE')

# log.setLevel(logging.WARNING)
# ....      > 50, silent
# CRITICAL  = 50, errors that may lead to data corruption and suchlike
# ERROR     = 40, things that go boom
# WARNING   = 30, things that may go boom later
# INFO      = 20, information of general interest
# PROGRESS  = 16, what's happening (broadly)
# TRACE     = 13, what's happening (in detail)
# DBG       = 10, sundry
# NOT SET   =  0, also sundry

# The End
