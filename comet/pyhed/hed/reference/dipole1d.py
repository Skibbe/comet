"""
Part of comet/pyhed/hed/reference

Read Dipole 1d result file and saves numpy arrays as well as vtk's.
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import pygimli as pg
import pyhed as ph


def getIntFromLine(line):
    """ Gets integer from line or convert float, else returns None.

    Internally used.
    """
    line = line.rstrip().lstrip()

    try:
        if '.' in line:
            integer = int(float(line))

        else:
            integer = int(line)

        return integer

    except ValueError:
        integer = None

    return integer


def convertCSEM(csem_file, mesh_name, dipole_lengths=1., out_name=None,
                out_vtk=True):
    """ Load csem file and saves numpy arrays as well as vtk.

    Parameters
    ----------
        csem_file: string
            Input filename for result csem file.

        mesh: string or pg.Mesh
            Mesh instnace or filename for positions (The positions in the .csem
            file are rounded to 1 digit only per default).

        dipole_lengths: float or np.ndarray
            Legth of the dipole if not given to Dipole1D during calculation
            for scaling of the fields. Expect one value per transmitter.

        out_name: string [ None ]
            Optional chance to alter output name. Default name will be
            generated using the *csem_file* string.

        out_vtk: boolean [ True ]
            Saving of a vtk file is optional.

    Returns
    -------
        (e_field, b_field)

        e_field: np.ndarray
            Electric field of shape (3, n) corresponding to the coordinates
            found in the mesh.

        b_field: np.ndarray
            Magnetic field of shape (3, n) corresponding to the coordinates
            found in the mesh.

    """
    if out_name is None:
        out_name = csem_file.rstrip('.csem')

    dipole_lengths = np.atleast_1d(dipole_lengths)

    with open(csem_file, 'r') as result:
        skip = 2
        # skip version ... and dipole lengths
        # the code is based on and tested for Dipole1D_1.1 results

        lp = ph.loop.buildDummy()  # for savekeeping
        lp.setLoopMesh(mesh_name)

        e_field = np.zeros((3, lp.loopmesh.nodeCount()), dtype=np.complex)
        b_field = np.zeros((3, lp.loopmesh.nodeCount()), dtype=np.complex)

        end_header = None
        tx_count = None

        for i, line in enumerate(result.readlines()):
            if skip > 0:
                skip -= 1
                continue

            integer = getIntFromLine(line)

            if integer is not None:
                skip = integer

                if tx_count is None and skip > 0:
                    tx_count = skip
                    if tx_count != len(dipole_lengths):
                        raise Exception('found {} transmitter but got {} '
                                        'dipole lengths'
                                        .format(tx_count,
                                                len(dipole_lengths)))
                continue

            if end_header is None:
                end_header = i
            break

    # now we now the number of transmitter and receiver as well as
    # the line number where the field values begin.

    print('load floats')
    all_floats = np.atleast_2d(np.genfromtxt(csem_file,
                                             skip_header=end_header))
    print('{.shape}'.format(all_floats))
    all_floats[np.where(np.isnan(all_floats))] = 1e-99
    # test tx*n % tx == 0
    if len(all_floats) % tx_count != 0:
        raise Exception('Shape of input {} cannot be divided by tx count {}.'
                        .format(all_floats.shape, tx_count))
    # test tx(file)*n // n == tx(expected)
    if len(all_floats) // tx_count != lp.loopmesh.nodeCount():
        raise Exception('Number of field values ({}) doesn\'t match the '
                        'node count of export mesh ({}).'
                        .format(len(all_floats) // tx_count,
                                lp.loopmesh.nodeCount()))

    # real + imag = complex (n*tx, 12) -> (n*tx, 6)
    print('compute complex')
    all_complex = all_floats[:, ::2] + 1j * all_floats[:, 1::2]
    print('found NaN', np.where(np.isnan(all_complex))[0].shape)
    print('{.shape}'.format(all_complex))

    # split the single tx fields (n*tx, 6) -> tx * (n, 6)
    print('split dipoles')
    dipole_fields = np.array_split(all_complex, tx_count, axis=0)

    # this only works after passing the two checks!
    # tx * (n, 6) -> (tx, n, 6)
    dipole_fields = np.array(dipole_fields)
    print('array dipoles: {.shape}'.format(dipole_fields))

    # weighting with individual dipole lengths
    print('weighting')
    ds = np.atleast_1d(dipole_lengths)
    print(ds[:, np.newaxis, np.newaxis])
    dipole_fields *= ds[:, np.newaxis, np.newaxis]
    print('{.shape}'.format(dipole_fields))

    # sum total fields
    # (tx, n, 6) -> (n, 6)
    print('sum it up')
    fields = np.sum(dipole_fields, axis=0)
    print('found NaN', np.where(np.isnan(fields))[0].shape)
    print('{.shape}'.format(fields))

    # split e and b field and swap axis (n, 6) -> (3, n), (3, n)
    e_field = fields[:, :3].T
    b_field = fields[:, 3:].T  # * 4e-7 * np.pi  # convert H to B
    print('split E: {.shape}, B: {.shape}'.format(e_field, b_field))

    e_field[1] *= -1  # right handed coord system with z negative downwards
    e_field[0] *= -1  # right handed coord system ...
    lp.field = e_field
    print('{.shape}'.format(e_field))
    np.save('E_{}.npy'.format(out_name), e_field)
    lp.exportVTK('E_{}.vtk'.format(out_name))

    b_field[1] *= -1  # right handed coord system with z negative downwards
    b_field[0] *= -1  # right handed coord system ...
    lp.field = b_field
    print('{.shape}'.format(b_field))
    np.save('H_{}.npy'.format(out_name), b_field)
    lp.exportVTK('H_{}.vtk'.format(out_name))
    return e_field, b_field


def makeRunFile(header, mesh, out_name='RUNFILE', verbose=True):
    """ Creates runfile for Dipole1D out of a sample header and a mesh file.

    Returns basically the sample header, but changes the coordinates of the
    receivers for the mesh coordinates.

    Parameters
    ----------
        header: string
            Filename of sample header. Read only access.

        mesh: string or pg.Mesh
            Filename of the mesh containing the coordinates or mesh instance.

        out: string [ 'RUNFILE' ]
            Filename of the Dipole1D run file.

        verbose: boolean [ True ]
            Turn verbose confirmation on. Note all *load* and *save*
            routines inside pyhed are verbose per default to give feedback
            if interacting with files.
    """
    with open(header, 'r') as base:
        if verbose:
            print('read master header: {}'.format(header))

        with open(out_name, 'w') as final:
            if isinstance(mesh, str):
                if verbose:
                    print('load mesh: {}'.format(mesh))

                m = pg.load(mesh)
                arr = m.positions().array()

            elif isinstance(mesh, np.ndarray):
                arr = mesh

            else:
                raise Exception('mesh not valid')

            # copy header until receiver port
            for line in base.readlines():
                if line.startswith('# RECEIVERS'):
                    break
                else:
                    final.write(line)

            # write receiver part
            final.write('# RECEIVERS:      {}\n'.format(len(arr)))
            for pos in arr:
                final.write('{:f} {:f} {:f}\n'
                            .format(pos[0], pos[1], -pos[2]))
    if verbose:
        print('prepared run file: {}'.format(out_name))


def makeRunFileFromLoop(loop, filename='RUNFILE',
                        result_filename='result_dipole1d.csem',
                        z_0_value=-0.1, use_spline=False, air_res=1e12,
                        verbose=True):
    """ Creates runfile for Dipole1D out of a loop class.

    Returns a suitable runfile for the usage of Dipole1D with respect to
    the loopmesh reciever coords, transmitter positions and defined
    1d layered earth model as well as other configurations defined in the
    loop class.

    Parameters
    ----------
        loop: ph.loop.Loop or string
            Filename or instance of loop.

        filename: string [ 'RUNFILE' ]
            Filename of the Dipole1D run file.

        result_filename: string [ 'result_dipole1d.csem' ]
            Filename written in the runfile. This filename defines the output
            name of the Dipole1D run.

        z_0_value: float [ -0.1 ]
            In contrast to pyhed, the source has to be placed a few cm into the
            ground. All transmitter dipoles at z==0 will be placed at
            *z_0_value* instead to ensure compatible results. Note the sign of
            *z_0_value*.

        use_spline: boolean [ False ]
            Enabling the usage of spline interpolation in the Dipole1D call.

        air_res: float [ 1e12 ]
            Airspace resistivity in Ohm*m.

        verbose: boolean [ True ]
            Turn verbose confirmation on. Note all *load* and *save*
            routines inside pyhed are verbose per default to give feedback
            if interacting with files.
    """
    if isinstance(loop, str):
        loop = ph.loop.loadLoop(loop, verbose=verbose)

    if loop.loopmesh is None:
        loop.createLoopMesh(verbose=verbose)

    rxs = loop.loopmesh.positions().array()
    rxs[:, 2] *= -1

    txs = np.copy(loop.pos)  # get ownership of positions
    z_0 = np.isclose(txs[:, 2], 0)
    txs[z_0, 2] = z_0_value
    txs[:, 2] *= -1

    with open(filename, 'w') as final:
        # Version:          DIPOLE1D_1.0
        # hard coded
        final.write('Version:          DIPOLE1D_1.0\n')

        # Output Filename:  kerry_key_ref.csem
        final.write('Output Filename: {}\n'.format(result_filename))

        # CompDerivatives:  yes/no
        # hack 1: hardcoded comp_derivatives is set to no
        # as the import of the resulting .csem file would fail for now.
        comp_derivatives = False
        final.write('CompDerivatives: {}\n'
                    .format('yes' if comp_derivatives else 'no'))

        # UseSpline1D:      yes/no
        final.write('UseSpline1D: {}\n'
                    .format('yes' if use_spline else 'no'))

        # # TRANSMITTERS: int
        final.write('# TRANSMITTERS: {}\n'.format(len(txs)))
        final.write(' X Y Z ROTATION DIP\n')
        # hack1: no dip for now
        for tx_i, tx in enumerate(txs):
            final.write('{:.8f} {:.8f} {:.8f} {:.8f} {}\n'
                        .format(*tx, np.rad2deg(loop.phi[tx_i]), 0))

        # # FREQUENCIES: int
        # hack2: only one freqency at once for now, this is only a simplified
        # wrapper
        final.write('# FREQUENCIES: 1\n')
        final.write('  {:.4f}\n'.format(loop.config.f))

        # # LAYERS: int
        # len(rho) + 1 (airspace)
        final.write('# LAYERS: {}\n'.format(len(loop.config.rho) + 1))
        # int int
        final.write('-100000 {:f}\n'.format(air_res))
        lay_top = np.append(0, np.cumsum(loop.config.d))
        for rho_i, rho in enumerate(loop.config.rho):
            final.write('{:f} {:f}\n'.format(lay_top[rho_i], rho))

        # # TRANSMITTERS: int
        final.write('# RECEIVERS: {}\n'.format(len(rxs)))
        for rx in rxs:
            final.write('{:f} {:f} {:f}\n'.format(*rx))

    if verbose:
        print('prepared run file: {}'.format(filename))


if __name__ == '__main__':
    makeRunFile('RUNFILE_base', 'loopmesh.bms')
    convertCSEM('kerry_key_ref.csem', 'loopmesh.bms')

# The End
