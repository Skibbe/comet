"""
module comet/pyhed/hed

Init file for HED calculation routines inside pyhed.
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# main routines
from . hed_bib import (hankelfc, downward, btp, downout, hfield_3D_hed_te,
                       hfield_3D_hed_tm, efield_3D_hed_te,
                       calcField, makeField)

# parallelization
from . hed_para import (SummationWorker, InterpolationWorker,
                        multiInterpolation)

# under construction
from . libHED import HED, World1D

# The End
