"""
Module comet/pyhed/IO

Init file for IO subpackage of pyhed.
Mainly used to load and save a bunch of stuff or handling some checks.
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from . vtk import add_vector_to_vtk, savefieldvtk
from . saveload import (ArgsError, cutExtension, TetgenNotFoundError,
                        searchforTetgen, getItem, checkDirectory, checkForFile,
                        delLastLine, addVolumeConstraintToPoly,
                        createCustEMDirectories)

# THE END
