"""
Part of comet/pyhed/plot
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from comet.pyhed.hed import hankelfc
import matplotlib.pyplot as plt
import numpy as np


def plotHankel(order):
    h = hankelfc(order)
    q = 0.1 * np.log(10)
    u = np.exp(-((h[2] - h[1] + np.arange(0, h[1], 1, dtype=np.int) + 1) - 1) *
               q)
    label = ['sin', 'cos', 'J0', 'J1']
    plt.semilogx(u, h[0][::-1], '.--', label=label[order - 1])
    plt.legend()


def plotKey(order):
    # kong_61_2007, kong_241_2007
    # key_101_2009 key_201_2009 key_401_2009
    # key_81_CosSin_2009 key_241_CosSin_2009 key_601_CosSin_2009
    from empymod.filters import key_401_2009 as kk_filter

    h = kk_filter()
    u = h.base
    label = ['kk J0', 'kk J1']
    if order == 1:
        to_plot = h.j0
    else:
        to_plot = h.j1
    plt.semilogx(u, to_plot, '.--', label=label[order - 1])
    plt.legend()


if __name__ == '__main__':
#    fig, ax = plt.subplots(2, 1)
    # plotHankel(1)
    # plotHankel(2)

    plotHankel(3)
    plotKey(1)
    plt.ylim(-.5, .5)

    plt.figure()
    plotHankel(4)
    plotKey(2)
    plt.ylim(-.5, .5)

# The End
