# -*- coding: utf-8 -*-
"""
Part of comet/pyhed/misc
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import time
import datetime
import numpy as np


class Timer():
    def __init__(self, verbose=True, timestamps=True, timefactor=1.0):
        self._start_time = time.time()
        self.actual_time = self._start_time
        self.history = []
        self.timestamps = []

        self.setTimestamps(timestamps)
        self.setVerbose(verbose)
        self.setTimeFactor(timefactor)

    def update(self):
        self.actual_time = time.time()

    @property
    def time_last(self):
        ret = (time.time() - self.actual_time) * self.time_factor
        return ret

    @property
    def time_total(self):
        ret = (time.time() - self._start_time) * self.time_factor
        return ret

    def getMessage(self, msg, ts=None):
        NOW = datetime.datetime.utcnow().strftime(self.strftime)
        if ts is None:
            ts = self.flag_ts

        if ts:
            actual_msg = '[{}] '.format(NOW)
        else:
            actual_msg = ''

        if isinstance(msg, str):
            actual_msg += '[{}]'.format(
                msg.format(self.time_last, self.time_total))
        else:  # e.g. a class object with __str__
            actual_msg += '[{}]'.format(msg)
        return actual_msg, NOW

    def noHist(self, msg, update=True, ts=None):
        msg, _ = self.getMessage(msg, ts=ts)
        print(msg)
        if update:
            self.update()

    def silent(self, msg, update=True, ts=None):
        actual_msg, now = self.getMessage(msg, ts=ts)
        self.history.append(actual_msg)
        self.timestamps.append(now)
        if update:
            self.update()

    def tick(self, msg, update=True, ts=None, **printkwargs):
        self.silent(msg, update=update, ts=ts)
        if self.verbose:
            print(self.history[-1], **printkwargs)

    def printHistory(self):
        for msg in self.history:
            print(msg)

    def setVerbose(self, verbose):
        self.verbose = verbose

    def setTimeFactor(self, factor):
        self.time_factor = factor

    def setTimestamps(self, bool_timestamps, strftime="%Y-%m-%d %H:%M:%S"):
        self.strftime = strftime
        self.flag_ts = bool_timestamps

    def exportLog(self, savename):
        np.save(savename, np.array(self.history))

    def importLog(self, savename):
        a = np.load(savename)
        self.history = (a.tolist())


class NoneTimer():
    def __init__(self, verbose=True):
        """
        Timer if no Timer is wanted.
        (The methods are used in the code anyway and need a valid target.)
        """
        pass

    def update(self):
        pass

    def getMessage(self, msg):
        pass

    def noHist(self, msg):
        pass

    def silent(self, msg):
        pass

    def tick(self, msg, **kwargs):
        pass

    def printHistory(self):
        print('No timer initialized.')

    def setVerbose(self, verbose):
        pass

    def setTimeFactor(self, factor):
        pass

    def exportLog(self, savename):
        pass

    def importLog(self, savename):
        pass


if __name__ == '__main__':
    tick = Timer(timestamps=True)
    tick.tick('Hallo')
# The End
