"""
Part of comet/pyhed/misc
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from . vec import getRSparseValues
from . timer import Timer


def exportSparseMatrixAsNumpyArchive(name, *sparseMats):
    tt = Timer()
    if name[-4:] != '.npz':
        name += '.npz'

    mats = []
    n_rows = []
    n_cols = []
    n_mats = 0
    for mat in sparseMats:
        n_mats += 1
        mats.append(np.column_stack(getRSparseValues(mat, getInCRS=False)))
        n_rows.append(mat.nRows())
        n_cols.append(mat.nCols())

    np.savez_compressed(name,
                        *mats,
                        n_rows=n_rows,
                        n_cols=n_cols,
                        n_mats=n_mats)
    tt.tick('total time: {1:2.2f} sec')


def loadSparseMatrixFromNumpyArchive(name, csr=True, verbose=True):
    import pygimli as pg
    if verbose:
        tt = Timer()
    if name[-4:] != '.npz':
        name += '.npz'

    npz = np.load(name)

    for i in range(npz['n_mats']):
        name = 'arr_{}'.format(i)
        sparseMatrix = pg.core.SparseMapMatrix(
            pg.core.IndexArray(npz[name][:, 0]),
                               pg.core.IndexArray(npz[name][:, 1]),
                               npz[name][:, 2])
        if verbose:
            tt.tick('build sparse matrix: {:2.2f} sec')
        sparseMatrix.setCols(int(npz['n_cols'][i]))
        sparseMatrix.setRows(int(npz['n_rows'][i]))

        if csr:
            sparseMatrix = pg.core.SparseMatrix(sparseMatrix)
            if verbose:
                tt.tick('transform to CRS: {:2.2f} sec')

        if i == 0:
            if npz['n_mats'] == 1:
                matrix = sparseMatrix
            else:
                matrix = [sparseMatrix]
        else:
            matrix.append(sparseMatrix)
    if verbose:
        tt.tick('total time: {1:2.2f} sec')
    return matrix


def dump2Json(json_name=None, **kwargs):
    """ Dumps all keyword-value combinations given in kwargs into a json
    file.
    Supported variable types can by found here:

    https://docs.python.org/3/library/json.html
    """
    import json
    from comet.pyhed import log
    from pathlib import Path

    if json_name is None:
        import tempfile as temp
        json_file = temp.NamedTemporaryFile(suffix='.json', delete=False)
        json_name = json_file.name
        json_file.close()

    path = Path(json_name)
    path.parent.mkdir(exist_ok=True, parents=True)

    with open(path.as_posix(), 'w') as out:
        towrite = json.dumps(kwargs, indent=4)
        log.info('Write json file: "{}"'
                 .format(path.absolute().as_posix()))
        out.write(towrite)

    return json_name


def json2Dict(name):
    """ Reads a json file from disk and converts it to python dictionary.
    """
    import json

    with open(name, 'r') as cfg:
        rawjson = cfg.read()
        outdict = json.loads(rawjson)
    return outdict

# The End
