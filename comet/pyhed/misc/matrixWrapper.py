"""
Part of comet/pyhed/misc
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pygimli as pg
import numpy as np


class RealNumpyMatrix(pg.core.MatrixBase):
    """
    Matrix Wrapper for for ndarrays, providing syntax for pygimli c++ core
    algorithms. Holds reference to a real matrix, providing the correct
    multiplication algorithms for the pygimli inversion process.
    """
    def __init__(self, mat, copy=False):
        super().__init__()

        self.ndim = 2

        # print('real Matrix')
        if isinstance(mat, str):
            self.M = np.load(mat)
        else:
            if copy is True:
                self.M = np.copy(mat)
            else:
                self.M = mat

    def __repr__(self):
        return self.M.__repr__()

    def rows(self):
        return np.shape(self.M)[0]

    def cols(self):
        return np.shape(self.M)[1]

    def save(self, name):
        np.save(name, self.M)

    def mult(self, vector):
        return self.M.dot(vector)

    def transMult(self, vector):
        return np.dot(vector, self.M)


def ComplexNumpyMatrix(mat, copy=False):
    return RealNumpyMatrix(np.row_stack([mat.real, mat.imag]), copy=copy)


def NumpyMatrix(mat, copy=False):
    """
    Matrix Wrapper for for ndarrays, providing syntax for pygimli c++ core
    algorithms (rows, cols, mult, transMult, save(numpy)).
    """
    if isinstance(mat, str):
        mat = np.load(mat)

    if isinstance(mat[0][0], complex):
        return ComplexNumpyMatrix(mat, copy=copy)
    else:
        return RealNumpyMatrix(mat, copy=copy)
# The End
