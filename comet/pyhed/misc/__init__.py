"""
Module comet/pyhed/misc
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# simple submodule import
from . matrixWrapper import NumpyMatrix
from . toolbox import (NamespaceError, insert, refine,
                       printv, project1DModel, convertCoordinates,
                       floatString, setNearestMarkers, temporal_printoptions,
                       plt_ioff, plt_ion, getAllValuesByReference,
                       progressBar)
from . vec import (fixSingularity, KtoP, KtoP_all, PtoK, PtoK_field,
                   KtoP_field, GridtoVector, VectortoGrid, translate,
                   translate_all, translateToDipole, rotate3, rotate3_all,
                   R3VtoNumpy, regular_slice, regular_sliceFrom3DMesh,
                   interpolateField_Matrix, interpolateField_rotatedMatrix,
                   interpolateField, interpolateVector, pointDataToCellData_np,
                   getRSparseValues, convertCRStoMap, fillCRS, uniqueAndSum,
                   perimeterFromPolyPoints, sumBetweenIndices, cumsumDepth,
                   sinhspace, areaFromPolyPoints, Ca2Cy, Cy2Ca, Ca2CyField,
                   Cy2CaField, getIndicesFromConstraintMatrix, getConstraints,
                   sinhZVolumeFunction, linspace2D, linspace3D,
                   rotationMatrix, angle, rotFromAtoB)
from . import vec
from . console_call import (tetgen151, embeddedMPIRun, embeddedMPIRun_bash,
                            local_apps, local_apps_bash)
from . poly_tools import createPolyBoxWithHalfspace, cleanUpTetgenFiles
from . load_save import (exportSparseMatrixAsNumpyArchive, json2Dict,
                         loadSparseMatrixFromNumpyArchive, dump2Json)
from . timer import Timer, NoneTimer
from . para_lib import InterpolationMatrix_para

from . mesh_tools import createH2, sameGeometry, createConstraintMesh

try:
    from . mpi_tools import abortIfError, saveFenicsField
except ImportError as e:
    pass
# THE END
