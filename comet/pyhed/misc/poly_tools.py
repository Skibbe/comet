"""
Part of comet/pyhed/misc
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import os


def createPolyBoxWithHalfspace(minx, maxx, miny, maxy, minz, maxz,
                               halfspace_at=0.0,
                               without_halfspace=False,
                               interface_marker=None):
    """ Creates a simple poly file for further mesh build processes.

    Imports pygimli.

    Parameters
    ----------

    minx: float
        Minimum x dimension.

    maxx: float
        Maximum x dimension.

    miny: float
        Minimum y dimension.

    maxy: float
        Maximum y dimension.

    minz: float
        Minimum z dimension.

    maxz: float
        Maximum z dimension.

    halfspace_at: float
        Z value where the halfspace is considered. Additionally to the corner
        points of the simple halfspace box, a separation of the z edges will
        be at *halfspace_at*.

    without_halfspace: booleam [ False ]
        An face that closes the 4 edge points at *halfspace_at* is inserted.
        This can be ommitted if creating but a tetrahedron boundary around
        another polygon.

    interface_marker: integer [ None ]
        Optional marker for the interface face, for later identification.

    Returns
    -------

        pg.Mesh
            Closed polygon mesh with or without face at the halfspace
            interface. Note that the first four nodes in the polygon correspond
            to the four edge nodes at the halfspace interface, for manual
            connection to other polygons.
    """
    import pygimli as pg

    if minz > halfspace_at:
        halfspace_at = minz
        print('set halfspace_at to minz: {}'.format(minz))

    elif maxz < halfspace_at:
        halfspace_at = maxz
        print('set halfspace_at to maxz: {}'.format(maxz))

    poly = pg.Mesh(3)
    ob8 = poly.createNode(minx, miny, halfspace_at)
    ob9 = poly.createNode(maxx, miny, halfspace_at)
    ob10 = poly.createNode(maxx, maxy, halfspace_at)
    ob11 = poly.createNode(minx, maxy, halfspace_at)
    # halfspace face

    if without_halfspace is False:
        # air ground interface
        if interface_marker is not None:
            poly.createQuadrangleFace(ob8, ob9, ob10, ob11,
                                      marker=interface_marker)
        else:
            poly.createQuadrangleFace(ob8, ob9, ob10, ob11)

    if minz < halfspace_at:
        # lower outer boundary nodes, earthspace
        ob0 = poly.createNode(minx, miny, minz)
        ob1 = poly.createNode(maxx, miny, minz)
        ob2 = poly.createNode(maxx, maxy, minz)
        ob3 = poly.createNode(minx, maxy, minz)
        # earth side
        poly.createQuadrangleFace(ob8, ob9, ob1, ob0)
        poly.createQuadrangleFace(ob9, ob10, ob2, ob1)
        poly.createQuadrangleFace(ob10, ob11, ob3, ob2)
        poly.createQuadrangleFace(ob11, ob8, ob0, ob3)
        # earth bottom
        poly.createQuadrangleFace(ob0, ob1, ob2, ob3)

    if maxz > halfspace_at:
        # upper outer boundary nodes, airspace
        ob4 = poly.createNode(minx, miny, maxz)
        ob5 = poly.createNode(maxx, miny, maxz)
        ob6 = poly.createNode(maxx, maxy, maxz)
        ob7 = poly.createNode(minx, maxy, maxz)
        # air side
        poly.createQuadrangleFace(ob8, ob9, ob5, ob4)
        poly.createQuadrangleFace(ob9, ob10, ob6, ob5)
        poly.createQuadrangleFace(ob10, ob11, ob7, ob6)
        poly.createQuadrangleFace(ob11, ob8, ob4, ob7)
        # air top
        poly.createQuadrangleFace(ob4, ob5, ob6, ob7)
    return poly


def cleanUpTetgenFiles(basename):
    """ Removes temporary tetgen files.

    Parameters
    ----------

    basename: string
        File path. All files with that basename and one of the following
        endings will be removed ('.poly', '.ele', '.node' '.face' '.edge').
    """
    if basename[-4:] == '.bms':
        basename = basename[:-4]
    for extension in ['.1.ele', '.1.edge', '.1.node', '.1.face', '.poly']:
        if os.path.exists(basename + extension):
            os.remove(basename + extension)

if __name__ == '__main__':
    pass

# The End
