"""
Part of comet/pyhed apps

Utlizing custEM (Rochlitz et al., 2018) for total field calculation of a
given loop.
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# =============================================================================
# This code is externally called and working in an mpi environment
# =============================================================================
from custEM.core import MOD

from comet import pyhed as ph
import sys

# small input check to save time
largv = len(sys.argv)

if len(sys.argv) != 2:
    raise Exception('Expected 2 input arguments, {} given: {}'
                    .format(largv, sys.argv))

# with ph.misc.abortIfError():

# get input
cfgname = sys.argv[1]

# import loop excluding mesh (mesh is not needed)
custem_config = ph.SecondaryConfig(name=cfgname)

Mod = MOD(custem_config.mod_name,
          custem_config.mesh_name,
          'E_t',
          p=custem_config.polynominal_order,
          overwrite_results=True,
          r_dir=custem_config.r_dir,
          m_dir=custem_config.m_dir)


Mod.MP.update_model_parameters(
    f=custem_config.frequency,
    sigma_ground=custem_config.sigma_ground + custem_config.sigma_anom,
    sigma_0=custem_config.sigma_ground)

Mod.FE.build_var_form()

Mod.solve_main_problem()

# The End
