"""
Part of comet/pyhed apps

Utlizing custEM (Rochlitz et al., 2018) for total field interpolation of
given loops to a kernel discretization in 2D.
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# =============================================================================
# This code is externally called and working in an mpi environment
# =============================================================================
from custEM.core import MOD
from mpi4py import MPI


import sys
# import main parts of COMET
from comet import snmr
from comet.pyhed.config import SecondaryConfig

import json
import os


# =========================================================================
# User input
# =========================================================================

json_name = sys.argv[1]

with open(json_name, 'r') as infile:
    # content = infile.read()s
    jsn_in = json.load(infile)

survey = jsn_in['survey']
invmesh = jsn_in['invmesh']
h_order= jsn_in['h_order']
cfgname = jsn_in['cfgname']
number_of_used_cores = jsn_in['num_cpu']
max_length = jsn_in['max_length']
max_num = jsn_in['max_num']
path_name = jsn_in['path_name']
force_new_paths = jsn_in['force_new_paths']
slice_export_name = jsn_in['slice_export_name']

# with ph.misc.abortIfError():

# import survey
site = snmr.survey.Survey()
site.load(survey, load_meshes=False)
kern = site.createKernel(fid=0, dimension=2)
kern.createYVec(max_length=max_length, max_num=max_num)
kern.set2DKernelMesh(invmesh, order=h_order)

inputcoords = kern.getSliceCoords()
custem_config = SecondaryConfig(name=cfgname)

# import previously calculated fields
Mod = MOD(custem_config.mod_name,
          custem_config.mesh_name,
          'E_t',
          p=custem_config.polynominal_order,
          overwrite=False,
          load_existing=True,
          r_dir=custem_config.r_dir,
          m_dir=custem_config.m_dir)

meshs = []
absolute_names = []
for kk in range(len(kern.yvec) - 1):
    meshs.append(f'{path_name}_{kk}_path')
    absolute_names.append(
        f'{custem_config.m_dir}/paths/{path_name}_{kk}_path.xml')

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    make_meshs = False or force_new_paths

    for mesh_name in absolute_names:
        if os.path.exists(mesh_name):
            if force_new_paths:
                os.remove(mesh_name)
            else:
                continue
        else:
            make_meshs = True

else:
    make_meshs = None

make_meshs = comm.bcast(make_meshs, root=0)

if make_meshs:
    Mod.IB.create_path_meshes(inputcoords, path_name=path_name,
                              max_new_procs_spawn=number_of_used_cores)

Mod.IB.interpolate(meshs,
                   ['H_t'],
                   export_pvd=False,
                   export_name=slice_export_name)

# Mod.IB.synchronize()

# The End
