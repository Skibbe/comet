# -*- coding: utf-8 -*-

# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

scriptname=$1
cfgname=$2
num_cpu=$3

echo "Starting custem interpolation routine."

# check command
command="mpirun -n $num_cpu python $scriptname $cfgname"

echo "calling : $command"

mpirun -n "$num_cpu" python "$scriptname" "$cfgname"
# check command output
pysuccess=$?

# exit script
if [ $pysuccess -eq 0 ]; then
    echo 'close mpi environment'
else
    exit 1
fi

# The End
