"""
Part of comet/pyhed apps

Utlizing custEM (Rochlitz et al., 2018) for secondary field calculation of a
given loop.
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# =============================================================================
# This code is externally called and working in an mpi environment
# =============================================================================

from custEM.misc import mpi_print as mpp
from comet import pyhed as ph
import sys

# from custEM.core import MOD

ph.log.setLevel(13)

# small input check to save time
largv = len(sys.argv)

if len(sys.argv) != 2:
    raise Exception('Expected 2 input arguments, {} given: {}'
                    .format(largv, sys.argv))

log_lvl = ph.log.getEffectiveLevel()
mpp('log level: {}'.format(log_lvl))
sys.stdout.flush()

# overwatching mpi processes to avoid zombies in case of an exception
with ph.misc.abortIfError():

    # get input
    loopname = sys.argv[1]
    # approach = sys.argv[2]
    # pf_EH_flag = 'E' if 'E' in approach else 'H'

    # import loop excluding mesh (mesh is not needed)
    loop = ph.loop.loadLoop(loopname, verbose=False, load_meshes=False)
    if log_lvl <= 20:
        mpp('loop loaded')
        sys.stdout.flush()

    # initialize custEM modeling class, load secondary config
    # loop.initCustEM(init_primary_field_class=False)  # original!
    # secondary approach with e fields more stable
    loop.initCustEM(secondary_config=loop.secondary_config,
                    init_primary_field_class=False)
    # == MOD()
    #  + MOD.MP.update_model_parameters(...)

    if log_lvl <= 20:
        mpp('initialized custEM')
        sys.stdout.flush()

    # pf_EH_flag -> default 'E'
    loop.secMOD.FE.build_var_form(
        pf_type=loop.secondary_config.pf_type,
        pf_name=loop.secondary_config.pf_name,
        pf_EH_flag=loop.secondary_config.pf_EH_flag,
        s_type='loop_r')

    if log_lvl <= 20:
        mpp('build var form finished')
        sys.stdout.flush()

    # solving problem, custEM calculates the secondary field
    if log_lvl <= 20:
        mpp('start solving main problem')
        sys.stdout.flush()

    # also does conversion from Nedelec to Lagrange for export
    loop.secMOD.solve_main_problem()

    if log_lvl <= 20:
        mpp('finished solving main problem and converting')
        sys.stdout.flush()

    ph.misc.saveFenicsField(loopname, loop, secondary=True)

    if log_lvl <= 20:
        mpp('successfully exported fields')
        sys.stdout.flush()

# The End
