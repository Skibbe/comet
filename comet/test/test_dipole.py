"""
Part of comet

This is not a full-fledged test. It tests certain features where bugs where
found in the past.

So if this passes: comet may still have a problem
but if this fails: comet definitively has a problem
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import os
import unittest
import scipy
import pygimli as pg
import comet.pyhed as ph
import numpy as np
from comet import snmr, basepath

import multiprocessing as mpc

ph.log.setLevel(30)

class TestRotations(unittest.TestCase):
    def test_vector_rotation_single(self):
        a = np.array([1, 0, 0])
        axis = np.array([1, -3, 1e-3])
        theta = np.pi / 2
        m = ph.misc.rotationMatrix(axis, theta)

        b = a.dot(m)
        for i in range(3):
            self.assertFalse(np.allclose(a, b))
            b = b.dot(m)

        self.assertTrue(np.allclose(a, b))

    def test_dot_broad(self):

        np.random.seed(seed=42)
        a = np.random.randn(30).reshape(10, 3)
        b = np.random.randn(30).reshape(10, 3)

        single_dots = np.zeros(a.shape[0])
        for ii in range(a.shape[0]):
            single_dots[ii] = a[ii].dot(b[ii])

        broad_dots = np.einsum('ij,ij->i', a, b)
        self.assertTrue(np.allclose(single_dots, broad_dots))

        # einsum can simply switch dimensions
        at = a.T
        bt = b.T
        broad_dotsT = np.einsum('ij,ij->j', at, bt)
        self.assertTrue(np.allclose(broad_dots, broad_dotsT))

        test_3d = np.array([1.3, -2.1, 12])

        self.assertEqual(1.3, test_3d.dot(np.array([1, 0, 0])))
        self.assertEqual(-2.1, test_3d.dot(np.array([0, 1, 0])))
        self.assertEqual(12, test_3d.dot(np.array([0, 0, 1])))

        ax = np.array([-1, -3, 6.1])
        theta = np.pi / 1.234
        m = ph.misc.rotationMatrix(ax, theta)
        test_3d_rot = test_3d.dot(m)

        frame1 = np.array([1, 0, 0]).dot(m)
        frame2 = np.array([0, 1, 0]).dot(m)
        frame3 = np.array([0, 0, 1]).dot(m)

        self.assertAlmostEqual(1.3, test_3d_rot.dot(frame1))
        self.assertAlmostEqual(-2.1, test_3d_rot.dot(frame2))
        self.assertAlmostEqual(12, test_3d_rot.dot(frame3))

    def test_vector_rotation_broad(self):

        np.random.seed(seed=4241)
        a = np.random.randn(30).reshape(10, 3)
        ax = np.random.randn(30).reshape(3, 10)
        theta = np.ones(a.shape[0]) * np.pi / 2
        m = ph.misc.rotationMatrix(ax, theta)

        b_manually = np.zeros_like(a)
        for k in range(10):
            b_manually[k] = a[k].dot(m[:, :, k])

        b = np.einsum('ki,ijk->kj', a, m)
        self.assertTrue(np.allclose(b, b_manually))

        for i in range(3):
            self.assertFalse(np.allclose(a, b))
            b_new = np.einsum('ki,ijk->kj', b, m)
            b = b_new

        self.assertTrue(np.allclose(a, b))

    def test_rotA2B(self):
        vec = np.array([2, 1, 0])
        a = np.array([4, 2, 0])
        b = np.array([0, 2, 1])
        vec_rot = ph.misc.rotFromAtoB(vec, a, b)
        bn = b / scipy.linalg.norm(b)
        vec_rot_analytical = bn * scipy.linalg.norm(vec)
        self.assertTrue(np.allclose(vec_rot_analytical, vec_rot))
        self.assertTrue(scipy.linalg.norm(vec_rot), scipy.linalg.norm(vec))

    def test_rotA2B_broad(self):
        vec = np.array([[2, 3, 0], [0, 2, 3]])
        a = np.array([[4, 6, 0], [0, 4, 6]])
        b = np.array([[0, 2, 5], [2, 5, 0]])
        vec_rot = ph.misc.rotFromAtoB(vec, a, b)
        bn = b / scipy.linalg.norm(b)
        vec_rot_analytical = bn * scipy.linalg.norm(vec)
        self.assertTrue(np.allclose(vec_rot_analytical, vec_rot))
        self.assertTrue(scipy.linalg.norm(vec_rot), scipy.linalg.norm(vec))


class TestIntegration(unittest.TestCase):
    def test_integration(self):
        # set up test coords
        loop = ph.loop.buildCircle(2, num_segs=24)
        loop2 = ph.loop.buildLoop(
            [[-30, -30, 0], [-35, -15, 0], [-5, 5, 0],
             [5, -5, 0], [-10, -10, 0], [-30, -15, 0]],
            max_length=0.51,
            grounded=False)

        # constant field, any closed path integration has to be zero
        dipoles = np.zeros(loop.pos.shape)
        dipoles[:, 0] = np.cos(loop.phi)
        dipoles[:, 1] = np.sin(loop.phi)

        field = np.zeros(loop.pos.shape)
        field[:, 0] = 1.0

        int1 = snmr.modelling.integrate(field, dipoles, loop.ds)
        # print('test1: int1: {}'.format(int1))
        self.assertTrue(np.isclose(int1, 0.0))

        dipoles2 = np.zeros(loop2.pos.shape)
        dipoles2[:, 0] = np.cos(loop2.phi)
        dipoles2[:, 1] = np.sin(loop2.phi)

        field2 = np.zeros(loop2.pos.shape)
        field2[:, 0] = 1.0

        int2 = snmr.modelling.integrate(field2, dipoles2, loop2.ds)
        # print('test1: int2: {}'.format(int2))

        self.assertTrue(np.isclose(int2, 0.0))

    def test_integration_discretization(self):
        # %%
        anax = 21.3221

        # integrate along wire for different discretizations
        rx1 = ph.loop.buildLoop(
            [[-10 - anax, -30, 0], [-35, -15, 0], [-5, 5, 0],
             [5, -5, 0], [-10, -10, 0], [-10, -15, 0]],
            max_length=0.251,
            grounded=True)

        rx2 = ph.loop.buildLoop(
            [[-10 - anax, -30, 0], [-35, -15, 0], [-5, 5, 0],
             [5, -5, 0], [-10, -10, 0], [-10, -15, 0]],
            max_length=0.51,
            grounded=True)

        rx3 = ph.loop.buildLoop(
            [[-10 - anax, -30, 0], [-35, -15, 0], [-5, 5, 0],
             [5, -5, 0], [-10, -10, 0], [-10, -15, 0]],
            max_length=0.051,
            grounded=True)

        # import matplotlib.pyplot as plt
        # rx3.show()
        # plt.show()

        d1 = np.zeros(rx1.pos.shape)
        d1[:, 0] = np.cos(rx1.phi)
        d1[:, 1] = np.sin(rx1.phi)

        d2 = np.zeros(rx2.pos.shape)
        d2[:, 0] = np.cos(rx2.phi)
        d2[:, 1] = np.sin(rx2.phi)

        d3 = np.zeros(rx3.pos.shape)
        d3[:, 0] = np.cos(rx3.phi)
        d3[:, 1] = np.sin(rx3.phi)

        # only x directed field
        f1 = np.zeros(rx1.pos.shape)
        f1[:, 0] = 1.0

        f2 = np.zeros(rx2.pos.shape)
        f2[:, 0] = 1.0

        f3 = np.zeros(rx3.pos.shape)
        f3[:, 0] = 1.0

        int1 = snmr.modelling.integrate(f1, d1, rx1.ds)
        int2 = snmr.modelling.integrate(f2, d2, rx2.ds)
        int3 = snmr.modelling.integrate(f3, d3, rx3.ds)

        # print('int1: {}'.format(int1))
        # print('int2: {}'.format(int2))
        # print('int3: {}'.format(int3))

        # %%
        # only difference between start and endpoint matters
        # difference is exactly "anax"
        #
        self.assertAlmostEqual(int1, anax)
        self.assertAlmostEqual(int2, anax)
        self.assertAlmostEqual(int3, anax)

    def test_curl(self):
        # set up test coords
        loop = ph.loop.buildLine([-30, 12, 0], [63, 50, 0],
                                  num_segs=120)
        # loop = ph.loop.buildCircle(12, P=[-10, 3, 0],
        #                             num_segs=30)
        shift = np.zeros(loop.pos.shape)
        shift[:, 2] = np.linspace(-2, 1, loop.pos.shape[0])
        loop.pos -= shift
        loop.pos[:, 2] += np.pi/2
        # loop.pos[:, 2] = 0

        d_int = 0.0005
        pos, direction, dlen =\
            snmr.modelling.createIntegrationPoints(loop.pos, d=d_int)

        mid_pos = (loop.pos[:-1] + loop.pos[1:]) / 2
        mid_dir = loop.pos[1:] - loop.pos[:-1]
        mid_dir = mid_dir / scipy.linalg.norm(mid_dir, axis=1)[:, np.newaxis]

        # constant field = 0 cur
        field_0curl = np.zeros(pos.shape)
        field_0curl[:, 0] = 1.0

        # field with curl != 0
        #                  (x * y)
        # field(x, y, z) = (cos(z))
        #                  (z**2 + y)
        field = np.zeros(pos.shape)
        field[:, 0] = pos[:, 0] * pos[:, 1]
        field[:, 1] = np.cos(pos[:, 2])
        # field[:, :, 1] = pos[:, :, 1] ** 2
        field[:, 2] = pos[:, 2]**2 + pos[:, 1]

        # analytic curl:
        #                       (1 + sin(z))
        # curl field(x, y, z) = (0)
        #                       (-x)
        ana_curl = np.zeros(mid_pos.shape)
        ana_curl[:, 0] = 1 + np.sin(mid_pos[:, 2])
        # ana_curl[:, 0] = 1 + 2 * mid_pos[:, 2]
        # ana_curl[:, 1] = 2 * mid_pos[:, 2]
        ana_curl[:, 2] = - mid_pos[:, 0]

        # field_0curl_stacked =\
        #     np.stack(np.array_split(field_0curl, 4, axis=0), axis=1)
        # field_stacked =\
        #     np.stack(np.array_split(field, 4, axis=0), axis=1)

        # explicit curls
        projected_field_0curl =\
            np.einsum('...k, ...k->...', field_0curl, direction)
        ecurl_0curl_mag =\
            snmr.modelling.explicit_curl(projected_field_0curl, d_int)
        integrated_0curl = np.sum(ecurl_0curl_mag * dlen)

        # for Thomas(es) with love
        # projected_field_0curl_2 = np.zeros(field_0curl.shape[:-1])
        # for i in range(field_0curl.shape[0]):
        #     for j in range(field_0curl.shape[1]):
        #         projected_field_0curl_2[i, j] = field_0curl[i, j].dot(direction[i, j])

        # has to be zero
        # print(np.allclose(integrated_0curl, 0.0))
        self.assertAlmostEqual(integrated_0curl, 0.0)

        projected_field = np.einsum('...k, ...k->...', field, direction)
        # projected_field = np.zeros(field.shape[:-1])
        # for i in range(field.shape[0]):
        #     for j in range(field.shape[1]):
        #         projected_field[i, j] = field[i, j].dot(direction[i, j])

        ecurl_mag = snmr.modelling.explicit_curl(projected_field, d_int)
        integrated = np.sum(ecurl_mag * dlen)

        # has to be close to analytic curl
        ana_curl_proj2 = np.einsum('ij,ij->i', ana_curl, mid_dir)
        ana_curl_proj = np.zeros(ana_curl.shape[:-1])
        for i in range(ana_curl.shape[0]):
            ana_curl_proj[i] = ana_curl[i].dot(mid_dir[i])

        # print(np.allclose(ana_curl_proj, ana_curl_proj2))
        self.assertTrue(np.allclose(ana_curl_proj, ana_curl_proj2))

        # print(ecurl_mag)
        # print(ana_curl_proj)
        # import matplotlib.pyplot as plt
        # plt.plot(ecurl_mag, label='integrated')
        # plt.plot(ana_curl_proj, label='analytical')
        # plt.legend()
        # plt.show()
        # print(ecurl_mag - ana_curl_proj)
        self.assertTrue(np.allclose(ecurl_mag, ana_curl_proj))

    def test_efield_int(self):
        # %%
        from comet.snmr.modelling import (integrate, explicit_curl,
                                          createIntegrationPoints)
        from comet.pyhed.loop import (buildLine, calcWithEmpymod)

        d = 0.0001
        tx1 = [-3, 3, 0]
        tx2 = [4, -5, 0]
        rx1 = [-5, 1, -5.2]
        rx2 = [10, 3, -5.1]

        txE = buildLine(tx1, tx2, num_segs=50)
        txE.config.ftype = 'E'

        txH = buildLine(tx1, tx2, num_segs=50)
        txH.config.ftype = 'H'

        rx_pos_outer = ph.misc.linspace3D(rx1, rx2, 50)
        rx_pos = (rx_pos_outer[1:] + rx_pos_outer[:-1]) / 2
        rx_int_pos, rx_int_dir, rx_int_len =\
            createIntegrationPoints(rx_pos_outer, d=d)

        rx_dir = rx_pos_outer[1:] - rx_pos_outer[:-1]
        rx_dir = rx_dir / scipy.linalg.norm(rx_dir, axis=1)[:, np.newaxis]

        txE.setLoopMesh(rx_pos)
        txH.setLoopMesh(rx_int_pos)
        txE.pos[:, 2] -= 5
        txH.pos[:, 2] -= 5

        fieldE = calcWithEmpymod(txE)
        fieldH = calcWithEmpymod(txH)
        # fieldE = txE.calculate()
        # fieldH = txH.calculate()

        magnitudes = np.einsum('ij,ji->j', fieldH, rx_int_dir)

        # E = 1/sig * rot(H)
        fieldEH = explicit_curl(magnitudes, d) * txH.config.rho[0]
        fieldEE = np.einsum('ij,ji->j', fieldE, rx_dir)

        np.allclose(fieldEH,fieldEE)

        # import matplotlib.pyplot as plt
        # fig, ax = plt.subplots(1, 2)
        # ax[0].plot(fieldEH.real, label='int, real')
        # ax[1].plot(fieldEH.imag, label='int, imag')
        # ax[0].plot(fieldEE.real, label='ana, real')
        # ax[1].plot(fieldEE.imag, label='ana, imag')

        # handles = []
        # labels = []
        # for axi in ax.flat:
        #     handlesi, labelsi = axi.get_legend_handles_labels()
        #     axi.legend(handlesi, labelsi)
        # plt.show()

        intEH = snmr.modelling.integrate(fieldEH, rx_dir, rx_int_len)[0]
        intEE = snmr.modelling.integrate(fieldEE, rx_dir, rx_int_len)[0]

        # diff = (intEH - intEE) / intEE
        # print('{} @ d = {}'.format(diff.real * 100, d))
        # print('{} @ d = {}'.format(diff.imag * 100, d))
        # %%
        self.assertTrue(
            np.allclose(fieldEH.real, fieldEE.real, rtol=0.0001))
        self.assertTrue(
            np.allclose(fieldEH.imag, fieldEE.imag, atol=np.mean(np.abs(fieldEE.real)) * 0.001))
        self.assertTrue(np.isclose(intEH, intEE, rtol=1e-3))

        # %%

    def test_full_curl(self):
        # %%
        # set up test coords
        loop = ph.loop.buildLoop([[-30, 3, 0],
                                  [10, -34, 0],
                                  [30, 15, 0],
                                  [63, -54, 0]],
                                  num_segs=120,
                                  grounded=True)
        # loop = ph.loop.buildCircle(12, P=[-10, 3, 0],
        #                             num_segs=30)
        shift = np.zeros(loop.pos.shape)
        shift[:, 2] = np.linspace(-2, 1, loop.pos.shape[0])
        loop.pos -= shift
        # loop.pos[:, 2] = np.pi/2
        # loop.pos[:, 2] = 0

        mid_pos = (loop.pos[1:] + loop.pos[:-1]) / 2
        mid_dir = loop.pos[1:] - loop.pos[:-1]
        mid_dir = mid_dir / scipy.linalg.norm(mid_dir, axis=1)[:, np.newaxis]

        d = 0.05
        curl_pos = np.repeat(mid_pos[np.newaxis, :, :], 6, axis=0)
        # shift x
        curl_pos[0, :, 0] += d/2
        curl_pos[1, :, 0] += -d/2
        # shift y
        curl_pos[2, :, 1] += d/2
        curl_pos[3, :, 1] += -d/2
        # shift z
        curl_pos[4, :, 2] += d/2
        curl_pos[5, :, 2] += -d/2

        hlp1 = curl_pos.reshape(-1, 3)
        hlp2 = hlp1.reshape(6, -1, 3)

        self.assertTrue(np.allclose(hlp2, curl_pos))

        # field with curl != 0
        #                  (x * y)
        # field(x, y, z) = (cos(z))
        #                  (z**2 + y)
        field = np.zeros(curl_pos.shape)
        field[:, :, 0] = curl_pos[:, :, 0] * curl_pos[:, :, 1]
        field[:, :, 1] = np.cos(curl_pos[:, :, 2])
        field[:, :, 2] = curl_pos[:, :, 2]**2 + curl_pos[:, :, 1]

        # analytic curl:
        #                       (1 + sin(z))
        # curl field(x, y, z) = (0)
        #                       (-x)
        ana_curl = np.zeros(mid_pos.shape)
        ana_curl[:, 0] = 1 + np.sin(mid_pos[:, 2])
        ana_curl[:, 2] = - mid_pos[:, 0]

        # explicit curl
        fydx = (field[0, :, 1] - field[1, :, 1]) / d
        fzdx = (field[0, :, 2] - field[1, :, 2]) / d
        fxdy = (field[2, :, 0] - field[3, :, 0]) / d
        fzdy = (field[2, :, 2] - field[3, :, 2]) / d
        fxdz = (field[4, :, 0] - field[5, :, 0]) / d
        fydz = (field[4, :, 1] - field[5, :, 1]) / d
        exp_curl = np.zeros(mid_pos.shape)
        exp_curl[:, 0] = fzdy - fydz
        exp_curl[:, 1] = fxdz - fzdx
        exp_curl[:, 2] = fydx - fxdy

        self.assertTrue(np.allclose(ana_curl, exp_curl, rtol=1e-3))

        if ph.log.getEffectiveLevel() <= 10:
            import matplotlib.pyplot as plt
            fig, ax = plt.subplots(3, 1)
            ax[0].plot(exp_curl[:, 0], label='integrated fx')
            ax[0].plot(ana_curl[:, 0], label='analytical fx')
            ax[1].plot(exp_curl[:, 1], label='integrated fy')
            ax[1].plot(ana_curl[:, 1], label='analytical fy')
            ax[2].plot(exp_curl[:, 2], label='integrated fz')
            ax[2].plot(ana_curl[:, 2], label='analytical fz')
            plt.legend()
            plt.show()

        # %%
    def field_test_empymod_E(self):
        # %%
        from comet.pyhed.loop import (buildLine, calcWithEmpymod)
        from comet.pyhed.misc import linspace3D
        loop1 = buildLine([-3, 3, 0], [-3, -3, 0], num_segs=50)
        loop1.config.f = 2000
        loop1.setModel(rho=[1000, 20, 500], d=[6, 6])
        loop1.config.ftype='E'

        rx = linspace3D([-20, -13, -3], [-25, -34, -15], 20)
        loop1.setLoopMesh(rx)
        loop1.calculate(num_cpu=4)

        pyf = loop1.field
        empf = calcWithEmpymod(loop1)

        # import matplotlib.pyplot as plt
        # fig, ax = plt.subplots(3, 2)
        # ax[0, 0].plot(pyf[0, :].real, label='pyhed fx, real')
        # ax[1, 0].plot(pyf[1, :].real, label='pyhed fy, real')
        # ax[2, 0].plot(pyf[2, :].real, label='pyhed fz, real')
        # ax[0, 1].plot(pyf[0, :].imag, label='pyhed fx, imag')
        # ax[1, 1].plot(pyf[1, :].imag, label='pyhed fy, imag')
        # ax[2, 1].plot(pyf[2, :].imag, label='pyhed fz, imag')

        # ax[0, 0].plot(empf[0, :].real, label='empymod fx, real')
        # ax[1, 0].plot(empf[1, :].real, label='empymod fy, real')
        # ax[2, 0].plot(empf[2, :].real, label='empymod fz, real')
        # ax[0, 1].plot(empf[0, :].imag, label='empymod fx, imag')
        # ax[1, 1].plot(empf[1, :].imag, label='empymod fy, imag')
        # ax[2, 1].plot(empf[2, :].imag, label='empymod fz, imag')

        # handles = []
        # labels = []
        # for axi in ax.flat:
        #     handlesi, labelsi = axi.get_legend_handles_labels()
        #     axi.legend(handlesi, labelsi)
        # plt.show()
        # %%
        self.assertTrue(np.allclose(pyf, empf, rtol=0.01))

    def field_test_empymod_H(self):
        # %%
        from comet.pyhed.loop import (buildLine, calcWithEmpymod)
        from comet.pyhed.misc import linspace3D
        loop1 = buildLine([-3, 3, 0], [-3, -3, 0], num_segs=50)
        loop1.config.f = 2000
        loop1.setModel(rho=[1000, 20, 500], d=[6, 6])
        loop1.config.ftype='H'

        rx = linspace3D([-20, -13, -3], [-25, -34, -15], 20)
        loop1.setLoopMesh(rx)
        loop1.calculate(num_cpu=4)

        pyf = loop1.field
        empf = calcWithEmpymod(loop1)

        # import matplotlib.pyplot as plt
        # fig, ax = plt.subplots(3, 2)
        # ax[0, 0].plot(pyf[0, :].real, label='pyhed fx, real')
        # ax[1, 0].plot(pyf[1, :].real, label='pyhed fy, real')
        # ax[2, 0].plot(pyf[2, :].real, label='pyhed fz, real')
        # ax[0, 1].plot(pyf[0, :].imag, label='pyhed fx, imag')
        # ax[1, 1].plot(pyf[1, :].imag, label='pyhed fy, imag')
        # ax[2, 1].plot(pyf[2, :].imag, label='pyhed fz, imag')

        # ax[0, 0].plot(empf[0, :].real, label='empymod fx, real')
        # ax[1, 0].plot(empf[1, :].real, label='empymod fy, real')
        # ax[2, 0].plot(empf[2, :].real, label='empymod fz, real')
        # ax[0, 1].plot(empf[0, :].imag, label='empymod fx, imag')
        # ax[1, 1].plot(empf[1, :].imag, label='empymod fy, imag')
        # ax[2, 1].plot(empf[2, :].imag, label='empymod fz, imag')

        # handles = []
        # labels = []
        # for axi in ax.flat:
        #     handlesi, labelsi = axi.get_legend_handles_labels()
        #     axi.legend(handlesi, labelsi)
        # plt.show()
        # %%
        self.assertTrue(np.allclose(pyf, empf, rtol=0.01))

    def test_reverse_dipole(self):
        # %%
        from comet.snmr.modelling import (integrate, explicit_curl,
                                          createIntegrationPoints)
        from comet.pyhed.loop import buildLine, calcWithEmpymod
        from comet.pyhed.misc import linspace3D
        import matplotlib.pyplot as plt

        tx_s = [-3, -3, 0]
        tx_e = [-3, 3, 0]
        rx_s = [-5, 2, -1]
        rx_e = [5, -4, -7]
        rho = [1000, 50, 500]
        thk = [6, 6]
        # sig = 1. / rho
        rx = linspace3D(rx_s, rx_e, 50)
        txE = buildLine(tx_s, tx_e, num_segs=50)
        txE.config.ftype = 'B'
        txE.setModel(rho=rho, d=thk)
        txE.setLoopMesh(rx)

        txE.calculate(num_cpu=2)
        e_py = txE.field
        e_em = calcWithEmpymod(txE)
        # %%
        self.assertTrue(np.allclose(e_py, e_em))
        # %%
        # import matplotlib.pyplot as plt
        # fig, ax = plt.subplots(3, 2)
        # ax[0, 0].plot(e_py[0].real, label='pyhed fx, real')
        # ax[1, 0].plot(e_py[1].real, label='pyhed fy, real')
        # ax[2, 0].plot(e_py[2].real, label='pyhed fz, real')
        # ax[0, 1].plot(e_py[0].imag, label='pyhed fx, imag')
        # ax[1, 1].plot(e_py[1].imag, label='pyhed fy, imag')
        # ax[2, 1].plot(e_py[2].imag, label='pyhed fz, imag')

        # ax[0, 0].plot(e_em[0].real, label='empymod fx, real')
        # ax[1, 0].plot(e_em[1].real, label='empymod fy, real')
        # ax[2, 0].plot(e_em[2].real, label='empymod fz, real')
        # ax[0, 1].plot(e_em[0].imag, label='empymod fx, imag')
        # ax[1, 1].plot(e_em[1].imag, label='empymod fy, imag')
        # ax[2, 1].plot(e_em[2].imag, label='empymod fz, imag')

        # handles = []
        # labels = []
        # for axi in ax.flat:
        #     handlesi, labelsi = axi.get_legend_handles_labels()
        #     axi.legend(handlesi, labelsi)
        # plt.show()
        # %%

if __name__ == '__main__':
    unittest.main()

# The End
