"""
Part of comet

This is not a full-fledged test. It tests certain features where bugs where
found in the past.

So if this passes: comet may still have a problem
but if this fails: comet definitively has a problem
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2019  Nico Skibbe

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import os
import unittest
import pygimli as pg
import pyhed as ph
import numpy as np
from comet import snmr, basepath

import multiprocessing as mpc

ph.log.setLevel(30)

#import sys
#from contextlib import contextmanager
#from io import StringIO


#@contextmanager
#def captured_output():
#    new_out, new_err = StringIO(), StringIO()
#    old_out, old_err = sys.stdout, sys.stderr
#    try:
#        sys.stdout, sys.stderr = new_out, new_err
#        yield sys.stdout, sys.stderr
#    finally:
#        sys.stdout, sys.stderr = old_out, old_err


class TestH2(unittest.TestCase):

    def test_H2(self):
        a = pg.Mesh(2)
        a.createNode([0, 1.2])
        a.createNode([0, 0.])
        a.createNode([1.3, 2.4])
        a.createNode([0, -2.32124])
        a.createNode([22., 13.])
        a.createNode([12.5, -23.])

        a.createCell([0, 1, 2])
        a.addRegionMarker(a.cells()[0].center(), marker=0, area=1)
        a.createCell([1, 2, 3])
        a.addRegionMarker(a.cells()[1].center(), marker=1, area=1)
        a.createCell([2, 3, 4])
        a.addRegionMarker(a.cells()[2].center(), marker=2, area=1)
        a.createCell([3, 4, 5])
        a.addRegionMarker(a.cells()[3].center(), marker=3, area=1)
        a.createNeighbourInfos()
        f1 = np.ones(4) * 0.35 + 1j * np.ones(4) * 0.25
        f1[0] = 0.45 + 1j * -0.15

        for i in range(1, 3):
            order = i
            f2 = np.ones(4**(order+1)) * 0.35 + \
                1j * np.ones(4**(order+1)) * 0.25
            f2[0:4**order] = 0.45 + 1j * -0.15

            _, m12 = ph.misc.createH2(a, order=order, integration_mat=True)
            # pg.show(a2, data=f2.real, colorBar=True)
            # pg.show(a, data=f1.real, colorBar=True)

            f12 = snmr.kernel.integrateKernelH2(m12, f2)
            self.assertTrue(np.allclose(f1, f12))


class TestPyhed(unittest.TestCase):

    def test_config(self):
        loop_a = ph.loop.buildDipole((0., 0., 0.))
        # test setModel
        loop_a.setModel([10, 45, 56], d=[13, 34, 45], thickness=False)
        self.assertTrue(np.allclose(loop_a.config.d, np.array([21, 11])))
        self.assertTrue(np.allclose(loop_a.config.rho,
                                    np.array([10, 45, 56])))
        loop_a.setModel([10, 45, 56], d=[13, 34], resistivity=False)
        self.assertTrue(np.allclose(loop_a.config.d, np.array([13, 34])))
        self.assertTrue(np.allclose(loop_a.config.rho,
                                    1./np.array([10, 45, 56])))

        loop_a.setModel(1000)
        self.assertTrue(np.allclose(loop_a.config.rho, np.array([1000.])))
        self.assertTrue(np.allclose(loop_a.config.d, np.array([])))
        return True

    def test_project(self):
        vec1 = np.array([2, 2.5, 3])
        m1 = ph.misc.project1DModel([2.375], [100, 5], vec1)
        self.assertTrue(np.allclose([76.25, 5], m1))

        vec2 = np.array([17.5, 21.1])
        m2 = ph.misc.project1DModel([20, 1], [0, 2.4, 0], vec2)
        vec3 = np.array([17.5, 21])
        m3 = ph.misc.project1DModel([20, 1], [0, 2.4, 0], vec3)
        vec4 = np.array([17.5, 20.9])
        m4 = ph.misc.project1DModel([20, 1], [0, 2.4, 0], vec4)
        vec5 = np.array([17.5, 43.5])
        m5 = ph.misc.project1DModel([20, 1], [0, 2.4, 0], vec5)
        self.assertTrue(np.allclose(m2*np.diff(vec2), m3*np.diff(vec3)))
        self.assertTrue(np.allclose(m3*np.diff(vec3), m5*np.diff(vec5)))
        self.assertTrue(np.allclose(m4*np.diff(vec4), [2.16]))

    def test_get_item(self):
        from comet.pyhed.IO import getItem
        import tempfile as temp
        tempFile = temp.NamedTemporaryFile(suffix='.npz', delete=False)
        name = tempFile.name
        tempFile.close()

        np.savez(name, pulses=np.array([-1.4, 1e-3, 12342 + 1j]),
                 dt=1.420983476598235,
                 dtype='block',
                 verbose=True,
                 test=None)

        with np.load(name, allow_pickle=True) as archive:

            self.assertTrue(np.allclose(getItem(archive, 'pulses'),
                                        np.array([-1.4, 1e-3, 12342 + 1j])))

            self.assertTrue(np.isclose(getItem(archive, 'dt'),
                                       1.420983476598235))

            self.assertTrue(getItem(archive, 'dtype', 'smooth') == 'block')
            self.assertTrue(getItem(archive, 'verbose', False))
            self.assertTrue(getItem(archive, 'test') is None)

            self.assertTrue(getItem(archive, 'test2', None) is None)
            self.assertTrue(np.isclose(getItem(archive, 'test4', np.pi),
                                       np.pi))

        os.unlink(name)


class TestContainer(unittest.TestCase):

    def test_distributed_arrays(self):

        fid = snmr.survey.FID()
        fid.setPulses([1, 2, 3])
        fid2 = snmr.survey.FID()
        fid2.setPulses([4, 5, 6])

        site = snmr.survey.Survey()
        site.addSounding(fid)
        site.addSounding(fid2)

        # check general behavior
        self.assertTrue(np.allclose(fid2.pulses, site.pulses[1]))
        self.assertTrue(fid2.pulses is site.pulses[1])

        # check after if pulse update is noted
        fid.setPulses([-1, 15.534, 1e-11])
        self.assertTrue(np.allclose(fid.pulses, site.pulses[0]))
        self.assertTrue(fid.pulses is site.pulses[0])

        # check iterator
        for i, pulse in enumerate(site.pulses):
            self.assertTrue(pulse is site.fids[i].pulses)

    def test_import_mrsd(self):
        mrsd = basepath.joinpath('../test_data/data.mrsd')
        site = snmr.survey.Survey()
        site.loadMRSD(mrsd)

        fid = site.fids[0]
        fid.save('../test_data/test_fid.npz')

        fid2 = snmr.survey.FID()
        fid2.load('../test_data/test_fid.npz')

        self.assertTrue(np.allclose(fid.pulses, fid2.pulses))
        self.assertTrue(
            np.allclose(fid.data_raw, fid2.data_raw, atol=1e-11))
        self.assertTrue(
            np.allclose(fid.data_gated, fid2.data_gated, atol=1e-11))

        self.assertTrue(np.allclose(fid.times, fid2.times))
        self.assertTrue(fid.rotated == fid2.rotated)
        self.assertTrue(np.allclose(fid.gates, fid2.gates))
        self.assertTrue(np.allclose(fid.df, fid2.df))

        self.assertTrue(
            np.allclose(fid.error_gated, fid2.error_gated, atol=1e-11))

        site.save('../test_data/test_survey.npz')
        site.load('../test_data/test_survey.npz')

    def test_grid_search(self):
        from snmr.modelling.nmr_base import getPhiByGridSearch

        # data without phase at all
        test_data = np.zeros(120) + 1j * np.ones(120)

        test_data *= (np.cos(np.pi/6) + 1j*np.sin(np.pi/6))

        # should be 120°
        compare = np.ones(12) * 4 * np.pi/6

        # simulate 12 pulse moments
        test_data = test_data.reshape(12, -1)

        fitted = getPhiByGridSearch(test_data)

        test = np.allclose(fitted, compare) or\
            np.allclose(fitted, (compare - np.pi))
        # can have 180° shift due to small numbers

        self.assertTrue(test)

        phases = np.linspace(-np.pi, np.pi, 13)[:-1]
        phi = phases[:, np.newaxis]
        test_data = np.ones((12, 10)) + 1j * np.zeros((12, 10))
        test2 = np.allclose(np.angle(test_data * (np.cos(phi) + 1j*np.sin(phi))), phi)
        self.assertTrue(test2)

        # should be 0
        test3 = np.allclose(getPhiByGridSearch(test_data), np.zeros(12)) or\
            np.allclose(getPhiByGridSearch(test_data), np.ones(12) * np.pi)
        self.assertTrue(test3)

        # rotate test data in phases from 0 to 360° in 12 steps
        test_data = test_data * (np.cos(phi) + 1j * np.sin(phi))
        found = getPhiByGridSearch(test_data)

        test4 = np.zeros(12, dtype=bool)
        for ip, p in enumerate(found):
            test4[ip] = np.isclose(p, phases[ip]) or\
                np.isclose(p, phases[ip] + np.pi) or\
                np.isclose(p, phases[ip] - np.pi)

        self.assertTrue(np.all(test4))


class TestKernel(unittest.TestCase):

    def test_init(self):
        from comet import snmr
        site = snmr.survey.Survey()
        site.addLoop(ph.loop.buildCircle(10, 5))
        site.addLoop(ph.loop.buildSquare(10, 5))

        fid = snmr.survey.FID(tx=0, rx=1)
        site.addSounding(fid)
        kern1 = snmr.Kernel(survey=site, fid=0, dimension=1)

        # check basics defined in kernel
        self.assertTrue(not kern1.coincident)
        kern1._checkSrc()
        kern1._checkPulses()
        kern1._evalOuterDims()

        fid = snmr.survey.FID(tx=1, rx=1)
        site.addSounding(fid)
        kern2 = site.createKernel(1, dimension=2)

        # coincident, 2D
        self.assertTrue(kern2.coincident)
        kern2._checkSrc()
        kern2._checkPulses()
        kern2._evalOuterDims()

        # check loop reference
        self.assertTrue(kern1.rx is kern2.tx)

    def test_results(self):
        site = snmr.survey.Survey()
        a = ph.loop.loadLoop(basepath.joinpath('../test_data/test_loop'),
                             load_meshes=False)
        a.setLoopMesh(basepath.joinpath('../test_data/test_mesh.bms'))
        site.addLoop(a)
        site.createSounding(tx=0, rx=0)
        kern = site.createKernel(fid=0, dimension=1)
        kern.createZVector(10, -5, min_thk=0.2)
        kern.calculate()
        res1 = kern.getKernel()
        kern.calculate(num_cpu=4, slices=True)
        self.assertTrue(np.allclose(res1, kern.getKernel()))


class TestSNMRBase(unittest.TestCase):

    def test_init(self):
        from comet import snmr
        from comet.snmr.modelling.nmr_base import SNMRBase
        testfile = basepath.joinpath('../test_data/data.mrsd')
        self.site = snmr.survey.Survey()
        self.site.loadMRSD(testfile)

        self.nmr = SNMRBase(survey=self.site, dtype='rotatedAmplitudes')

        data, error = self.nmr.data, self.nmr.error
        gates = self.nmr.fid.gates

        self.assertTrue(data.shape == error.shape)
        self.assertTrue(self.nmr.fid.rotated is True)

        self.nmr.setDataType('amplitudes')
        data2, error2 = self.nmr.data, self.nmr.error
        gates2 = self.nmr.fid.gates

        self.assertTrue(np.allclose(gates, gates2))

        self.assertTrue(data.shape == data2.shape)
        self.assertTrue(error.shape == error2.shape)
        self.assertTrue(self.nmr.fid.rotated is False)

        self.nmr.setDataType('rotatedAmplitudes')

        data3, error3 = self.nmr.data, self.nmr.error

        self.assertTrue(np.allclose(data, data3))
        self.assertTrue(np.allclose(error, error3))

        self.nmr.setDataType('complex')
        self.assertTrue(len(data) * 2 == len(self.nmr.data))
        self.assertTrue(len(error) * 2 == len(self.nmr.error))


class TestEarth(unittest.TestCase):
    def test_init(self):
        e1 = snmr.Earth(incl=60, decl=2, mag=48000e-9)
        e2 = snmr.Earth(incl=60, decl=2, mag=48000e-9)
        e3 = snmr.Earth(incl=60.1, decl=2, mag=42000e-9)

        self.assertTrue(np.allclose(e1.field, e2.field))
        self.assertFalse(np.allclose(e1.field, e3.field))

        self.assertTrue(np.isclose(e1.larmor, e2.larmor))
        self.assertFalse(np.isclose(e1.larmor, e3.larmor))
        e1.magnitude = e3.magnitude
        self.assertFalse(np.isclose(e1.larmor, e2.larmor))
        self.assertTrue(np.isclose(e1.larmor, e3.larmor))

        self.assertTrue(
            np.allclose(snmr.Earth(incl=0, decl=0).field.T, (1, 0, 0)))
        self.assertTrue(
            np.allclose(snmr.Earth(incl=-90, decl=0).field.T, (0, 0, 1)))
        self.assertTrue(
            np.allclose(snmr.Earth(incl=0, decl=90).field.T, (0, 1, 0)))
        self.assertTrue(
            np.allclose(snmr.Earth(incl=90, decl=90).field.T, (0, 0, -1)))
        self.assertTrue(
            np.allclose(snmr.Earth(incl=45, decl=90).field.T,
                    (0, 1./np.sqrt(2), -1/np.sqrt(2))))
        self.assertTrue(
            np.allclose(snmr.Earth(incl=-30, decl=90).field.T,
                    (0, np.cos(np.pi/6), 0.5)))

if __name__ == '__main__':
#    with captured_output() as (out, err):
    # from comet.test import TestRotations, TestIntegration
    unittest.main()

#    suite = unittest.TestSuite()
#    suite.addTest(TestContainer("test_grid_search"))
#    runner = unittest.TextTestRunner()
#    runner.run(suite)

# The End
