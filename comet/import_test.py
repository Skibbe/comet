#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" @author: skibbe """
import os
import sys


def check_tetgen():
    print('[tetgen] ', end='', flush=True)
    os.system('which tetgen')


if __name__ == '__main__':
    print('######## Import check comet ########')

    print('Python {}'.format(sys.version))

    try:
        import pygimli as pg
        file_ = pg.__file__.split('envs')
        if len(file_) >= 2:
            file_ = file_[1]
        print('[found] [pygimli] ({}), ({})'.format(pg.__version__, file_))
    except ModuleNotFoundError:
        print('[not found] [pygimli]')

    try:
        import comet as c
        file_ = c.__file__.split('envs')
        if len(file_) >= 2:
            file_ = file_[1]
        print('[found] [comet] ({}), ({})'.format(c.__version__, file_))
    except ModuleNotFoundError:
        print('[not found] [comet]')

    try:
        import numpy as np
        file_ = np.__file__.split('envs')
        if len(file_) >= 2:
            file_ = file_[1]
        print('[found] [numpy] ({}), ({})'.format(np.__version__, file_))
    except ModuleNotFoundError:
        print('[not found] [numpy]')

    try:
        import matplotlib as mpl
        file_ = mpl.__file__.split('envs')
        if len(file_) >= 2:
            file_ = file_[1]
        print('[found] [matplotlib] ({}), ({})'.format(mpl.__version__, file_))
    except ModuleNotFoundError:
        print('[not found] [matplotlib]')

    check_tetgen()

    print('#'*40)
    print('Not necessary for COMET unless for ERT inversion:')

    try:
        import pybert as pb
        file_ = pb.__file__.split('envs')
        if len(file_) >= 2:
            file_ = file_[1]
        print('[found] [pybert] ({}), ({})'.format(pb.__version__, file_))
    except ModuleNotFoundError:
        print('[not found] [pybert]')

    print('#'*40)
    print('Not necessary for COMET unless for 2D magnetic fields:')

    try:
        import custEM as cm
        file_ = cm.__file__.split('envs')
        if len(file_) >= 2:
            file_ = file_[1]
        try:
            print('[found] [custEM] ({}), ({})'.format(cm.__version__, file_))
        except AttributeError:
            print('[found] [custEM] ({}), ({})'.format('v <= 0.9.05', file_))
    except ModuleNotFoundError:
        print('[not found] [custEM]')

# The End
