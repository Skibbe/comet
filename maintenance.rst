How to: 
=======

1. Build a conda package:
-------------------------

   1. test all relevant files
   2. change version string
   3. commit all changes, merge with master
   5. push
   5. create tag on homepage
   6. switch to conda build enironment
   7 `unset PYTHONPATH`
   9. check out https://github.com/gimli-org/conda-recipes
   10.`cd conda-recipes`
   11 `git pull`
   12. check comet/meta.yaml
   13. conda activate [anaconda build environment]
   14. unset PYTHONPATH
   15 `conda build -c gimli -c conda-forge comet`
   16. success?
      16a yes: 
        anaconda login
        anconda upload blabla.tar.bz2
      16b no: find bug, start at 1

2. sphinx documentation:
------------------------

   - commit/push all changes
   - create new tag on website, add release message

   1. Did you change the module names?
      1. Yes: build apidoc rst
      2. No: continue
   2.  Check version string in `comet/VERSION.rst`
   3. build doc on `https://readthedocs.org/projects/comet-project/`
      `latest` version is automatically build on master branch push
      `stable` version is build with version of last tag
