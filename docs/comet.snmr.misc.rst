comet\.snmr\.misc package
=========================

Submodules
----------

comet\.snmr\.misc\.IO\_pdf module
---------------------------------

.. automodule:: comet.snmr.misc.IO_pdf
    :members:
    :undoc-members:
    :show-inheritance:

comet\.snmr\.misc\.plot\_routines module
----------------------------------------

.. automodule:: comet.snmr.misc.plot_routines
    :members:
    :undoc-members:
    :show-inheritance:

comet\.snmr\.misc\.plotting\_tools module
-----------------------------------------

.. automodule:: comet.snmr.misc.plotting_tools
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: comet.snmr.misc
    :members:
    :undoc-members:
    :show-inheritance:
