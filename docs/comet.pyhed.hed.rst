comet\.pyhed\.hed package
=========================

Subpackages
-----------

.. toctree::

    comet.pyhed.hed.reference

Submodules
----------

comet\.pyhed\.hed\.hed\_bib module
----------------------------------

.. automodule:: comet.pyhed.hed.hed_bib
    :members:
    :undoc-members:
    :show-inheritance:

comet\.pyhed\.hed\.hed\_para module
-----------------------------------

.. automodule:: comet.pyhed.hed.hed_para
    :members:
    :undoc-members:
    :show-inheritance:

comet\.pyhed\.hed\.libHED module
--------------------------------

.. automodule:: comet.pyhed.hed.libHED
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: comet.pyhed.hed
    :members:
    :undoc-members:
    :show-inheritance:
