step 1: commit all changes

git commit --all -m ''

step 2: merge all to master branch

git checkout master
git merge branch

step 3: change version in comet/comet/version.rst

git tag

step 4: add new tag, push tag

git push --tags

step 5: build docu