comet\.snmr\.modelling package
==============================

Submodules
----------

comet\.snmr\.modelling\.errors module
-------------------------------------

.. automodule:: comet.snmr.modelling.errors
    :members:
    :undoc-members:
    :show-inheritance:

comet\.snmr\.modelling\.mrs module
----------------------------------

.. automodule:: comet.snmr.modelling.mrs
    :members:
    :undoc-members:
    :show-inheritance:

comet\.snmr\.modelling\.mrs\_survey module
------------------------------------------

.. automodule:: comet.snmr.modelling.mrs_survey
    :members:
    :undoc-members:
    :show-inheritance:

comet\.snmr\.modelling\.nmr\_base module
----------------------------------------

.. automodule:: comet.snmr.modelling.nmr_base
    :members:
    :undoc-members:
    :show-inheritance:

comet\.snmr\.modelling\.smooth\_syn module
------------------------------------------

.. automodule:: comet.snmr.modelling.smooth_syn
    :members:
    :undoc-members:
    :show-inheritance:

comet\.snmr\.modelling\.snmrModelling module
--------------------------------------------

.. automodule:: comet.snmr.modelling.snmrModelling
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: comet.snmr.modelling
    :members:
    :undoc-members:
    :show-inheritance:
