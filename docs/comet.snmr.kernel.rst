comet\.snmr\.kernel package
===========================

Submodules
----------

comet\.snmr\.kernel\.kernel\_bib module
---------------------------------------

.. automodule:: comet.snmr.kernel.kernel_bib
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: comet.snmr.kernel
    :members:
    :undoc-members:
    :show-inheritance:
