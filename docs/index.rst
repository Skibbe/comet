#################
Welcome to comet!
#################

Version: |version|

.. include:: ../README.rst

.. toctree::
   :hidden:
   :caption: Installation

   installation.rst

.. toctree::
   :hidden:
   :caption: Tutorials

   tutorials.rst

.. toctree::
   :hidden:
   :caption: API

   Main Classes <classes.rst>
   Modules <modules.rst>
   indices_and_tables

.. toctree::
   :hidden:
   :caption: About

   License <license.rst>
   Authors <authors.rst>

