Classes
=======

Loop
----

.. autoclass:: comet.pyhed.loop.Loop
   :members:

Survey
------

.. autoclass:: comet.snmr.survey.Survey
   :members:

FID
---

.. autoclass:: comet.snmr.survey.FID
   :members:

Kernel
------

.. autoclass:: comet.snmr.kernel.Kernel
   :members:
