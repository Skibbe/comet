comet\.pyhed\.hed\.reference package
====================================

Submodules
----------

comet\.pyhed\.hed\.reference\.dipole1d module
---------------------------------------------

.. automodule:: comet.pyhed.hed.reference.dipole1d
    :members:
    :undoc-members:
    :show-inheritance:

comet\.pyhed\.hed\.reference\.homogeneous\_fullspace module
-----------------------------------------------------------

.. automodule:: comet.pyhed.hed.reference.homogeneous_fullspace
    :members:
    :undoc-members:
    :show-inheritance:

comet\.pyhed\.hed\.reference\.homogeneous\_halfspace module
-----------------------------------------------------------

.. automodule:: comet.pyhed.hed.reference.homogeneous_halfspace
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: comet.pyhed.hed.reference
    :members:
    :undoc-members:
    :show-inheritance:
