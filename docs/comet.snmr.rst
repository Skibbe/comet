comet\.snmr package
===================

Subpackages
-----------

.. toctree::

    comet.snmr.framework
    comet.snmr.kernel
    comet.snmr.misc
    comet.snmr.modelling
    comet.snmr.survey

Module contents
---------------

.. automodule:: comet.snmr
    :members:
    :undoc-members:
    :show-inheritance:
