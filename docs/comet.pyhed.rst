comet\.pyhed package
====================

Subpackages
-----------

.. toctree::

    comet.pyhed.IO
    comet.pyhed.hed
    comet.pyhed.loop
    comet.pyhed.misc
    comet.pyhed.plot

Submodules
----------

comet\.pyhed\.config module
---------------------------

.. automodule:: comet.pyhed.config
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: comet.pyhed
    :members:
    :undoc-members:
    :show-inheritance:
