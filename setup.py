# -*- coding: utf-8 -*-
"""
Setup for conda package build.
"""
# COMET - COupled Magnetic resonance Electrical resistivity Tomography
# Copyright (C) 2017 - 2021 Skibbe N.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup
from setuptools import find_packages
readme = open('README.rst').read()
version = open('comet/VERSION.rst').read()

setup(
    name='comet',
    version=version,
    description='comet',
    long_description=readme,
    author="Skibbe, Nico",
    author_email="nico.skibbe@leibniz-liag.de",
    license="GPL",
    url="https://comet.readthedocs.io/",
    packages=[*find_packages(), 'comet/pyhed/apps'],
    include_package_data=True,
    package_data={'': ['*.py', '*.sh', 'VERSION.rst']}
    )