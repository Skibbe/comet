"""
This is the sixth and last part of the example 2.

As the last script, result can be found in the directory 'mrt_results'.

This script exports the model plot.

Changes needed if script 4 (inversion) was altered:

1. rotatedAmplitudes -> complex
If script 4 (inversion) was altered to do a complex inversion, than you can
modify this script to show the misfits for both real and imaginary part, rather
than the absolute misfit. The "to_plot" parameter for the plotting function
can be changed by exchanging

    import_filename = 'rotatedAmplitudes_lam...'

with

    import_filename = 'complex_lam...'


2. lam=250 -> lam=x
The result file name has to be adjusted. If you have done the inversion with a
lambda of "x" the new "import_filename" has to reflect that. Simply exchange
the 250 with your lambda value x.

"""
# =============================================================================
# Import
import numpy as np
import pygimli as pg

from comet import snmr
from comet import pyhed as ph

# general plotting library
import matplotlib.pyplot as plt
# logger as always
ph.log.setLevel(20)
ph.addLogFile()

# =============================================================================
# User input
# =============================================================================
import_filename = 'rotatedAmplitudes_lam250'

# example filename for a complex inversion using a lambda of 125
# import_filename = 'complex_lam250'

# =============================================================================
# Plot
# =============================================================================
# load mrt results
mrt = snmr.modelling.MRT()
model, misfit, response, chi2 = mrt.loadResults(
    f'example2/mrt_results/{import_filename}.npz')
# split water content and relaxation times for plotting purpose
wc, t2 = np.split(model, 2)
# load plotting mesh
invmesh = pg.Mesh('invmesh.bms')
label1 = 'inversion, water content (1)'
label2 = 'inversion, relaxation times (s)'

# plot
with ph.misc.plt_ioff():
    # plot figure with two subfigures
    fig, ax = plt.subplots(2, 1, figsize=[16, 9])
    # show water content
    # define some colorbar limits, map, omit log scale (0 to 50 % water)
    pg.show(invmesh, wc, colorBar=True, ax=ax[0], label=label1,
            cMin=0, cMax=0.5, cMap='viridis_r', hold=True,
            logScale=False)
    # show relaxation times in second axis of plot (0.04 to 0.4 seconds)
    pg.show(invmesh, t2, colorBar=True, ax=ax[1], label=label2,
            cMin=0.04, cMax=0.4, cMap='inferno_r', hold=True)
    # cut edges to focus view, like in the paper
    # you can comment the next three lines to take a look at the whole
    # inversion domain, if you like.
    for axi in ax.flatten():
        axi.set_xlim([-100, 100])
        axi.set_ylim([-60, 0])
    # save figure
    fig_name = f'example2/mrt_results/{import_filename}_model.pdf'
    fig_name = f'example2/mrt_results/{import_filename}_model.png'
    fig.savefig(fig_name,
                bbox_inches='tight',
                facecolor='none', edgecolor='none', transparent=True)

ph.log.info('Example 2 part 6 run finished.')
ph.log.info(f'Result was saved as "{fig_name}".')
