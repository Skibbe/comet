'''
This is the first part of the example 2.
Version 2 without custEM, using up to 1D resistivities.

Note that the mesh for the magnetic fields is a little different from the mesh
used in the manuscript (custEM is needed for the manuscript version, this
however is some fallback we use for windows inversions).
'''
# =============================================================================
# import main parts of COMET
from comet import pyhed as ph
from comet import snmr
# also importing pathlib to create output directories
from pathlib import Path

# set logger to give us some feedback during computation
ph.log.setLevel(20)

# Add logging file which catches output and executed code, so later you
# know all the parameters you setted when you change things. I found this
# to be extremely useful, you can uncomment it, however the file is not
# big and invaluable if something is not working.
ph.addLogFile()

# make output dir
Path('example2').mkdir(exist_ok=True)

# =========================================================================
# configuration
# =========================================================================
# earth magnetic field: Bfield vector and frequency
earth = snmr.survey.Earth(decl=0, incl=60, mag=48000e-9)
# loop configuration: 1D earth + frequency
loop_config = ph.config(rho=[1000, 100, 20, 300], d=[2, 13, 7],
                        f=earth.larmor)

# we want to gather everything in this bare survey class
# later we can simply import the whole thing
site = snmr.survey.Survey()
# survey containing the simulated data
site.load('test_data/Example2Prep/synthetic_data/data')

# let's make sure the config is known by the survey class
site.setLoopConfig(loop_config)
# same with the magnetic parameters
site.setEarth(earth)

loops = ph.loop.buildEtraSurvey(50, num_loops=8, max_length=0.51,
                                config=loop_config)

for lp, loop in enumerate(loops):
    # add loop to survey class
    site.addLoop(loop)

site.save('example2/survey/survey_withloops')
# save datacontainer with synthetic data, and geometry of the loops
# (no field calculation yet, this will be done later, first we need a mesh)

# =========================================================================
# create magnetic field mesh
# =========================================================================
# create mesh once and then distribute later
mesh = ph.loop.createMultipleLoopMesh(
    site.loops,
    savename='example2/loopmesh.bms',
    source_max_area=5,
    minx=-125, maxx=125,
    miny=-50, maxy=50,
    minz=-50, mid_area_volume=25)

mesh.exportVTK('example2/loopmesh.vtk')
ph.log.info('Example 2 part 1 run finished.')
