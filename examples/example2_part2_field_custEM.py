# -*- coding: utf-8 -*-
"""
This script can be called using mpirun to calculate the magnetic fields using
custEM.
"""

from comet import pyhed as ph

ph.loop.totalFieldCalculation('./results/custEMConfig2.cfg', num_cpu=16)

# The End
