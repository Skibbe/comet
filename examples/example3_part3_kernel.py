'''
This is the third part of the example 2.
From this point forward custEM is no longer reqired.
No matter if custEM was used for thefirst to parts or not, the nexts parts
remain the same.

This script calculates the kernel functions based on the previously calculated
magnetic fields (found in the loop instances saved along with the survey class
under "surveys/survey_ready").

This script does not need a lot of memory but takes a while to execute if only
a few cores are used. The number of used cores is defined below.

If it still is not fast enough, you could reduce the order given when setting
the 2DKernelMesh in "kern.set2DKernelMesh(... order=2)" to 1.
However, this reduces the kernel accuracy somewhat.
'''
# =============================================================================
# import main parts of COMET
from comet import pyhed as ph
from comet import snmr

if __name__ == '__main__':
    __spec__ = None
    # =========================================================================
    # User input
    # =========================================================================
    # feel free to change the number
    number_of_used_cores = 48

    # =========================================================================
    # calculation
    # =========================================================================
    # log level 20 == Info level, some infos of what is happening are given
    ph.log.setLevel(20)
    ph.addLogFile()
    # import prepared survey from second script
    site = snmr.survey.Survey()
    site.load('example3/survey/survey_ready', load_meshes=False)
    # load mesh for all loops (actually once and distribute, to save time)
    ph.log.info('loading mesh... this could take a minute')
    site.loadLoopMesh('example3/_h5/fem_mesh.bms')

    # do for all FID's = measurements or NMR experiments
    for i in range(len(site.fids)):
        # generate kernel class for a 2D kernel for fid index i
        kern = site.createKernel(fid=i, dimension=2)
        # define kernel base and order of internal refinement
        kern.set2DKernelMesh('invmesh.bms', order=1)
        # calculate kernel in 2D slices in parallel
        kern.calculate(num_cpu=number_of_used_cores)
        # save result for import when inversion is done
        kern.save('example3/kernel/kern_{}'.format(i),
                  kernelmesh_name='invmesh.bms')
        # save kernel under kern_0, kern1,... kern_7 in directory "surveys"
        # omit saving kernel mesh with default name as all meshs are the same
        # (simply saves disk space)

    ph.log.info('Example 3 part 3 run finished.')
