'''
This is the first and second part of the example 2.
Version 2 without custEM, using 1D resistivities.

Note that the mesh for the magnetic fields is a little different from the mesh
used in the manuscript (custEM is needed for the manuscript version, this
however is some fallback we use for windows inversions).

Limited to a simple computer thsi can take a while. Maybe limit the number of
used cpu cores (num_cpu line 58) to half the cores you have to maintain
some computational power for other stuff.
'''
# =============================================================================
# import main parts of COMET
from comet import pyhed as ph
from comet import snmr
import pygimli as pg

if __name__ == '__main__':
    __spec__ = None
    # set logger to give us some feedback during computation
    ph.log.setLevel(20)
    ph.addLogFile()

    # =========================================================================
    # User input
    # =========================================================================
    # feel free to change the number
    number_of_used_cores = 48

    # =========================================================================
    # import stuff from first script
    # =========================================================================
    site = snmr.survey.Survey()
    # load survey with data and raw loops: we will set the mesh later
    site.load('example2/survey/survey_withloops', load_meshes=False)

    # import mesh from first script in binary pygimli format
    # again for visualization see .vtk file using paraview
    ph.log.info('import "example2/loopmesh.bms"...')
    mesh = pg.Mesh('example2/loopmesh.bms')

    # =========================================================================
    # calculate magnetic fields
    # =========================================================================
    # loop over all loops to be calculated
    for li, loop in enumerate(site.loops):
        # set 1D earth, frequency, mesh
        loop.setLoopMesh(mesh, savename='example2/loopmesh.bms')

        # This can take a while, its a fine mesh, but otherwise the kernel is
        # not suited for inversion. In linux there is a progress bar, but not
        # on windows.
        loop.calculate(num_cpu=number_of_used_cores)

        # changing the loop name
        # not needed, just to ensure the results are not deleted by calling
        # script 1 again.
        loop.name = 'example2/survey/loop_ready_{}'.format(li)

    # save calcualted fields and mesh again to reuse it if the following
    # scripts are perhaps played around with (to save the heavy computational
    # stuff).
    site.save('example2/survey/survey_ready')  # saves loops (and fields) as well
    ph.log.info('Example 2 part 2 run finished.')
