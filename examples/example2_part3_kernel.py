'''
This is the third part of the example 2.
From this point forward custEM is no longer reqired.
No matter if custEM was used for thefirst to parts or not, the nexts parts
remain the same.

This script calculates the kernel functions based on the previously calculated
magnetic fields (found in the loop instances saved along with the survey class
under "surveys/survey_ready").

This script does not need a lot of memory but takes a while to execute if only
a few cores are used. The number of used cores is defined below.

If it still is not fast enough, you could reduce the order given when setting
the 2DKernelMesh in "kern.set2DKernelMesh(... order=2)" to 1.
However, this reduces the kernel accuracy somewhat.
'''
# =============================================================================
# import main parts of COMET
from comet import pyhed as ph
from comet import snmr
import matplotlib.pyplot as plt


if __name__ == '__main__':
    __spec__ = None
    # =========================================================================
    # User input
    # =========================================================================
    # feel free to change the number
    number_of_used_cores = 48
    survey_name = 'example2/survey/survey_ready'
    invmesh = 'invmesh.bms'
    cfgname = './results/custEMConfig2.cfg'

    # y discretization
    # max distance between slices inside the loops
    max_length = 0.05
    # number of slices
    max_num = 1200

    # =========================================================================
    # calculation
    # =========================================================================
    # log level 20 == Info level, some infos of what is happening are given
    ph.log.setLevel(20)
    ph.addLogFile()

    kernelname = snmr.kernel.calculateKernelFromSlices(
        survey_name,
        invmesh,
        cfgname,
        max_length=max_length,
        max_num=max_num,
        path_name='kernel',
        num_cpu=number_of_used_cores,
        h_order=1,
        json_name='results/kernelConfig.json',
        force_new_paths=False,
        slice_export_name='kernel_slice',
        kernel_name='kernel/kernp2_{}')

    # let's for exmaple show the imaginary part of the third kernel
    kern = snmr.Kernel(name=kernelname.format(2))
    kern.show(toplot=['imag'])
    plt.show()

    ph.log.info('Example 2 part 3 run finished.')
