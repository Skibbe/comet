"""
This is the fifth part of the example 2.

The last two scripts will simply export the inversion results in the directory
'mrt_results'.

This script exports the data, and misfit plots.

Additional information:

Changes needed if script 4 (inversion) was altered:

1. rotatedAmplitudes -> complex
If script 4 (inversion) was altered to do a complex inversion, than you can
modify this script to show the misfits for both real and imaginary part, rather
than the absolute misfit. The "to_plot" parameter for the plotting function
can be changed by exchanging

    to_be_plotted = ['abs']

with this line:

    to_be_plotted = ['real', 'imag']

and

    import_filename = 'rotatedAmplitudes_lam...'

with

    import_filename = 'complex_lam...'

2. lam=250 -> lam=x
The result file name has to be adjusted. If you have done the inversion with a
lambda of "x" the new "import_filename" has to reflect that. Simply exchange
the 250 with your lambda value x.

"""
# =============================================================================
# Import
from comet import snmr
from comet import pyhed as ph
# logger as always
ph.log.setLevel(20)
ph.addLogFile()

# =============================================================================
# User input
# =============================================================================
import_filename = 'rotatedAmplitudes_lam250'
to_be_plotted = ['abs']

# example filename for a complex inversion using a lambda of 125
# import_filename = 'complex_lam250'

# and the changes to the plotting routine
# to_be_plotted = ['real', 'imag']

# =============================================================================
# Plot
# =============================================================================
# load site with data
site = snmr.Survey()
# site.load('example2/survey/survey_ready', load_meshes=False)
site.load('test_data/Example2Prep/synthetic_data/data', load_meshes=False)
# initialize mrt class
mrt = snmr.modelling.MRT(survey=site)
# load mrt results
model, misfit, forward, chi2 =\
    mrt.loadResults(f'example2/mrt_results/{import_filename}')
# some console output
ph.log.info(f'reached chi² of {chi2:.3f}')
# set response vector for misfit calculation
site.setResponse(forward)
# make some plots (in the "mrt_results" directory)
for to_plot in to_be_plotted:
    for draw in ['data', 'misfit', 'response']:
        ph.plot.showEtraData(
            site, to_plot=to_plot, draw=draw, perc=0.99,
            praefix=import_filename,
            rdir='example2/mrt_results', png=True)

ph.log.info('Example 2 part 5 run finished.')
