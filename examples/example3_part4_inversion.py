"""
This is the fourth part of the example 2.

Here the final inversion is done using the previously calculated kernels.
The default for the inversion is a rotated amplitude inversion similar to that
shown in the manuscript.

If you cannot see the inversion process in the console window and are working
on spyder, you can run this file in a dedicated console
(Run -> Configuration per file select "execute in an external system terminal")
Reason is that some cpp messages are not piped correctly in the spyder ipython
console (comet cannot fix this). This, however, does not change any results.

For your information:

A complex inversion could also be done by changing the
dtype from "rotatedAmplitudes" to "complex" like so:

data_type = 'complex'

If you like to try this, mind the changes in the plotting routines of script 5.

Of course also lambda and z_weight values could be changed.
"""
# =============================================================================
# Import
from comet import pyhed as ph
from comet import snmr
import pygimli as pg
# set log level for console messages
ph.log.setLevel(20)
ph.addLogFile()

# =============================================================================
# User input
# =============================================================================
# data_type = 'rotatedAmplitudes'
# alternatively:
data_type = 'complex'

# define some inversion parameters
# general smoothness constraint
lam = 150

# weight of lam in vertical direction
z_weight = 0.1
# =============================================================================
# Inversion
# =============================================================================
# load data from survey, no further need for loop meshes
site = snmr.survey.Survey()
site.load('example3/survey/survey_ready', load_meshes=False)
# load inversion mesh
invmesh = pg.Mesh('invmesh.bms')
# create MRT manager instance for inversion. Input:
# survey = survey containing the synthetic data
# dim = dimension = 2 == 2D inversion
# dtype = (inversion) data type == rotated Amplitudes
mrt = snmr.modelling.MRT(
    survey=site, dim=2, dtype=data_type)
# We invert 8 soundings, each gets a kernel
for sd, sound in enumerate(mrt.sounds):
    sound.setKernel(f'example3/kernel/kern_{sd}', load_loopmesh=False)

# create forward operator
mrt.createFOP()
# set inversion parameters and create inversion instance
mrt.setZWeight(z_weight)
mrt.createINV(lam=lam)
# define (default) starting model and run inversion
mrt.inv.setModel(mrt.fop.createStartModel())
mrt.inv.run()
# save MRT results
mrt.saveResults('example3/mrt_results/{}_lam{}'
                .format(mrt.dtype, lam))
# print inversion output in case messages are not piped to console
mrt.inv.echoStatus()
ph.log.info('Example 3 part 4 run finished.')
