from comet import pyhed as ph
from comet import snmr
import matplotlib.pyplot as plt

if __name__ == '__main__':  # <- this line prevents crash of multiprocessing
    ph.log.setLevel(20)  # info level for logger = console output
    # %% part 1: definition of synthetic model (3-layer earth)
    syn_thk = [10, 20]              # thickness [m] last layer infinite
    syn_res = [1000., 200., 2000.]  # resistivity      [Ohm*m]
    syn_wc = [0.15, 0.25, 0.1]      # water content    [1]
    syn_t2 = [0.2, 0.3, 0.1]        # relaxation times [s]
    # %% part 2: loop configuration and resistivity
    cfg = ph.config(rho=syn_res, d=syn_thk)
    # 40*40 m square loop around origin [0, 0], max dipole distance 2m
    loop = ph.loop.buildRectangle([[-20, -20], [20, 20]],
                                  max_length=2, config=cfg)
    
    # loop.loopmesh.save('ex1_mesh.bms')
    # %% part 3: survey with earth definition and sounding
    site = snmr.survey.Survey()
    site.addLoop(loop)  # only one loop
    site.setEarth(mag=48e-6, incl=60, decl=0)  # magnetic field strenght + dir
    site.createSounding()  # by default tx=0, rx=0 coincident loop
    # %% part 4: setup 1D manager for inversion and simulation
    mrs = snmr.modelling.MRS(
        survey=site,            # config
        fid=0,                  # which FID to invert
        mtype='block',          # block inversion (or 'smooth')
        dtype='complex',        # complex inversion (or 'rotatedAmplitudes')
        )
    # mrs.kernel.load('kernel.npz')  # comment in to speed computation
    # simulation of mrs data for given block model
    mrs.simulate([syn_thk, syn_wc, syn_t2], err=20e-9, num_gates=50,
                 num_cpu=32)  # used wherever applicable
    mrs.kernel.save('kernel')  # save kernel for later loading 4 lines above
    # %% part 5: inversion with a fixed regularization
    mrs.invert(lam=100)
    print('final chi square:', mrs.inv.getChi2())
    ax = mrs.showDataAndFit()
    ax2 = mrs.showResult(syn=[syn_thk, syn_wc, syn_t2])  # comparison to syn model
    plt.show()