#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

"""
from comet import pyhed as ph
# from comet import snmr
# import numpy as np
# import pygimli as pg

if __name__ == '__main__':
    ph.log.setLevel(30)
    ph.addLogFile()

    # 1
    import matplotlib.pyplot as plt
    plt.style.use("seaborn-notebook")

    # 2
    import pygimli as pg
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.colors as clr
    import empymod

    # 3
    # receiver setting
    rx = np.arange(50., 501., 50)
    ry = np.zeros(rx.size)
    # empymod arguments including transmitter
    txLen = 400
    inpdat = {'src': [0, 0, txLen/2, -txLen/2., 0.1, 0.1], 'strength': 1,  # ground
               'mrec': True, 'rec': [rx, ry, -20, 0, 90],  # Hz air
               # 'mrec': False, 'rec': [rx, ry, 0.1, 90, 0],  # Ey ground
              'srcpts': 11, 'htarg': {'pts_per_dec': -1}, 'verb': 1}
    # frequencies
    freqs = [10., 20, 50, 100, 200, 500, 1000, 2000., 5000.]  # Hz

    # 4
    def fwd(res, dep, inpdat):
        """Call empymods function bipole with the above arguments."""
        assert len(res) == len(dep)
        OUT = np.zeros((len(freqs), len(rx)), dtype=np.complex)
        for i, f in enumerate(freqs):
            OUT[i, :] = empymod.bipole(res=np.concatenate(([2e14], res)),
                                       depth=dep, freqtime=f, **inpdat)

        return OUT

    def pyhed_init(inpdat):
        src = ph.loop.buildLine((inpdat['src'][0], inpdat['src'][2]),
                                (inpdat['src'][1], inpdat['src'][3]),
                                num_segs=inpdat['srcpts'])
        rec_pos = np.zeros((3, len(inpdat['rec'][0])))
        rec_pos[0, :] = inpdat['rec'][0]
        rec_pos[1, :] = inpdat['rec'][1]
        rec_pos[2, :] = inpdat['rec'][2]
        src.setLoopMesh(rec_pos)
        src.config.ftype = 'H'
        return src

    def fwd2(src, res, dep):
        """Call empymods function bipole with the above arguments."""
        assert len(res) == len(dep)
        thk = np.diff(dep)
        OUT = np.zeros((len(freqs), len(rx)), dtype=np.complex)
        src.setModel(res, d=thk)
        for i, f in enumerate(freqs):
            src.setFrequency(f)
            src.calculate(num_cpu=1)
            OUT[i, :] = src.field[2]  # z-comp

        return OUT

    # 5
    def showData(A, ax=None, sym=True):
        """Show data as two subplots for real and imaginary part."""
        if ax is None:
            fig, ax = plt.subplots(1, 2)

        if A.ndim == 1: # long vector
            reim = np.reshape(A, (2, -1))
            A = np.reshape(reim[0], (len(freqs), -1)) + \
                np.reshape(reim[1], (len(freqs), -1)) * 1j

        norm = clr.NoNorm()
        cmap = "Spectral_r"
        if sym:
            mi = np.min(np.abs(A))
            ma = np.max(np.abs(A))
            norm = clr.SymLogNorm(mi, vmin=-ma, vmax=ma, base=10)#, linscale=mi)
            cmap = 'RdBu_r'

        imr = ax[0].imshow(A.real, norm=norm, cmap=cmap)
        plt.colorbar(imr, ax=ax[0], orientation="horizontal")
        imi = ax[1].imshow(A.imag, norm=norm, cmap=cmap)
        plt.colorbar(imi, ax=ax[1], orientation="horizontal")
        if A.shape[1] == len(rx):
            for a in ax:
                a.xaxis.set_ticks(range(len(rx))[::3])
                a.xaxis.set_ticklabels([str(int(r)) for r in rx][::3])
                a.set_xlabel('x (m)')
                a.yaxis.set_ticks([y for y in range(len(freqs))])
                a.yaxis.set_ticklabels([str(int(f)) for f in freqs])
                a.set_ylabel('f (Hz)')

        ax[0].xaxis.set_ticks([])
        ax[0].set_xlabel('')
        ax[0].set_title("real part")
        ax[1].set_title("imaginary part")
        plt.tight_layout()
        return ax

    # 6
    depth = [0, 100, 200]  # m
    res = [100, 10, 100]
    A0 = fwd(res, depth, inpdat)
    showData(A0);

    src = pyhed_init(inpdat)
    A1 = fwd2(src, res, depth)
    showData(A1);

    # 7
    class myFwd(pg.Modelling):
        def __init__(self, depth):
            """Initialize the model."""
            self.dep = depth
            self.mesh1d = pg.meshtools.createMesh1D(len(self.dep))
            super().__init__()
            self.setMesh(self.mesh1d)

        def init_src(self, inpdat):
            self.src = pyhed_init(inpdat)

        def response(self, model):
            """Forward response."""
            A = fwd2(self.src, model, self.dep)
            Avec = A.ravel()
            return np.hstack((Avec.real, Avec.imag))

        def createStartModel(self):
            return pg.Vector(len(self.dep)) * 100

    # 8
    A0vec = A0.ravel()
    data = np.hstack((A0vec.real, A0vec.imag))
    # error = np.abs(data) * 0.01 + 0.00001
    relativeError = np.ones_like(data) * 0.01
    data *= (np.random.randn(len(data)) * relativeError + 1.0)

    # 9
    depth_fixed = np.linspace(0., 300., 21)
    fop = myFwd(depth_fixed)
    fop.init_src(inpdat)

    resistivity = np.ones_like(depth_fixed) * 100
    response = fop.response(resistivity)
    showData(response);

    # 10
    inv = pg.Inversion()
    inv.setForwardOperator(fop)
    transModel = pg.trans.TransLog(1) # > 1 Ohmm
    inv.transModel = transModel

    # 11
    model = inv.run(data, relativeError, startModel=100, verbose=True)

    # 12
    from pygimli.viewer.mpl import drawModel1D
    fig, ax = plt.subplots()
    drawModel1D(ax, np.diff(depth), res, color="red", label="synthetic")
    drawModel1D(ax, np.diff(depth_fixed), model, color="blue", label="inverted")

    # 13
    showData(response);

    #14
    showData(inv.response-data);

# The End