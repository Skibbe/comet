'''
This is the first part of the example 2 using the custEM library.

In this script a suited finite element mesh is created which includes the
source geometry of the 8 used loops as well as the parameter mesh markers
for the subsequent transfer of resistivity values.
'''
# =============================================================================
# Import main parts of COMET
from comet import pyhed as ph
from comet import snmr
# import numpy and pygimli libraries
import numpy as np
import pygimli as pg
# set logger to give us some feedback during computation
# 20 == info level
ph.log.setLevel(20)
ph.addLogFile()

# Add logging file which catches output and executed code, so later you
# know all the parameters you setted when you change things. I found this
# to be extremely useful, you can uncomment it, however the file is not
# big and invaluable if something is not working.
ph.addLogFile()
# =============================================================================
# loop configuration: 1D earth + frequency
# =============================================================================
# earth magnetic field: Bfield vector and frequency
earth = snmr.survey.Earth(decl=0, incl=60, mag=48000e-9)
# Now we import the 2D parameter mesh from the ERT inversion.
# This is where the resistivity lives on.
# In order to transfer resistivities to the FEM mesh, we need the geometry.
paramesh2d = pg.Mesh('test_data/Example2Prep/ert/invmesh.bms')
# import associated resistivity vector
res = np.load('test_data/Example2Prep/ert/anom_res.npy')
# etra survey, small edge length = 50 meter, using 8 loops in total
# maximum length of a dipole is 0.51 m, you can try other values
loops = ph.loop.buildEtraSurvey(50, num_loops=8, max_length=0.51)
# =============================================================================
# survey
# =============================================================================
# we want to gather everything in this bare survey class
# later we can simply import the whole thing
site = snmr.survey.Survey()
# survey containing the simulated data (same as for exmaple 3)
site.load('test_data/Example2Prep/synthetic_data/data')

# In this case the primary background field of the unsaturated zone
# (we use the resistivity close to the loop wires as background) is a
# halfspace with 1000 Ohmm using the larmor frequency
loop_config = ph.config(rho=[1000.], f=earth.larmor)
# let's make sure every loops knows this
site.setLoopConfig(loop_config)

# looping over the loops
for lp, loop in enumerate(loops):
    # add loop to survey class, generate index and so on for later
    # also our config is passed on to the loops
    site.addLoop(loop)

# lets set earth (like config for loops, just for NMR later on)
site.setEarth(earth)

# let's check some console output, for example the final config
ph.log.info(loops[0].config)
# we use a different name to ensure the original files remain intact for reruns
site.save('example3/survey/survey_withloops')
# this actually creates several files:
# main survey object: "surveys/survey_withloops.npz"
# + one fid object with id: "surveys/survey_withloops_fid_0...7.npz"
# + one loop object with id: "surveys/survey_withloops_loop_0...7.npz"
# =============================================================================
# create FEM mesh
# =============================================================================
# temporarily create merged loop-class for the purpose of mesh building
# simple merge in order to incorporate measurement geometry of all loops
# true_merge = False keeps the single dipoles intact, otherwise close dipoles
# would be merged
mesh_loop = ph.loop.mergeLoops(loops, true_merge=False)
# create mesh directory under /example3, this also generates a configuration
# subclass for storing information concerning the secondary field calculation.
# This is normally done internally by default, however as we like to change
# the output directories, we need to call it here.
mesh_loop.createDefaultSecondaryConfig(m_dir='example3')
# set 2D mesh and geometry
mesh_loop.setParaMesh2D(paramesh2d, append_boundary=True,
                        anomaly=1./res)  # need conductivity here
# finally create FEM mesh and export additional VTK for quality control
# ! this is the step that needs custEM the first time !
mesh_loop.createFEMMesh(savename='fem_mesh',
                        source_setup='etra',
                        number_of_loops=8,
                        exportH5=True,
                        box_x=[-125, 125],
                        box_y=[-125, 125],
                        box_z=-50,
                        inner_area_cell_size=2,
                        subsurface_cell_size=35.,
                        exportVTK=True,
                        )

# Note: All mesh are saved in directory '_h5', so custEM can find them.
# So in this case a file like "example3/_h5/fem_mesh.h5"
# The directory also contains several .vtk files for debugging purposes.
# They can be opened using the ParaView software.

# =============================================================================
# some output from this script
# =============================================================================
# plot the loop layout
ax = ph.plot.showLoopLayout(loops)
ax.figure.savefig('loop_layout.png', dpi=300, bbox_inches='tight',
                  transparent=True)

# furthermore you can use paraview to open the '_h5/fem_mesh.vtk'
# using a y-normal slice you can select _attribute to see the ERT resistivity
# values distributed in the 3D
ph.log.info('Example 3 part 1 run finished.')
