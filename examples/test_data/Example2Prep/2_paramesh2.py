# -*- coding: utf-8 -*-
"""
filling 2D parameter mesh with anomaly values + export for various cases
"""

import numpy as np
import pygimli as pg
import matplotlib.pyplot as plt
from pathlib import Path
from comet import pyhed as ph

ph.log.setLevel(13)
ph.addLogFile()

# %% para valley
inpath = Path('geometry')

raw = pg.load(inpath.joinpath('raw_paramesh_2d.bms').as_posix())

back_res = 1000
back_t2 = 0.1  # ?
back_wc = 0.05  # ?

# top, clay, lower_grad, subsurface
map_thk = np.array([2, 13, 7])
map_wc = np.array([back_wc, 0.4, 0.25, 0.35])
map_t2 = np.array([back_t2, 0.005, 0.1, 0.25])
map_res = np.array([back_res, 20, 300, 100])

marker = np.array(raw.cellMarkers()) - 1  # marker 1...4 -> 0...3 for indexing

anom_wc = map_wc[marker]
anom_t2 = map_t2[marker]
anom_res = map_res[marker]

for out in ['block', 'layered']:

    layered = out == 'layered'  # just a flag
    path = Path(out)

    # %% block, do not block comment, except for path
    if layered:

        # 1d layered earth
        layered = True
        path = Path('layered')
        path.mkdir(exist_ok=True)

        depths = -np.cumsum(map_thk)

        for cell in raw.cells():
            yy = cell.center().y()
            if yy >= depths[0]:
                anom_res[cell.id()] = map_res[0]
                cell.setMarker(1)

            elif yy < depths[0] and yy >= depths[1]:
                anom_res[cell.id()] = map_res[3]
                cell.setMarker(4)

            elif yy < depths[1] and yy >= depths[2]:
                anom_res[cell.id()] = map_res[1]
                cell.setMarker(2)

            else:
                anom_res[cell.id()] = map_res[2]
                cell.setMarker(3)

        # for export only background resistivity is important, as no sec field
        # will be calculated anyway
        lay_anom = 0.0
        lay_size = 0.0

        clay_cells = raw.findCellByMarker(1)
        for cell in clay_cells:
            lay_anom += map_res[1] * cell.size()
            lay_size += cell.size()

        anom_cells = raw.findCellByMarker(4)
        for cell in anom_cells:
            if cell.center().y() <= -15 and cell.center().y() >= -22:
                lay_anom += anom_res[cell.id()] * cell.size()
                lay_size += cell.size()

        print(lay_anom, lay_size, lay_anom/lay_size)
        lay_anom = lay_anom/lay_size

    # %% export
    np.save(path.joinpath('anom_wc.npy').as_posix(), anom_wc)
    np.save(path.joinpath('anom_t2.npy').as_posix(), anom_t2)
    np.save(path.joinpath('map_res.npy').as_posix(), map_res)
    np.save(path.joinpath('map_thk.npy').as_posix(), map_thk)
    np.save(path.joinpath('background.npy').as_posix(),
            np.array([back_res, back_wc, back_t2]))
    np.save(path.joinpath('anom_res.npy').as_posix(), anom_res)

    raw.save(path.joinpath('paramesh_2d.bms').as_posix())

    # %% plot
    ax, cb = pg.show(raw, raw.cellMarkers(), colorBar=True, cMap='Spectral_r',
                     logScale=False)
    cb.set_label('Cell Marker')
    ax.figure.savefig(path.joinpath('paramesh2D_marker.pdf').as_posix(),
                      bbox_inches='tight',
                      facecolor='none', edgecolor='none')
    ax.figure.savefig(path.joinpath('paramesh2D_marker.png').as_posix(),
                      bbox_inches='tight',
                      facecolor='none', edgecolor='none', dpi=600)
    plt.close(ax.figure)

    ax, cb = pg.show(raw, anom_wc, colorBar=True, cMap='viridis_r',
                     logScale=False, cMin=0.0, cMax=0.5)
    ax.set_xlim([-150, 150])
    ax.set_ylim([-80, 0])
    cb.set_label('Water Content (1)')
    ax.figure.savefig(path.joinpath('paramesh2D_wc.pdf').as_posix(),
                      bbox_inches='tight',
                      facecolor='none', edgecolor='none')
    ax.figure.savefig(path.joinpath('paramesh2D_wc.png').as_posix(),
                      bbox_inches='tight',
                      facecolor='none', edgecolor='none', dpi=600)
    plt.close(ax.figure)

    ax, cb = pg.show(raw, anom_t2, colorBar=True, cMap='inferno_r', cMin=0.04,
                     cMax=0.4)
    ax.set_xlim([-150, 150])
    ax.set_ylim([-80, 0])
    cb.set_label('Relaxation Times (s)')
    ax.figure.savefig(path.joinpath('paramesh2D_t2.pdf').as_posix(),
                      bbox_inches='tight',
                      facecolor='none', edgecolor='none')
    ax.figure.savefig(path.joinpath('paramesh2D_t2.png').as_posix(),
                      bbox_inches='tight',
                      facecolor='none', edgecolor='none', dpi=600)
    plt.close(ax.figure)

    ax, cb = pg.show(raw, anom_res, colorBar=True, cMap='viridis', cMin=20,
                     cMax=1000)
    ax.set_xlim([-150, 150])
    ax.set_ylim([-80, 0])
    cb.set_label('Resistivity ($\Omega$m)')
    ax.figure.savefig(path.joinpath('paramesh2D_res.pdf').as_posix(),
                      bbox_inches='tight',
                      facecolor='none', edgecolor='none')
    ax.figure.savefig(path.joinpath('paramesh2D_res.png').as_posix(),
                      bbox_inches='tight',
                      facecolor='none', edgecolor='none', dpi=600)
    plt.close(ax.figure)

# The End

# Don't mind this block please.
# It has been used to give the valley a
# gradient in the middle. This however is no loner part of the tutorial,
# however I might revieve it someday, so I like to leave it here.

# smooth, gradient

# def gradAnom(x, grad_anom):
#     return grad_anom[0] + x * (grad_anom[1] - grad_anom[0])

# path = Path('smooth')

# # seriously this will lead nowhere.. you might as well stop reading.
# path.mkdir(exist_ok=True)

# gradient = [-30, -5]
# grad_res = [map_res[2], back_res]
# grad_t2 = [map_t2[2], back_t2]
# grad_wc = [map_wc[2], back_wc]

# anom_cells = raw.findCellByMarker(4)
# mark = 4
# allanom = 0.0
# allsize = 0.0
# for cell in anom_cells:
#     rel_pos = (cell.center().y() - gradient[0]) / np.diff(gradient)[0]
#     anom_wc[cell.id()] = gradAnom(rel_pos, grad_wc)
#     anom_t2[cell.id()] = gradAnom(rel_pos, grad_t2)
#     anom_res[cell.id()] = gradAnom(rel_pos, grad_res)
#     if cell.center().y() >= -50 and cell.center().y() <= 0:
#         allanom += gradAnom(rel_pos, grad_res) * cell.size()
#         allsize += cell.size()
#     cell.setMarker(mark)
#     mark += 1
