# -*- coding: utf-8 -*-
"""
run previously:

    Scripts 1-5

What this script does:

    Calculation of kernel functions for synthetic modelling.
"""
from comet import pyhed as ph
from comet import snmr


if __name__ == '__main__':
    __spec__ = None

    ph.log.setLevel(13)
    ph.addLogFile()

    # =========================================================================
    # User input
    # =========================================================================
    # feel free to change the number
    number_of_used_cores = 48
    invmesh = 'block/paramesh_2d.bms'
    cfgname = './block/custEMConfig.cfg'
    survey_name = 'block/kernelsurvey'

    # y discretization
    # max distance between slices inside the loops
    max_length = 0.05
    # number of slices
    max_num = 1200

    # init survey
    # step 0: empty survey class
    site = snmr.survey.Survey()

    # step 1: add loops (sources and receiver are determined afterwards)
    loops = ph.loop.loadLoops('block/loop_{}', num=8, load_meshes=False)

    for lp, loop in enumerate(loops):
        site.addLoop(loop)
        ph.log.info('{!r}'.format(loop))
        site.createSounding(tx=0, rx=lp)

    site.save(survey_name)

    # step 3 calculate kernel based on survey and magnetic fields
    name = snmr.kernel.calculateKernelFromSlices(
        survey_name,
        invmesh,
        cfgname,
        max_length=max_length,
        max_num=max_num,
        path_name='kernel',
        num_cpu=number_of_used_cores,
        h_order=1,
        json_name='block/kernel/kernelConfig.json',
        force_new_paths=False,
        slice_export_name='kernel_slice',
        kernel_name='block/kernel/kernp2_{}')


# The End
