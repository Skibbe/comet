# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 15:13:56 2018

@author: Skibbe.N

part of 2D MRT

defining 2D parameter mesh + export
"""

import pygimli as pg
import matplotlib.pyplot as plt
import numpy as np
from comet import pyhed as ph
from pathlib import Path

ph.log.setLevel(13)
ph.addLogFile()

plt.ioff()

def scale2Coords(points, x_range, y_range):
    anom_rx = np.abs(x_range[1] - x_range[0])
    anom_ry = np.abs(y_range[1] - y_range[0])
    rx = np.abs(np.min(points[:, 0]) - np.max(points[:, 0]))
    ry = np.abs(np.min(points[:, 1]) - np.max(points[:, 1]))

    # scale
    points *= np.array([[anom_rx/rx, anom_ry/ry]])

    # shift
    points[:, 0] += x_range[0] - np.min(points[:, 0])
    points[:, 1] += y_range[0] - np.max(points[:, 1])
    return points

# %% poly valley

path = Path('geometry')
path.mkdir(exist_ok=True, parents=True)

outer_x = [-195, 195]
outer_y = [-75, 0]

x_dim = [-175, 175]
y_dim = [0, -100]
area = 3.
anom_area = 3.  # for the layered case we need a good approximation in z
inv_area = 10.

invouter = pg.meshtools.createWorld([x_dim[0], y_dim[0]],
                                    [x_dim[1], y_dim[1]],
                                    marker=1)


world = pg.meshtools.createWorld([x_dim[0], y_dim[0]],
                                 [x_dim[1], y_dim[1]], area=area, marker=1)

invinner = pg.meshtools.createRectangle(start=(-120, 0), end=(120, -75))

invworld = pg.meshtools.mergePLC([invouter, invinner])
invworld.addRegionMarker([0, -1], marker=1, area=inv_area)
invworld.addRegionMarker([0, -99], marker=2, area=inv_area*3.5)

for x in range(-120, 121, 2):
    world.createNode([x, 0.])
    invworld.createNode([x, 0.])
    invworld.createNode([x, -0.3])

invmesh = pg.meshtools.createMesh(
    invworld, quality=34, smooth=[2, 1], verbose=True)

# clean markers from mesh generation
invinner.setCellMarkers(np.ones(invinner.cellCount(), dtype=int))
invmesh.setCellMarkers(np.zeros(invmesh.cellCount()))
bm = invmesh.boundaryMarkers()
bm[bm > 0] = 0
invmesh.setBoundaryMarkers(bm)

invmesh.save(path.joinpath('invmesh.bms').as_posix())
print('invmesh', invmesh)

pg.show(invmesh)
plt.show()

line_depth = -2
anom_x = [-50, 50]
anom_z = [-15, -30]
anom_points = np.array([[x_dim[0], anom_z[0]],
                        [-115., -9.],
                        [-108.43569275, -10.55555556],
                        [-103.00656646, -10.55555556],
                        [-96.59032629, -12.11111111],
                        [-93.62898467, -15.55051269],
                        [-91.65475692, -19.],  # no 6
                        [-88.19985837, -21.82607721],
                        [-84.25140288, -25.74830504],
                        [-78.82227659, -28.8860873],
                        [-71.91247948, -29.67053286],
                        [-61.05422689, -29.90720009],
                        [-47.23463268, -30.],
                        [-26.99879829, -30.],
                        [-1.33383762, -30.],
                        [11.00508578, -27.31719617],
                        [16.92776901, -24.57163669],
                        [24.82467999, -22.82607721],
                        [32.72159097, -19.],  # no 18
                        [44.0734005, -16.90384938],
                        [54.43809616, -15.94273547],
                        [63.81567794, -13.98162156],
                        [74.67393053, -12.80495321],
                        [87.99996781, -10.8438393],
                        [99.84533427, -10.44494873],
                        [115., -9.],
                        [x_dim[1], anom_z[0]]])

anom_points2 = scale2Coords(anom_points[1:-1], anom_x, anom_z)
anom_points[1:-1] = anom_points2
node1 = anom_points[6]
print(node1)
node2 = anom_points[18]
print(node2)
hori0 = pg.meshtools.createLine([x_dim[0], line_depth],
                                [x_dim[1], line_depth], 1)
print('node1', [x_dim[0], node1[1]], node1)
hori1 = pg.meshtools.createLine([x_dim[0], node1[1]], node1, 1)
hori2 = pg.meshtools.createLine(node2, [x_dim[1], node2[1]], 1)

anom = pg.meshtools.createPolygon(anom_points, isClosed=False)
plot_anom = pg.meshtools.mergePLC([anom, hori0, hori1, hori2])

world = pg.meshtools.mergePLC([world, anom, hori0, hori1, hori2])

# clay left
world.addRegionMarker([anom_x[0] - 5., anom_z[0] - 1.], marker=2, area=area)
# clay right
world.addRegionMarker([anom_x[1] + 5., anom_z[0] - 1.], marker=2, area=area)
# top
world.addRegionMarker([0., line_depth*0.5], marker=1, area=0.25)
# base
world.addRegionMarker([0., anom_z[1] - 5.], marker=3, area=area*5)
# valley
world.addRegionMarker([0., anom_z[1] - anom_z[0]], marker=4, area=anom_area)

# %% export
plot_anom.save(path.joinpath('anom.bms').as_posix())  # for later plot
ax, _ = pg.show(world)

ax.figure.savefig(path.joinpath('paramesh2D_poly.pdf').as_posix(),
                  bbox_inches='tight', facecolor='none',
                  edgecolor='none')

ax.figure.savefig(path.joinpath('paramesh2D_poly.png').as_posix(),
                  dpi=600, bbox_inches='tight', facecolor='none',
                  edgecolor='none')
plt.close(ax.figure)

pg.meshtools.exportPLC(world, path.joinpath('paramesh.poly').as_posix())

paramesh2d = pg.meshtools.createMesh(world, quality=33)
#pg.show(paramesh2d)
#plt.show()
#pg.show(invmesh)
#plt.show()
#bla
paramesh2d.save(path.joinpath('raw_paramesh_2d').as_posix())

ax, _ = pg.show(paramesh2d)
ax.figure.savefig(path.joinpath('raw_paramesh2D.pdf').as_posix(),
                  facecolor='none',
                  edgecolor='none', bbox_inches='tight')

ax.figure.savefig(path.joinpath('raw_paramesh2D.png').as_posix(),
                  facecolor='none',
                  edgecolor='none', bbox_inches='tight', dpi=1200)
plt.close(ax.figure)

ax, _ = pg.show(invmesh)
ax.figure.savefig(path.joinpath('invmesh.pdf').as_posix(),
                  bbox_inches='tight', facecolor='none',
                  edgecolor='none')

ax.figure.savefig(path.joinpath('invmesh.png').as_posix(),
                  dpi=600, bbox_inches='tight', facecolor='none',
                  edgecolor='none')
plt.close(ax.figure)

print('paramesh2d: {}'.format(paramesh2d))

area = 5.
start_inner = [paramesh2d.xmin(), 0.]
end_inner = [paramesh2d.xmax(), -3]
start = [paramesh2d.xmin(), paramesh2d.ymin()]
end = [paramesh2d.xmax(), paramesh2d.ymax()]

print(start_inner)
print(end_inner)
print(start)
print(end)

# The End
