# -*- coding: utf-8 -*-
"""
@author: Skibbe.N
"""
import custEM
import numpy as np
import pygimli as pg

from comet import pyhed as ph
from comet import snmr
from pathlib import Path


"""
NOTE:

    This script calculates the magnetic fields based on various resistivity
    distributions. This takes some time. Hence you can select the case you
    are interested in and calcualte the loops only for that case. You do this
    by commenting in the path  variable you want: block, layered or ert.

    For generating the synthetic data, "block" is needed, which is uncommented
    by default.

    You can only run one calcuation the a time. So you have to do the cases
    after one another.

    Cases:

        block: synthetic blocky resistivity
        ert: ert inverted resistivity
        layered: simplified 1D resistivity (no custEM needed)

"""


if __name__ == '__main__':
    ph.log.setLevel(20)
    ph.addLogFile()
    loops = ph.loop.buildEtraSurvey(50)
    earth = snmr.survey.Earth(decl=60, incl=0, mag=48000e-9)
    loop_config = ph.config(rho=[1000.], f=earth.larmor)

    # %% block
    path = Path('block')
    path.mkdir(exist_ok=True)
    bound = False

    # %% layered
    # path = Path('layered')
    # bound = False

    # %% ert
    # path = Path('ert')
    # bound = True

    # %% calc and save

    if path.name in ['block', 'ert']:
        loopmesh = pg.Mesh(path.joinpath('_h5/fem_mesh.bms').as_posix())
        for lp, loop in enumerate(loops):
            ph.log.info('prepare loop: {}'.format(lp))

            # configuration like directories and names and resistivity
            loop.setPrimaryConfig(loop_config)
            loop.createSecondaryConfig(
                mod_name=path.joinpath('mod_{}'.format(lp)).as_posix(),
                mesh_name='fem_mesh',
                m_dir=path.as_posix(),
                r_dir=path.as_posix(),
                pf_name=path.joinpath('prim_{}'.format(lp)).as_posix())

            # import 2d mesh and resistivity distribution
            res = np.load(path.joinpath('anom_res.npy').as_posix())
            sigma = 1./res
            if path.name == 'ert':
                paramesh_2d = path.joinpath('invmesh.bms').as_posix()
            else:
                paramesh_2d = path.joinpath('paramesh_2d.bms').as_posix()

            loop.setParaMesh2D(
                paramesh_2d,
                anomaly=sigma,
                sort=True,
                append_boundary=bound)

            ph.log.info(loop.secondary_config)

            # set finite element mesh
            loop.setFEMMesh(loopmesh, savename='fem_mesh')
            ph.log.info(loop.loopmesh)
            ph.log.info(loop.loop_mesh_name)

            # prepare primary field
            loop.prepareSecondaryFieldCalculation(
                force_primary=True,
                set_marker=False)

            # calc secondary field
            loop.calculateSecField(num_cpu=32, python_to_call='python')
            loop.save(path.joinpath('loop_sec_{}'.format(lp)).as_posix(),
                      save_mesh=False)

    elif path.name == 'layered':
        model1d = np.load(path.joinpath('background.npy').as_posix())
        resthk = model1d[0]
        split = int((len(resthk) + 1)/2)
        res, thk = resthk[:split], resthk[split:]
        loopmesh = pg.Mesh(path.joinpath('loopmesh.bms').as_posix())

        for lp, loop in enumerate(loops):
            ph.log.info('prepare loop: {}'.format(lp))

            # configuration like directories and names and resistivity
            loop_config = ph.config(rho=res, d=thk, f=earth.larmor)
            loop.setPrimaryConfig(loop_config)

            # set loop mesh
            loop.setLoopMesh(
                loopmesh,
                savename=path.joinpath('loopmesh.bms').as_posix())
            loop.calculate(num_cpu=32, verbose=True)
            loop.save(path.joinpath('loop_sec_{}'.format(lp)).as_posix(),
                      save_mesh=False)

# The End
