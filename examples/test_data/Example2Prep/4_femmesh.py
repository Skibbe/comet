# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 18:57:58 2018

@author: skibbe

creation of fem mesh out of paramesh2d and anomaly vectors...
"""
from pathlib import Path
import pygimli as pg
import numpy as np
from comet import pyhed as ph
from comet import snmr
# import custEM tools for FE mesh build
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld


ph.log.setLevel(13)
ph.addLogFile('4_femmesh')

paradir = Path('block')

# import parameter mesh
paramesh2d = pg.load(
    paradir.joinpath('paramesh_2d.bms').as_posix())
boundary = False

# import resistivty
para_res = np.load(paradir.joinpath('anom_res.npy').as_posix())
para_sig = 1./para_res

# =============================================================================
# create FEM mesh
# =============================================================================
# first: source discretization
txrx = snmr.misc.etraSourceDiscretization(-100, 100, 50, segments=100)

# second: blank world with outer dimensions fitting to input parameter mesh
M = BlankWorld(name='fem_mesh',
               x_dim=[-130, 130],
               y_dim=[-100, 100],
               z_dim=[-50, 15],
               r_dir='./block',
               m_dir='./block',
               subsurface_cell_size=35,  # innerer kasten 3d mit dim von oben
               )

# third add source didscretization to blank world
M.build_surface(insert_loop_tx=txrx)

# fourth: refine area in a 2 m cube around tx/rx
M.add_surface_anomaly(insert_paths=[mu.loop_r(start=[-127, -27.],
                                              stop=[127., 27.], n_segs=4)],
                      depths=[-2.],
                      cell_sizes=[1],
                      # inner loop cell size (3d) (für den meter tiefe)
                      marker_positions=[[-126, -26, -1]]
                      #  marker, x, y, z
                      )

# build surface mesh (geometry of refinement box including tx/rx)
M.build_halfspace_mesh()

# adding boundary as factor to original extend
M.there_is_always_a_bigger_world(20., 30., 60.)

# build mesh, export vtk as well
M.call_tetgen(tet_param='-pq1.4aA', export_vtk=True)

mesh = pg.load('block/mesh_create/fem_mesh.1.vtk')
mesh.save('block/_h5/fem_mesh.bms')
# Note: Final meshes are saved in directory "./block/_h5"

# temporarily create empty loop-class for the purpose of handling
# input resistivities and providing methods
mesh_loop = ph.loop.buildDummy()
mesh_loop.createSecondaryConfig(
    'mod', 'fem_mesh', m_dir=paradir.as_posix(),
    r_dir=paradir.as_posix(), approach='E_t')
# set 2D mesh and geometry
mesh_loop.setParaMesh2D(paramesh2d, append_boundary=boundary,
                        anomaly=para_sig, sort=True, limits=[-120, 120])

# adds resistivity distribution to mesh and saves
# final .h5 mesh file for custEM
mesh_loop.setFEMMesh(mesh, savename='fem_mesh')

# earth magnetic field: Bfield vector and frequency
earth = snmr.survey.Earth(decl=0, incl=60, mag=48000e-9)
mesh_loop.secondary_config.frequency = earth.larmor

# this file is used in custem as input configuration
# and later for the kernel calculation as well
mesh_loop.secondary_config.save('block/custEMConfig.cfg')

# The End
