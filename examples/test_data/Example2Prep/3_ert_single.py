# -*- coding: utf-8 -*-
"""
ERT inversion in preparation of the synthetic data for example 2

requires an installation of pybert:

>>>    conda install -c gimli pybert

tested for pygimli version >=1.1
used comet version 1.0.2

"""
import pygimli as pg
import numpy as np
import os

import matplotlib.pylab as plt
from pathlib import Path
from comet import pyhed as ph
import logging

ph.log.setLevel(13)
logging.getLogger('pyGIMLi').setLevel(15)

# parameter mesh
p_dir = Path('block')
simulate = True

# result dir
r_dir = Path('ert')
r_dir.mkdir(exist_ok=True)

paramesh2d = pg.load(p_dir.joinpath('paramesh_2d.bms').as_posix())

syn_arr = np.load(p_dir.joinpath('anom_res.npy').as_posix())

invmesh = pg.Mesh(Path('geometry').joinpath('invmesh.bms').as_posix())
asdfasfd
invmesh.save(r_dir.joinpath('invmesh.bms').as_posix())

# append_boundary:
paramesh2d = pg.meshtools.appendTriangleBoundary(
    paramesh2d, marker=0)
cm = np.array(paramesh2d.cellMarkers())
orig_cells = cm != 0

anom = np.zeros(paramesh2d.cellCount())
anom[orig_cells] = syn_arr
anom = pg.Vector(anom)
paramesh2d.prolongateEmptyCellsValues(anom)

 # import scheme created by "3a_ert_scheme.py"
scheme = pg.load('ert/ert_scheme.shm')

if simulate:
    # ert_sim = pb.Resistivity()
    ert_sim = pg.physics.ert.ERTManager()
    data = ert_sim.simulate(paramesh2d, scheme, anom, noiseLevel=0.02)
    data.save(r_dir.joinpath('ert_data.dat').as_posix())
else:
    data = pg.load(r_dir.joinpath('ert_data.dat').as_posix())

ert = pg.physics.ert.ERTManager()
ert.fop.setData(data)

ert.fop.setThreadCount(16)  # sep vaut for > 1
ph.log.info('start inversion')
zweight = 0.2
lam_ert = 250
ert.invert(mesh=invmesh, data=data, zWeight=zweight, lam=lam_ert)
pg.show(ert.paraDomain)
ph.log.info('finished inversion')
ph.log.info('Final Chi²: {:.2f}'.format(ert.inv.chi2()))

# export results + meshes
anom_poly = pg.Mesh('geometry/anom.bms')
ax, cbar = ert.showResult(cMap='viridis', cMin=20, cMax=1000)
figpath = r_dir.joinpath('ert_single_{}_z{}.pdf'
                         .format(int(lam_ert),
                                 int(10 * zweight)))
ax.figure.savefig(figpath.as_posix(), bbox_inches='tight')
ax.figure.savefig(figpath.with_suffix('.png').as_posix(),
                  bbox_inches='tight')

ph.plot.drawMeshLines(ax, anom_poly)
ax.figure.savefig(
    figpath.with_name(figpath.stem + '_lines.pdf').as_posix(),
    bbox_inches='tight')
ax.figure.savefig(
    figpath.with_name(figpath.stem + '_lines.png').as_posix(),
    bbox_inches='tight')

plt.close(ax.figure)

# for savekeeping
savename = 'ert_single_{}_z{}'.format(lam_ert, zweight)
np.save(figpath.with_suffix('.npy').as_posix(),
        ert.inv.model.array())

# anom_res.npy for magnetic field calcualtion
np.save(r_dir.joinpath('anom_res.npy').as_posix(),
        ert.inv.model.array())

ert.fop.mesh().save(r_dir.joinpath('paraDomainWithBound.bms').as_posix())
ert.fop.regionManager().paraDomain().save(
    r_dir.joinpath('paraDomain.bms').as_posix())

# The End
