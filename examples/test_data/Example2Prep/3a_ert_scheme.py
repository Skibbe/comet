# -*- coding: utf-8 -*-
"""
Create Ert measurement scheme wth pybert. Only thing that needs pybert, this
is the reason I made this an extra script and provide the scheme with the
source code. usually you just import a scheme from a file, like we will do for
example in the script "3_ert_single.py".

To install pybert:

simply clone the Bert repository from gitlab:

>>> git clone https://gitlab.com/resistivity-net/bert

or install via conda:

>>>    conda install -c gimli pybert

tested for pygimli version <1.1, not for >= 1.1 !
used comet version 1.0.2

"""
import pybert as pb
from pathlib import Path

from comet import pyhed as ph

if __name__ == '__main__':
    ph.log.setLevel(13)
    ph.addLogFile()

    # outpt folder
    r_dir = Path('ert')
    r_dir.mkdir(exist_ok=True)

    # scheme: gradient with 121 electrodes @ 2 m spacing
    scheme = pb.data.createData(121, schemeName='gr', spacing=2.)
    scheme.translate([-120., 0.])

    scheme.save('ert/ert_scheme.shm') # pg can import but not create

# The End
