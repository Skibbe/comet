# -*- coding: utf-8 -*-
"""
Created on Wed Oct 24 18:43:49 2018

@author: skibbe

mrt single: data preparation

run previously:

    All scripts  from 1-5 for :

    - geometry generation (paramesh)
    - handling of synthetic model in para domain
    - synthetic data creation for ert
    - creation of suited FEM mesh for magnetic fields based on paramesh
    - calculation of magnetic fields
    - calculation of kernel functions

What this script does:

    - simulation of synthetic mrt dataset for given layout.
"""
import pygimli as pg
import numpy as np

import comet
from comet import pyhed as ph
from comet import snmr
from pathlib import Path

import matplotlib.pyplot as plt


if __name__ == '__main__':
    ph.log.setLevel(13)
    ph.addLogFile()

    # %% directories
    load_kernel = False

    # FIXED to block as it only creates the synthetic data set !!!!
    inpath = Path('block')

    # using smooth magnetic fields for simulation
    # invert with smooth/block and layered to see the differences
    outpath = Path('synthetic_data')
    outpath.mkdir(exist_ok=True)

    # import from paramesh 2d folder
    paramesh2d = pg.load(inpath.joinpath('paramesh_2d.bms').as_posix())
    para_t2 = np.load(inpath.joinpath('anom_t2.npy').as_posix())
    para_wc = np.load(inpath.joinpath('anom_wc.npy').as_posix())
    # synthetic model mrt
    syn_mrt = pg.cat(para_wc, para_t2)

    noise_factor = 0.25

    # init survey
    # step 0: empty survey class
    site = snmr.survey.Survey()

    # step 1: add loops (sources and receiver are determined afterwards)
    loops = ph.loop.loadLoops(inpath.joinpath('loop_{}').as_posix(), num=8,
                              load_meshes=False)
    # Add loops to site using implicit ordering
    site.addLoop(loops)

    # step 2: define local earth
    earth = snmr.survey.Earth(decl=0, incl=60, mag=48000e-9)
    site.setEarth(earth)

    # step 3: this defines which loop is transmitter and receiver for each
    # measurement as well as used gates.
    gates = np.logspace(-2, 0, 32)

    for i in range(8):
        site.createSounding(tx=0, rx=i)
        site.fids[-1].setGates(gates)

    mrt = snmr.modelling.MRT(
        survey=site, dim=2, dtype='complex', mtype='smooth')

    for si, sound in enumerate(mrt.sounds):
        # import kernel without mesh
        # mesh will be imported for all at once again
        sound.kernel.load('block/kernel/kernp2_{}'.format(si),
                          load_kernelmesh=False)

    # simulate synthetic data set
    # FOP simulate, one the synthetic paramesh, kernel only for simulate
    # also sets kernel mesh for all kernel instances
    mrt.createFOP(kernelmesh=paramesh2d)

    syn_errlvl = []
    for fop in mrt.fop.fop_list:
        syn_errlvl.append(
            snmr.modelling.effectiveNoise(fop.kernel.rx_area) *
            np.sqrt(0.5) * noise_factor)

    ph.log.info('syn_errlvl: {}'.format(syn_errlvl))

    ph.log.info(mrt)
    ph.log.info(mrt.survey.earth)
    mrt.simulate(syn_mrt, syn_errlvl, maxCPUCount=32, verbose=True)

    site.save(outpath.joinpath('data'), save_loops=False)

    site.save('{}/../examples/example2/survey/survey_ready'
              .format(comet.basepath), save_loops=False)
    # save prepared data, no need for loops anymore, so cannot accidently use
    # them during the inversion
    # side note: if loops are saved, by default a new name is given based on
    # the name the survey is saved under.

    # do some standard output figures of data and error

    fig, axes = plt.subplots(2, 4, figsize=(9, 9))
    for axi, ax in enumerate(np.array(axes).flatten()):
        ph.plot.drawFid(ax, site.fids[axi],
                        draw='data',
                        cmap='bwr',
                        clim=None,
                        to_plot='real',
                        cbar=True)
        fig.savefig(
            outpath.joinpath('syn_data_real.pdf').as_posix(),
            bbox_inches='tight')

    fig, axes = plt.subplots(2, 4, figsize=(9, 9))
    for axi, ax in enumerate(np.array(axes).flatten()):
        ph.plot.drawFid(ax, site.fids[axi],
                        draw='data',
                        cmap='bwr',
                        clim=None,
                        to_plot='imag',
                        cbar=True)
        fig.savefig(
            outpath.joinpath('syn_data_imag.pdf').as_posix(),
            bbox_inches='tight')

    fig, axes = plt.subplots(2, 4, figsize=(9, 9))
    for axi, ax in enumerate(np.array(axes).flatten()):
        ph.plot.drawFid(ax, site.fids[axi],
                        draw='data',
                        cmap='bwr',
                        clim=None,
                        to_plot='abs',
                        cbar=True)
        fig.savefig(
            outpath.joinpath('syn_data_abs.pdf').as_posix(),
            bbox_inches='tight')

    fig, axes = plt.subplots(2, 4, figsize=(9, 9))
    for axi, ax in enumerate(np.array(axes).flatten()):
        ph.plot.drawFid(ax, site.fids[axi],
                        draw='error',
                        cmap='bwr',
                        clim=None,
                        to_plot='abs',
                        cbar=True)
        fig.savefig(
            outpath.joinpath('syn_error_abs.pdf').as_posix(),
            bbox_inches='tight')

    ph.log.info('You have now prepared the synthetic data set for example 2 '
                'of the comet package. Please continue with example 2 in '
                '"comet/examples". Figures with data and error plots from the '
                'synthetic data are found in {}'
                .format(outpath.absolute().as_posix()))

    plt.close('all')

# The End
