'''
This is the second part of the example 2 if custem is used.

Here the magnetic field are calculated based on the ERT resitivity
distribution shown in the manuscript.

This script can take a while to complete, a couple of minutes for each field,
summing up to

    1-2 hours using 48 cores.

Feel free to change the number of used cores and expect a memory usage of

    up to !!! 175 Gb !!!

for the direct solver of the FEM problem.
Using a coarser mesh would lower the memory requirements, however also reducing
the quality of the subsequent kernel calculation. If no hardware is at hand,
please consider using the no_custEM version of the second example,
which can be calculated on every computer.

The result of this file ('surveys/survey_ready.npz' as well as some additional
files) are imported by part 3 of the example.

Results of this script can be found in the directory
"example3/E_s/fem_mesh_results" in the form of .pvd files. Similar to .vtk they
can be imported via paraview for visualization.
Example name for first loop, real and imaginary field:

    'example3/E_s/fem_mesh_results/loop_0_H_t_real_cg.pvd'
    'example3/E_s/fem_mesh_results/loop_0_H_t_imag_cg.pvd'
'''
# =============================================================================
# Import main parts of COMET
from comet import pyhed as ph
from comet import snmr
# import numpy and pygimli libraries
import numpy as np
import pygimli as pg

if __name__ == '__main__':  # protection for multiprocessing
    __spec__ = None  # additionaly required if working with spyder
    # The past two lines are sometimes necessary and does not harm otherwise.
    # Unfortunately this is something that cannot be adressed from within
    # COMET. Let's now resume with the script:
    # log level 20 == Info level, some infos of what is happening are given
    ph.log.setLevel(20)
    ph.addLogFile()

    # =========================================================================
    # User input
    # =========================================================================
    # feel free to change the number
    number_of_used_cores = 48

    # =========================================================================
    # import stuff from first script
    # =========================================================================
    # import 2D parameter mesh from ERT inversion done with pyGIMLi
    paramesh2d = pg.Mesh('test_data/Example2Prep/ert/invmesh.bms')
    # create local copy
    paramesh2d.save('invmesh.bms')
    # import associated resistivity vector
    res = np.load('test_data/Example2Prep/ert/anom_res.npy')
    # load survey with data and raw loops: we will set femmesh later
    site = snmr.survey.Survey()
    site.load('example3/survey/survey_withloops', load_meshes=False)
    # load femmesh once and then distribute later
    ph.log.info('import "example3/_h5/fem_mesh.bms"...')
    femmesh = pg.Mesh('example3/_h5/fem_mesh.bms')

    # =========================================================================
    # calculate fields
    # =========================================================================
    # loop over all loops to be calculated
    for li, loop in enumerate(site.loops):
        # define mesh and result directory
        loop.createDefaultSecondaryConfig(m_dir='example3', r_dir='example3')
        # set 2D ERT inversion mesh and appropriate model,
        # append_boundary: prevent res model to fall back to 1000 Ohmm
        # immediately where the 2D mesh ends (FEM mesh is much larger).
        loop.setParaMesh2D(
            paramesh2d,
            anomaly=1./res,  # conductivity needed here
            append_boundary=True)
        # setting femmesh
        loop.setFEMMesh(femmesh, savename='example3/_h5/fem_mesh.bms')
        # Computes primary field for secondary field calculation on 48 cores.
        # Initializes the custEM instance for subsequent computation and
        # insures that femmesh and primary field are found.
        ph.log.info('Calculating fields. This can take a while...')
        loop.prepareSecondaryFieldCalculation(
            num_cpu=number_of_used_cores, mod_name=f'loop_{li}',
            force_primary=True)
        # calculate secondary field based on available primary field
        loop.calculateSecField(num_cpu=number_of_used_cores)
    # save calcualted fields and mesh
    site.save('example3/survey/survey_ready')
    ph.log.info('Example 3 part 2 run finished.')
