'''
This is the first part of the example 2 using the custEM library.

In this script a suited finite element mesh is created which includes the
source geometry of the 8 used loops as well as the parameter mesh markers
for the subsequent transfer of resistivity values.
'''
# =============================================================================
# Import main parts of COMET
from comet import pyhed as ph
from comet import snmr
# import numpy and pygimli libraries
import numpy as np
import pygimli as pg
# import custEM tools for FE mesh build
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
# set logger to give us some feedback during computation
# 20 == info level
ph.log.setLevel(20)

# Add logging file which catches output and executed code, so later you
# know all the parameters you setted when you change things. I found this
# to be extremely useful, you can uncomment it, however the file is not
# big and invaluable if something is not working.
ph.addLogFile()
# =============================================================================
# loop configuration: 1D earth + frequency
# =============================================================================
# earth magnetic field: Bfield vector and frequency
earth = snmr.survey.Earth(decl=0, incl=60, mag=48000e-9)
# Now we import the 2D parameter mesh from the ERT inversion.
# This is where the resistivity lives on.
# In order to transfer resistivities to the FEM mesh, we need the geometry.
paramesh2d = pg.Mesh('test_data/Example2Prep/ert/invmesh.bms')
# import associated resistivity vector
res = np.load('test_data/Example2Prep/ert/anom_res.npy')
# etra survey, small edge length = 50 meter, using 8 loops in total
# maximum length of a dipole is 0.51 m, you can try other values
loops = ph.loop.buildEtraSurvey(50, num_loops=8, max_length=0.51)
# =============================================================================
# survey
# =============================================================================
# we want to gather everything in this bare survey class
# later we can simply import the whole thing
site = snmr.survey.Survey()
# survey containing the simulated data
site.load('test_data/Example2Prep/synthetic_data/data')

# In this case the primary background field of the unsaturated zone
# (we use the resistivity close to the loop wires as background) is a
# halfspace with 1000 Ohmm using the larmor frequency
loop_config = ph.config(rho=[1000.], f=earth.larmor)
# let's make sure every loops knows this
site.setLoopConfig(loop_config)

# looping over the loops
for lp, loop in enumerate(loops):
    # add loop to survey class, generate index and so on for later
    # also our config is passed on to the loops
    site.addLoop(loop)

# lets set earth (like config for loops, just for NMR later on)
site.setEarth(earth)

# let's check some console output, for example the final config
ph.log.info(loops[0].config)
# we use a different name to ensure the original files remain intact for reruns
site.save('surveys/survey_withloops')
# this actually creates several files:
# main survey object: "surveys/survey_withloops.npz"
# + one fid object with id: "surveys/survey_withloops_fid_0...7.npz"
# + one loop object with id: "surveys/survey_withloops_loop_0...7.npz"
# =============================================================================
# create FEM mesh
# =============================================================================
# first: source discretization
txrx = snmr.misc.etraSourceDiscretization(-100, 100, 50, segments=100)

# second: blank world with outer dimensions fitting to input parameter mesh
M = BlankWorld(name='femesh',
               x_dim=[-130, 130],
               y_dim=[-100, 100],
               z_dim=[-50, 15],
               r_dir='./results',
               m_dir='./meshes',
               subsurface_cell_size=35,  # innerer kasten 3d mit dim von oben
               )

# third add source didscretization to blank world
M.build_surface(insert_loop_tx=txrx)

# fourth: refine area in a 2 m cube around tx/rx
M.add_surface_anomaly(insert_paths=[mu.loop_r(start=[-127, -27.],
                                              stop=[127., 27.], n_segs=4)],
                      depths=[-2.],
                      cell_sizes=[1],
                      # inner loop cell size (3d) (für den meter tiefe)
                      marker_positions=[[-126, -26, -1]]
                      #  marker, x, y, z
                      )

# build surface mesh (geometry of refinement box including tx/rx)
M.build_halfspace_mesh()

# adding boundary as factor to original extend
M.there_is_always_a_bigger_world(20., 30., 60.)

# build mesh, export vtk as well
M.call_tetgen(tet_param='-pq1.4aA', export_vtk=True)

mesh = pg.load('meshes/mesh_create/femesh.1.vtk')
mesh.save('meshes/_h5/femesh.bms')
# Note: Final meshes are saved in directory "./meshes/_h5"

# temporarily create merged loop-class for the purpose of handling
# input resistivities
mesh_loop = ph.loop.buildDummy()
mesh_loop.createSecondaryConfig('example2_mod', 'femesh',
                                m_dir='./meshes', r_dir='./results',
                                approach='E_t')
# set 2D mesh and geometry
mesh_loop.setParaMesh2D(paramesh2d, append_boundary=True,
                        anomaly=1./res,  # need conductivity here
                        limits=[-100, 100],)
# finally create FEM mesh and export additional VTK for quality control
# ! this is the step that needs custEM the first time !

# adds resistivity distribution to mesh and saves
# final .h5 mesh file for custEM
mesh_loop.setFEMMesh(mesh, savename='femesh')

# this file is used in custem as input configuration
# and later for the kernel calculation as well
mesh_loop.secondary_config.frequency = earth.larmor
mesh_loop.secondary_config.save('./results/custEMConfig.cfg')

# save anomaly vector for later as well
np.save('./results/anom_vector.npy', mesh_loop.secondary_config.sigma_anom)

# =============================================================================
# some output from this script
# =============================================================================
# plot the loop layout
ph.plot.showLoopLayout(loops)
# furthermore you can use paraview to open the '_h5/fem_mesh.vtk'
# using a y-normal slice you can select _attribute to see the ERT resistivity
# values distributed in 3D
ph.log.info('Example 2 part 1 run finished.')
