#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

"""
from comet import snmr
import numpy as np
from comet import pyhed as ph
import matplotlib.pyplot as plt


ph.log.setLevel(20)
print('remember this work with qt5, not inline plots...')

# %% dipole discretization
loop = ph.loop.buildCircle(1, 15)
m2 = np.append(loop.pos, np.atleast_2d(loop.pos[0]), axis=0)

# %% cacl integration and curl windows
quadsh_rotated, quad_dir, d_len = snmr.modelling.createIntegrationPoints(m2)

# %% plot

fig = plt.figure()
ax = plt.axes(projection='3d')

for idx in range(len(d_len)):
    ax.plot3D(quadsh_rotated[idx, :, 0],
              quadsh_rotated[idx, :, 1],
              quadsh_rotated[idx, :, 2], 'gray')

    ax.scatter3D(quadsh_rotated[idx, :, 0],
                 quadsh_rotated[idx, :, 1],
                 quadsh_rotated[idx, :, 2], cmap='Greens')

    ax.quiver(quadsh_rotated[idx, :, 0],
              quadsh_rotated[idx, :, 1],
              quadsh_rotated[idx, :, 2],
              quad_dir[idx, :, 0],
              quad_dir[idx, :, 1],
              quad_dir[idx, :, 2], length=0.1, normalize=True)

d_mid = (m2[:-1] + m2[1:]) / 2
ax.scatter(d_mid[:, 0],
           d_mid[:, 1],
           d_mid[:, 2])

# just some outer corner for aspect=1
ax.scatter(np.array((-1, -1, 1, 1)) * 0.4,
           np.array((-1, 1, -1, 1)) * 0.4,
           np.array((-1, -1, -1, 1)) * 0.4)


ax.axes.xaxis.set_label('X')
ax.axes.yaxis.set_label('Y')
ax.axes.zaxis.set_label('Z')

# The End
