.. _authorlabel:

Authors
=======

Corresponding author for **COMET**:

    Nico Skibbe - nico.skibbe@leibniz-liag.de

Corresponding author for **custEM**:

    Raphael Rochlitz - raphael.rochlitz@leibniz-liag.de

Additional support and contact for issues regarding **pyGIMLi**:

    Thomas Günther - thomas.guenther@leibniz-liag.de