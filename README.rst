COupled Magnetic resonance and Electrical resistivity Tomography (**COMET**)
============================================================================

The code developed in this project has the main goal to invert magnetic 
resonance data, particularly in 2D, utilizing the resistivity information
of 2D ERT measurements.

**GitLab repository**: https://gitlab.com/Skibbe/comet/

**Documentation**: https://comet-project.readthedocs.io/

.. If you can read this, you have opened the bare readme.rst in an editor. The compiled version at https://comet-project.readthedocs.io/ is much easier to read.

Third-party package availability and dependencies
-------------------------------------------------

**pyGIMLi**: The documentation and the repository can be accessed at https://www.pygimli.org/.

**TetGen**: The 3D tetrahedral mesh generator http://tetgen.org. Available using conda (see below).

**custEM**: The custEM repository is located at https://gitlab.com/Rochlitz.R/custEM/, the documentation at https://custem.readthedocs.io/en/latest/.

Other Dependencies
------------------

The complete list of dependencies are listed below. Note that almost all dependencies are shared with **pyGIMLi** as well, so installation of **pyGIMLi** or **custEM** usually meets all requirements for **COMET** as well. If working on linux, the installation of **custEM** (and implicetly all other requirements including **comet**) via conda is highly recommended. 

- python >= 3.8
- numpy >= 1.17  # The version depends on the pygimli installation as they share binary files (main reason why conda installation is recommended)
- pyGIMLi >= 1.2
- TetGen
- h5py
- scipy
- matplotlib

Optional (Linux only):

- custEM >= 1.0
Note: An installation of custEM actually requires comet, so a simple installation of custEM will usually take care of everything for you.

Installation
------------
The easiest way of handling the dependencies is to use the conda (Anaconda or miniconda) system. For the full installation guide please read the installation instructions on the main documentation webpage of the project: `Installation <https://comet-project.readthedocs.io/en/stable/installation.html#>`_

License
=======

Copyright 2016-2023 Skibbe N.

The Comet toolbox is licensed under the GNU GENERAL PUBLIC LICENSE Version 3 (`GPL <https://www.gnu.org/licenses/gpl-3.0.en.html>`_).

N.Skibbe @ nico.skibbe@leibniz-liag.de (c) 2016-2023, Leibniz Institute for Applied Geophysics, Hannover, Germany
