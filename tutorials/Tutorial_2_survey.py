#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tutorial 2

Survey-class and FID-class

The tutorial for the Survey class is covering the definition and adjustments
for simple FID experiments as well as the site parameters as magnetic field,
tx and rx definition, pulse moments and many more.

Two complex classes are needed directly for this and presented here.
Furthermore the loop class presented in the first example are used.

This script does not calculate anything extensive. It's only purpose is to
give you a feeling on how to use the survey and FID classes and where to put
your input information.
"""
# comet packages
from comet import pyhed as ph
from comet import snmr
# numerics
import numpy as np
# plotting
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # set log level to 20 == Info level
    ph.log.setLevel(20)

    # %%
    # We quickly define some half overlapping loops for measuring
    # =========================================================================
    # for details see Tutorial 1
    loop0 = ph.loop.buildRectangle([[-40, -20], [0, 20]], max_length=2)
    loop1 = ph.loop.buildRectangle([[-20, -20], [20, 20]], max_length=2)
    loop2 = ph.loop.buildRectangle([[0, -20], [40, 20]], max_length=2)
    # Three 40 * 40 square loops from -40 to 40 meter in x
    ph.plot.showLoopLayout([loop0, loop1, loop2])

    # %%
    # Now we create the survey class
    # =========================================================================
    site = snmr.survey.Survey()
    # bare class, we call it "site" as it will represent our field site

    # measurement geometry from above
    site.addLoop([loop0, loop1, loop2])
    # add our loops
    # they can be found in site.loops
    print(site.loops)

    # earth magnetic field at site
    site.setEarth(mag=48000e-9, incl=60, decl=0)
    print(site.earth)

    # resistivity distribution at site
    # 1 Layer case (1000 Ohmm, 10m thickness) + halfspace (10 Ohmm)
    # take larmor frequency from earth magnetic field
    config = ph.config(rho=[1000, 10], d=[10], f=site.earth.larmor)
    # config is also a class, however it is little more than a container...
    # ... but it can be piped to the survey class
    site.setLoopConfig(config)
    # This sets the resistivity configuration for all loops at once
    # let's check the first loop right away
    print(site.loops[0].config)

    # Apart from the measurement configuration everything is sound.
    # Up to this point we only added more stuff to the survey class.
    # The reason is, that this input is needed for other classes that can work
    # on specific tasks (kernel calculation, modelling of NMR signals etc.)

    # %%
    # Now we define our NMR experiments
    # =========================================================================
    site.createSounding(tx=0, rx=0)
    # That's it. Coincident measurement using the first (python first = 0) loop
    # as receiver and transmitter

    # let's take all combinations:
    for tx_i in range(3):
        for rx_i in range(3):
            site.createSounding(tx=tx_i, rx=rx_i)

    # Note that the fid using tx=0 and rx=0 is not created a second time
    # take a loop
    print(site.fids)
    # %%
    # Let's see what a FID really is
    # =========================================================================
    # Also see documentation of API:
    # https://comet-project.readthedocs.io/en/latest/classes.html#comet.snmr.survey.FID

    # we just grab the first fid
    fid = site.fids[0]

    # This class holds the configuration for a free induction decay experiment
    # This includes for example

    # setter functions
    # =========================================================================

    # frequency offset in Hertz
    fid.setFrequencyOffset(2.3)

    # gates
    fid.setGates([0, 0.1, 0.2, 0.4, 0.6, 0.8, 1.0], midpoints=False)
    # first gate between 0 and 0.1, second between 0.1 and 0.2 and so on...
    # this way the gate thicknesses can be used to calculate the error weights
    # weight = sqrt(number of points per gate)
    # this information is not available if midpoint are given

    # set pulse (s) and deadtime (s)
    fid.setPulseDuration(0.04, deadtime_device=0.005)
    # the effective deadtime is calculated internally summing up the deadtime
    # of the device and half of the pulse duration
    print(f'Effective deadtime: {fid.deadtime:.3f} s')

    # manually changing/setting the receiver index and turns of the loop
    fid.setTx(0, turns=1)  # stored in fid.tx_index and fid.tx_turns
    # same for rx
    fid.setRx(0, turns=1)  # stored in fid.rx_index and fid.rx_turns

    # %%
    # Importing data
    # =========================================================================
    # There are several ways to import data.
    # Here we show you the manually way, convenience functions are available
    # for MRSMatlab files.

    # The basic function for importing raw data need at least three arrays:
    # data (1D vector, complex), error (1D vector, complex),
    # and times (1D vector, float)

    # in this tutorial we take some previously saved vectors from a csv file
    # (you have to modify the import with respect to your data file format)
    # usually numpy has some easy and very flexible options when it comes to
    # import of txt or any kind of csv files
    # e.g. datafile = np.genfromtxt('datafile', ...)
    # you might want to look here for details:
    # https://docs.scipy.org/doc/numpy/reference/generated/numpy.genfromtxt.html

    # here we simply import a npz (numpy compressed vector) archive.
    # importing those we get numpy arrays of various shapes (see console)
    data_npz = np.load('tut2_data.npz')

    data = data_npz['data']
    print('data', data.shape, data.dtype)

    error = data_npz['error']
    print('error', error.shape, error.dtype)

    pulses = data_npz['pulses']
    print('pulses', pulses.shape, pulses.dtype)

    phases = data_npz['phases']
    print('phases', phases.shape, phases.dtype)

    times = data_npz['times']
    print('times', times.shape, times.dtype)

    # set data, error and times, phases (unrotated)
    fid.setRawDataErrorAndTimes(data, error, times, phases=phases,
                                rotated=False)

    # Pulse moments in As (so NOT amperes!)
    fid.setPulses(pulses)
    # you will find a default pulsemoment vector in each fid. That only for
    # synthetic calculations.

    # gating the data, define number of desired gates
    # sometimes first gate does not have data and is deleted, so you might end
    # up one gate short
    fid.gating(num_gates=42)

    # plot example for one fid
    # make figure with one axis to plot stuff into
    fig, ax = plt.subplots()
    # fills a given plot axis with a gated FID plot
    ph.plot.drawFid(ax, fid, to_plot='abs')
    plt.show()

# The End
