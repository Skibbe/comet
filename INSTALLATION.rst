Installation
============

Windows
-------------

This is the most easy installation using the conda environment. If conda is not used, please see the installation "from source" chapter.

After installation of anaconda or miniconda, open the *anaconda navigator*. On the left side you can switch from the *Home* tab to the *Environments* tab. Under the *base (root)* environment click *create* to create a new environment using python=3.7. Click on the new environment to switch to it, the right side shows the installed packages and should be nearly empty. Click on the green triangle right to the environment name and select *Open Terminal*. Proceed with the following conda commands to install packages.

``conda install -c gimli -c conda-forge comet=1.1``

Switching back from *Environments* to *Home* Anaconda provides some apps, one of them being *Spyder*. Click install and then start Spyder from here to make sure the correct environment is used (it is highlighted at the top next to "Applications on"). This should not interfere with previously installed python packages, due to the own environment. With Spyder installed you can proceed with the tutorials or examples. The tutorials are also available as Jupyter Notebook. The Notebook can be installed the same way as Spyder.

Note that on Windows **COMET** is restricted to 1D resistivity since there is no Windows version of the underlying FEniCS library that is used by **custEM**. For using 2D resistivities on Windows 10, consider the `Windows Subsystem for Linux <https://docs.microsoft.com/en-us/windows/wsl/about>`_.

The Anaconda Navigator can be used to install packages. Since **COMET** and **pyGIMLi** are hosted in the channel `gimli <https://anaconda.org/gimli>`_ and **TetGen** in the channel `conda-forge <https://anaconda.org/conda-forge>`_.
These channels must be added to the channel list before installing **COMET** along with **pyGIMLi** and **TetGen**.

Linux
---------

Miniconda or Anaconda is required to install the package via conda. The installation line for **comet** should look like this:

For the handling of 2D resistivities **custEM** is required. Compatibility is ensured for version 0.99.14 and newer. Newer versions will be tested added as soon as they are available.

``conda create -n cometcustem -c gimli -c conda-forge -c anaconda custem=0.99.14 comet``

This creates a new environment with the name "cometcustem" and installs the **COMET** package as well as the **custEM** and all dependencies. Note that the -c argument are adding the gimli (for **COMET**, **pyGIMLi**, and **custEM**) and conda-forge (for **TetGen**) channels, otherwise another package with the name comet will be downloaded instead (not a geophysical package at all). Versions for python 3.6 and 3.7 (technically 3.8 as well, however those are untested) are provided. We highly recommend python 3.7 for now. You can activate the environment using the following line:

``conda activate cometcustem``

Please add ``export OMP_NUM_THREADS=1`` to your ``.bashrc``.

If any problems are encountered installing **custEM**, please consult https://custem.readthedocs.io/en/latest/.

For a **COMET** version without **custEM** instead use the following command for installation (same as for windows):

``conda create -n comet11 -c gimli -c conda-forge -c anaconda comet=1.1``

This creates a new environment with the name "comet11" (the names are your choices really...) and installs the comet package and all dependencies. Then activate environment via:

``conda activate comet``

Again if conda is not used, see installation "from source".

MacOS
-----

Unless using a virtual Linux machine, MacOS is currently not supported. COMET without custEM could work if using the installation from source, however, this is not tested.

From Source
--------------------

Installation of the **comet** python package is easy as only python code is involved. No compiling is needed, a clean copy-paste or *git clone* of the git repository is sufficient. 
Clone the **COMET** repository manually using git or download the zip archive from the project page
``git clone https://gitlab.com/Skibbe/comet/``
The PYTHONPATH has to be set to find the directory containing the __init__.py (see below). **No** execution of the setup.py is required.

If this way of installation is chosen and a **custEM** installation is aimed as well (Linux only), we recommend to install **custEM** first (it brings all compatibilities for **comet** as well, see weblink above). You can additioanlly clone **COMET** as mentioned above and set the *PYTHONPATH* accordingly if you want to work with comet from the source and still not having to deal with dependancies.

If **custEM** is not installed or also cloned from gitlab, than **pyGIMLi** and **TetGen** need to be installed additionally, either by using binary installers or building from source. Please be referred to the webpages linked in the "Where to find the third party packages" section.

The PYTHONPATH needs to be set as global environment variable (or locally inside Spyder using PYTHONPATH manager).

To set the PYTHONPATH directly add the following line to your ~/.bashrc directly (with appropriate path, Linux).

``export PYTHONPATH=$PYTHONPATH:$HOME/some_directory/comet``

Setting the PYTHONPATH can also be done in Spyder directly (Tools -> PYTHONPATH manager -> Add path). We suggest to close Spyder afterwards and re-open it as the built-in synchronization will not always work.

Where to find the third party packages
--------------------------------------

A **pyGIMLi** installation can be found here: https://www.pygimli.org/installation.html.
There is also the support for prebuild binary installers for windows, if conda is not used. However this does not include a **TetGen** installation. Also follow the installation instructions of **pyGIMLi** concerning the ~/.bashrc.

A **TetGen** installation can be found here: http://tetgen.org. If not using conda, you would have to build **TetGen** using the source files. Add the **TetGen** directory to the PATH variable if **TetGen** was build from source.

A **custEM** installation can be found here: https://custem.readthedocs.io/en/latest/install.html.